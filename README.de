### Readme Datei - TuxDash Version 0.8 ###

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Vorwort

TuxDash wurde bisher von mir allein programmiert und stellt mein erstes gr��eres
Projekt dar. Nachempfunden wurde TuxDash dem Spiel Boulder Dash, dass in den
80er Jahren bekannt war.

Da ich �hnliche Spiele, die meinen Vorstellungen entsprachen, nicht finden konnte
- vor allem nicht unter Linux - habe ich mich entschlossen so etwas selbst zu versuchen.

Der grafische Teil ist mit den SDL-Bibliotheken umgesetzt (Simple Direct
Media Layer - http://www.libsdl.org). Daher muss eine aktuelle libSDL
installiert sein, um TuxDash zu starten (au�er bei der statischen gelinkten, bin�ren Version).

Das Programm unterliegt der GPL (General Public License) und somit kann jeder
das Programm verfielf�ltigen, ver�ndern und auch ver�u�ern. Welche Regeln dabei beachtet
werden m�ssen kann man der GPL selbst entnehmen, die neben dieser readme Datei auch beiliegen
sollte.

Was ich allerdings nochmal im speziellen anmerken m�chte ist, dass ich in keiner Weise daf�r verantwortlich
bin, was mit dieser Software geschieht. F�r abge�nderte Versionen des Programms kann ich verst�ndlicherweise
keine Verantwortung �bernehmen, und auch f�r die Funktion meiner origin�ren Version gebe ich keinerlei Garantie.

TuxDash spielen

Es gibt drei Ausf�hrungen von TuxDash, einmal gibt es den reinen Quellcode, dann eine bin�re
Version die einmal dynamisch gelinkt und einmal statisch gelinkt vorliegt.

Der Quellcode l�sst sich durch ein simples 'make' im Verzeichniss /src kompilieren. Die ausf�hrbare
Datei hei�t dann genau wie in den bin�ren Paketen TuxDash.

Die statisch gelinkte Version enth�lt bereits alle Bibliotheken die n�tig sind, um TuxDash zu spielen,
daher ist das Paket auch um einiges gr��er.

Steuerung

Taste(n)			Funktion

Pfeiltasten			In die entsprechende Richtung
				bewegen
a + Pfeiltasten		"Benutzen" (in die entsprechende Richtung graben,
				Diamanten aufsammeln, Steine schieben usw.)
Leertaste			Level neustarten nach Tod oder Timeout
p				Pause

Die Steuerung kann im Men� eigens angepasst werden

Der Stand des Projekts

Ich habe viel Arbeit investiert, um vorhandene Fehler zu beseitigen. Meiner Kenntnis nach d�rfte
es keine signifikanten Probleme mehr geben.
Vom Spielablauf her ist das Spiel im wesentlichen fertig.

Es gibt aber durchaus hier und da noch kleinere, evtl. auch nervende Fehler. Der Quellcode an sich
ist wohl auch nicht von erster G�te. Da ich zu Beginn in reinem C programmierte und dann auf C++ portierte
ist die Objektorientierung und die gesamte Klassenstruktur etwas holprig.

Probleme gibt es vor allem noch mit der Performance in hohen Aufl�sungen und �hnlichen Dingen.

Ich selbst werde wohl nicht mehr viel am Programm �ndern. Wenn ich Feedback bekomme, freue ich mich trotzdem und werde
auch Fehler beseitigen, wenn sie mir berichtet werden. Gerne lade ich jeden ein, der Lust hat, dem Spiel noch etwas Feinschliff
zu geben. Das Spielmen� k�nnte wohl entweder eine komplette �berarbeitung oder eine komfortablere F�hrung brauchen. Urspr�nglich
dachte ich auch an einen Level Editor. Leider fehlt mir hierzu vor allem die Zeit und es zieht mich inzwischen auch zu neuen Vorhaben hin.

Levelkarten

Mit Hilfe der Option "Custom map" kann man eine Karte erstellen, die entsprechend den Parametern gestaltet wird.
"Random Map" generiert hingegen eine komplett zuf�llige Karte. Beide Funktionen unterliegen keiner hinreichenden
validierung, d. h. dass nicht sichergestellt ist, ob der Level �berhaupt gel�st werden kann. Diese validierung zu programmieren
w�re wohl den Aufwand nicht Wert gewesen. Eher w�rde sich da ein Leveleditor anbieten.

Das speichern und laden der Karten erfolgt �ber die entsprechenden Optionen "Load Map" und "Save Map".
Der Spielstand l�sst sich mit den analogen Optionen "Load Game" / "Save Game" sichern. Der Spielstand
enth�lt aber nicht mehr die urspr�ngliche Karte, also kann man den Level bei einem gespeicherten Spiel nicht
mehr von Vorne beginnen.

Kontakt

Wer einen oder mehrere Fehler gefunden hat, Vorschl�ge oder Anregungen mitbringt oder eine
Frage hat, ist gerne eingeladen mich per E-Mail zu kontaktieren. Auch ein Besuch meiner Website
http://www.tuxdash.de mag hilfreich oder interessant sein. Dort gibt es auch noch das ein oder
andere Programm mehr von mir zu finden.

E-Mail: Matthias.Gerstner@student.fh-nuernberg.de
Website: http://www.tuxdash.de

Copyright (c) Matthias Gerstner

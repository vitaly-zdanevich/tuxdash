I copied the source code from Gentoo - when I got the email that soon this game will be removed.

![email](/email.png)

![gentoo](/gentoo.png)

![video](/gameplay.webm)

![website](/website.png) http://web.archive.org/web/20041206013604/http://www.tuxdash.de/index.php?menue=tdash&nav=about&language=EN

![documentation](/documentation.png) http://web.archive.org/web/20041107194256/http://www.tuxdash.de/index.php?menue=tdash&nav=doc&language=EN

Wikidata item https://www.wikidata.org/wiki/Q63065334

/*
    TuxDash 0.8
    field_mgm.h - contains array for game map fields, functions to do operations on several fields

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

#ifndef _fieldmgm_h
#define _fieldmgm_h

class field_mgm {
	private:
		class fields *fieldarray;	// this array represents every field on the map
		class map* game_map;	// pointer to the map object
    		short *dimX, *dimY;		// dimension of current map
		int *diamond_frame_start;	// the correct frame numbers we need to display different objects
		int *explosion_frame_start;
		int *lava_frame_start;
		int *fly_frame_start, *bfly_frame_start, *sfly_frame_start;
	public:
		field_mgm();
		~field_mgm();	// destroys the array
		void hit(const short&, const short&, const short&);	// create an explosion
		void init(const int&, const int&, map*);	// (re)initialise the array with the given dimensions
		int drop(const short&, const short&);	// drop a stone or diamond
		void drop_finish(const short&, const short&, const bool&);	// finish dropping
		void topple(const short&, const short&);	// topple a stone or diamond
		void push(const short&, const short&, const char&);	// push a stone
		void magic(const short&, const short&, const bool&);	// manage magic wall
		void draw(const short&, const short&, const bool&);	// draw the specified field on the screen
		void lava_grow(const short&, const short&, bool&);	// magage lava
		void ai(short, short);	// manage enemies
		void ai_move(const short&, const short&);	// move enemies
		inline class fields& operator()(const short&, const short&) const;	// return the specified field
};

#endif /* field_mgm.h */

/*
    TuxDash 0.8
    surface.h - interfaces/class for SDL_Surface and SDL functions

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

#ifndef _surface_h
#define _surface_h

#include <SDL.h>

using namespace std;
class surface {
	private:
		SDL_Surface *area;	// the standard sdl_surface

		void blit(const SDL_Rect&);	// blit whole surface to positon in SDL_Rect
		void blit(const SDL_Rect&, const SDL_Rect&);	// blit whole or part of surface to position in SDL_Rect

		static short width, height;	// store width and height of surface
		static bool alphaset;	// if alpha blending is activated
		static ostring font;	// global font file for blits with ttf_write
	public:
		static class surface window; // points to the screen surface
		surface(); // default constructor
		surface(const short, const short, const int, const bool=false);	// open new sdl window
		surface(const ostring&);	// load new surface from file
		surface(const ostring&, const ostring&, int, int, int, int);	// create TTF surface
		surface(const ostring&, const class rect&);	// load new surface from file and blit it to position in rect
		~surface();	// destructor
		void init(const ostring&);	// initialize object with new BMP file
		void font_init(const ostring&, const ostring&, int, int, int, int);	// initialize object as ttf surfaces loaded from the specified ttf file
		void blit(short, short);	// blit whole surface to x/y position
		void blit(const class rect&, const class rect&);	// blit part of surcafe to rect position
		void blit(const class rect&);	// blit whole surface to rect position
		static void screenblit(const class rect&, const class rect&);	// blit part of window onto itself
		static void screenfill(short, short, short, short, unsigned char, unsigned char, unsigned char);	// fill screen at specified int-rect in specified int-color
		static void screenfill(const class rect&, unsigned char, unsigned char, unsigned char);	// fill screen at specified rect in specified int-color
		static void screen_drawframe(const short&, const short&, const short&, const short&, const short&, const unsigned char& =255, const unsigned char& =255, const unsigned char& =255);
		static void screen_drawframe(const class rect&, const short&, const unsigned char& =255, const unsigned char& =255, const unsigned char& =255);
		static void ttf_write(const ostring&, int, int, int, int, class rect&);	// blit string as ttf surface at rect position
		static void ttf_write(const ostring&, int, int, int, int, int, int);	// blit string as ttf surface at int-rect position
		static void set_ttf(const ostring&);	// set global ttf file
		void fill(const SDL_Rect&, unsigned char, unsigned char, unsigned char);	// fill rect with color
		void fill(const class rect&, unsigned char, unsigned char, unsigned char);
		void fill(short, short, short, short, unsigned char, unsigned char, unsigned char);
		static void refresh();	// make changes on screen surface visible
		void setalpha(const unsigned char&);	// set alpha channel for surface
		void transparency(const unsigned char&, const unsigned char&, const unsigned char&);	// set transparent color on surface
		static void screen_transparency(const unsigned char&, const unsigned char&, const unsigned char&);	// set transparent color on screen surface
  		void displayformat();	// turn a surface into display format
		void fill_pattern(const class rect&);	// fill rect with a pattern consisting of the surface object
		void line(short, short, short, short, unsigned char, unsigned char, unsigned char);	// draw a line from one point to another
		SDL_Surface* getsf();	// get a pointer to screen surface
		void settitle(const ostring&);	// set title for screen window and task bar button
		void colorize(unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char);	// change src color into dst color
		static short w();	// return screen width
		static short h();	// return screen height
		};
#include "surface.cpp"
#endif /* _surface_h */


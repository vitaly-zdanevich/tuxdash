/*
    TuxDash 0.8
    field_mgm.cpp - contains array for game map fields, functions to do operations on several fields

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

/*
	default constructor
*/
field_mgm::field_mgm() {
	fieldarray = NULL;
	return;
}

/*
	destructor
*/
field_mgm::~field_mgm() {
	delete[] fieldarray;
	return;
}

/*
	returns the choosen object of the array
*/
inline class fields& field_mgm::operator()(const short& X, const short& Y) const {

	/*
	if(X>=*dimX || Y>=*dimY || X<0 || Y<0) {	// Activates debugging and protection against dangerous array access - slows down the game rather much for big maps
		cout << "Warning: array boundaries overflow!" << " X: " << X << " Y: " << Y << endl;
		return feldarray[0];
	}
	*/

	return fieldarray[X * *dimY + Y];
}

/*
	initialises the array
*/
void field_mgm::init(const int& X, const int& Y, map* pointer) {
	game_map = pointer;	// pointer to map object for access to variables

	delete[] fieldarray;
	fieldarray = new class fields[(X*Y)];
	dimX = &game_map->dimX;
	dimY = &game_map->dimY;
	*dimX = X;	// copy dimension in map object
	*dimY = Y;
	diamond_frame_start = &game_map->diamond_frame_start;	// Initialise the pointers to the frame numbers of the map object
	explosion_frame_start = &game_map->explosion_frame_start;
	lava_frame_start = &game_map->lava_frame_start;
	fly_frame_start = &game_map->fly_frame_start;
	sfly_frame_start = &game_map->sfly_frame_start;
	bfly_frame_start = &game_map->bfly_frame_start;
	return;
}

/*
	checks if an object can drop down and does that if possible
*/
int field_mgm::drop(const short& X, const short& Y) {
	if(Y >= (*dimY)-1) return 1;	// The bottom row of the map does not need to be checked

	fields& this_object = operator()(X, Y);		// Reference to the currenct field
	fields& down  = operator()(X, Y+1);		// Reference to the field below


	if((down.type==-1 || (down.type==-2 && ((down.action==1 && down.moved==30) || (this_object.action == -1 && down.action==7))) || (down.type==-3 && down.action==-1))) {
		// under the object is a empty field
		down = this_object.type;	// down object is now the new object
		this_object.type = -2;	// block the source field and set some variables
		this_object.action = 6;
		this_object.moved = 0;
		down.action = 5;		// mark the object as 'dropping'
		down.moved = 30;	// 30 pixels have to be moved on before the object has finally arrived the new field
		return 0;	// return that we have done something
	}

	return 1;	// return that we have done nothing
}

/*
	finish topple or drop for an object
*/
void field_mgm::drop_finish(const short& X, const short& Y, const bool& type) {
	class fields& this_object = operator()(X, Y);	// reference to the current object
	this_object.moved = 0;	// set moved back

	if(type == 1) {	// object was toppling
		if(this_object.action == 7) {	// to the left
			if(operator()(X+1, Y) == -2) operator()(X+1, Y).clear(); // clear source field

		}
		else 	{	// to the right
			if(operator()(X-1, Y) == -2) operator()(X-1, Y).clear();	// clear source field
		}
	}
	else 	if(type == 0) {	// object was dropping
		if(operator()(X, Y-1) == -2) operator()(X, Y-1).clear(); 	// clear source field
	}

	if(type == 1 && operator()(X, Y+1)==-2 && operator()(X, Y+1).action==7) this_object.action = -1;	// toppling object is now dropping
	else if(this_object==*diamond_frame_start) this_object.action = 4;	// drop was finished and diamond animates normal again
	else this_object.action = 0;	// drop was finished and stone does nothing again

	if(Y< (*dimY-1)) {	// now check if we hit something special
		class fields& down = operator()(X, Y+1);	// reference to the down object

		if(down.type==0) hit(X, Y+1, -1); // we hit the player
		else if(down.type == *fly_frame_start) hit(X, Y+1, -1);  // we hit a normal enemy
		else if(down.type == *bfly_frame_start) hit(X, Y+1, *diamond_frame_start); // we hit a butterfly and leave diamonds
		else if(down.type == *sfly_frame_start) hit(X, Y+1, 8); // we hit a stonefly and leave stones
		else if(down.type==-2 && down.action == 1) {	// we got the backfield of the player

			if(X < (*dimX)-1 && operator()(X+1, Y+1) == 0 && operator()(X+1, Y+1).moved < 30)
				hit(X, Y+1, -1);		// player is down right but could not escape quick enough
			else if(X > 0 && operator()(X-1, Y+1) == 0 && operator()(X-1, Y+1).moved < 30)
				hit(X, Y+1, -1);		// player is down left but could not escape quick enough
			else if(Y < (*dimY)-2 && operator()(X, Y+2) == 0 && operator()(X, Y+2).moved < 30)
				hit(X, Y+1, -1);		// player is two fields down but could not escape quick enough

		}
		else if(down.type==-2 && down.action == 2) {	// we got the backfield of an enemy
			short fill;
			if(down.direction == 'R' && operator()(X+1, Y+1).moved < 30) {	// tried to escape to the right
				class fields &temp = operator()(X+1, Y+1);
				if(temp.type == *fly_frame_start) fill = -1;	// normal enemy
				else if(temp.type == *bfly_frame_start) fill = *diamond_frame_start;	// butterfly
				else if(temp.type == *sfly_frame_start) fill = 8;	// stonefly
				hit(X, Y+1, fill);
			}
			else if(down.direction == 'L' && operator()(X-1, Y+1).moved < 30) {	// tried to escape to the left
				class fields &temp = operator()(X-1, Y+1);
				if(temp.type == *fly_frame_start) fill = -1;
				else if(temp.type == *bfly_frame_start) fill = *diamond_frame_start;
				else if(temp.type == *sfly_frame_start) fill = 8;
				hit(X, Y+1, fill);
			}
			else if(down.direction == 'D' && operator()(X, Y+2).moved < 30) {	// tried to escape to fields down
				class fields &temp = operator()(X, Y+2);
				if(temp.type == *fly_frame_start) fill = -1;
				else if(temp.type == *bfly_frame_start) fill = *diamond_frame_start;
				else if(temp.type == *sfly_frame_start) fill = 8;
				hit(X, Y+1, fill);
			}
		}
		else 	{	// if there was nothing special we check again if the object can drop or topple
			// additional we check if a stone is dropping/toppling while we are scrolling. If so then we have to draw it because otherwise there would occur draw errors
			if(drop(X,Y)==1 && game_map->scroll_state == 1 && this_object.type == 8) draw(X,Y, 0);
		}
	}

	return;
}

/*
	check if an object can topple and do so if possible
*/
void field_mgm::topple(const short& X, const short& Y) {
	if(Y >= *dimY-1) return;
	class fields& down = operator()(X, Y+1);	// reference to the object below
	char where=0;

	// if below is a diamond/stone or a wall
	if((down.type==*diamond_frame_start && down.action==4) || (down.type==8 && down.action==0) || (down.type==9 && down.action == 0)) {

		class fields *left, *left_lower, *right, *right_lower;	// pointers to necessary fields around

		if(X-1 >= 0 && Y+1 < *dimY) {	// if we are not at the left border
			left = &operator()(X-1, Y);		// pointer to the left object
			left_lower = &operator()(X-1, Y+1); // pointer to the left lower object
		}

		if(X+1 < *dimX && Y+1 < *dimY) {		// if we are not at the right border
			right = &operator()(X+1, Y);	// pointer to the right object
			right_lower = &operator()(X+1, Y+1); // pointer to the right lower object
		}

		if((X>0 && Y+1 < *dimY) && (((*left) == -1 && (*left_lower) == -1) ||	// left and left lower object are empty
		((*left_lower) == -1 && (*left)==-2 && left->action == 1 && left->moved==30) ||	// left is the moving player
		((*left)==-1 && (*left_lower) == -2 && left_lower->action==1 && left_lower->moved==30 ))) 	// the left lower object is the moving player
			where = 'L';	// able to topple to the left

		else if((X+1 < *dimX && Y+1 < *dimY) && (((*right) == -1 && (*right_lower) == -1) ||	// right and right lower object are empty
		((*right_lower)==-1 && (*right)==-2 && right->action==1 && right->moved==30) ||	// right is the moving player
		((*right==-1) && (*right_lower)==-2 && right_lower->action==1 && right_lower->moved==30) ))	// the right lower object is the moving player
			where = 'R';	// able to topple to the right
	}

	if(where==0) return;	// object can not topple

	fields& center = operator()(X,Y);		// reference to the current field
	fields* target;	// pointer to target field

	if(where=='L') {	// topple left
		target = &operator()(X-1, Y);
		operator()(X-1,Y+1) = -2;	// block the target field
		operator()(X-1,Y+1).action = 7;
		target->action = 7;
	}
	else if(where=='R') {	// nach rechts kippen
		target = &operator()(X+1, Y);
		operator()(X+1,Y+1) = -2;	// block the target field
		operator()(X+1,Y+1).action = 7;
		target->action = 8;
	}

	target->type = center.type;
	target->moved = 30;	// we got to move on 30 pixels
	center.type = -2;		// block the source field
	center.action = 9;
	center.moved = 0;

	return;
}


/*
	creates an explosion around the specified center and fills the fields with the appropriate filltype in the end
*/
void field_mgm::hit(const short& X, const short& Y, const short& filltype) {
	fields& center = operator()(X, Y);

	if(center.action != 10) {		// this is the first pass on this field, hit was just happening
		for(int i=X-1; i<=X+1; i++) {		// cycle through the nine field block around the center
			for(int j=Y-1; j<=Y+1; j++) {
				if(i>=0 && j>=0 && j< *dimY && i< *dimX) {	// we only go on if we are within the map boundaries
					fields& temp = operator()(i,j);	// reference to the current field
					if((temp.type<2 || temp.type>7) && temp.type!=10) {	// everything except of metal walls explodes
						if(temp.type == *diamond_frame_start || temp.type == 8) {	// if a diamond or stone explodes we have to clean up everything correctly
							if(temp.action == 5 || temp.action == -1) {	// falling diamond / stone
								if(operator()(i,j-1).type == -2) {
									operator()(i,j-1).clear();	// clear source field
									draw(i, j-1, true);
								}
							}
							else if(temp.action == 7 || temp.action == 8) {	// toppling diamond / stone
								if(operator()(i,j+1) == -2) {	// have to check the lower field to, because it's been blocked by topple()
										operator()(i,j+1).clear();	// clear lower field
										draw(i,j+1, true);
								}
								if(temp.action == 7) {	// was toppling left
									if( operator()(i+1,j) == -2) {
										operator()(i+1,j).clear();	// clear right field
										draw(i+1,j, true);
									}
								}
								else if(temp.action == 8) {	// was toppling right
									if( operator()(i-1,j) == -2) {
										operator()(i-1,j).clear();	// clear left field
										draw(i-1, j, true);
									}
								}
							}
						}
						else if((temp.type == *fly_frame_start || temp.type == *bfly_frame_start || temp.type == *sfly_frame_start) && temp.action == 2) { // hit a moving enemy, clean up all involved fields
							if(temp.direction == 'R') {	// was moving right
								if(operator()(i-1, j) == -2) {
									operator()(i-1, j).clear();
									draw(i-1, j, true);
								}
							}
							else if(temp.direction == 'L') {	// was moving left
								if(operator()(i+1,j) == -2) {
									operator()(i+1,j).clear();
									draw(i+1, j, true);
								}
							}
							else if(temp.direction == 'D') {	// was moving down
								if(operator()(i, j-1) == -2) {
									operator()(i, j-1).clear();
									draw(i, j-1, true);
								}
							}
							else if(temp.direction == 'U') {	// was moving up
								if(j+1 < *dimY) {
									operator()(i, j+1).clear();
									draw(i, j+1, true);
								}
							}
						}
						else if(temp.type == -2 && temp.action == 7 && j>0) {		// hit the target field of a toppling diamond/stone
							if(operator()(i,j-1).action == -1) {	// got to stop the diamond from continue toppling, because the target field is exploding right now0
								if(operator()(i,j-1) == *diamond_frame_start) operator()(i,j-1).action = 4;
								else if(operator()(i,j-1) == 8) operator()(i,j-1).action = 0;
							}
						}
						else if(temp.type == 0) {	// hit player
							if(temp.action == 1) {	// player was moving - need to clean the involved fields
								if(temp.direction == 'R') {	// was moving right
									if(operator()(i-1, j).type == -2 ) {
										operator()(i-1, j).clear();
										draw(i-1, j, true);
									}
								}
								else if(temp.direction == 'L') {	// was moving left
									if(operator()(i+1, j).type == -2 ) {
										operator()(i+1, j).clear();
										draw(i+1, j, true);
									}
								}
								else if(temp.direction == 'D') {	// was moving down
									if(operator()(i, j-1).type == -2 ) {
										operator()(i, j-1).clear();
										draw(i, j-1, true);
									}
								}
								else if(temp.direction == 'U') {	// was moving up
									if(operator()(i, j+1).type == -2 ) {
										operator()(i, j+1).clear();
										draw(i, j+1, true);
									}
								}
							}
							game_map->playerdeath = 2;	// player was hit and is now dead
						}
						temp=*explosion_frame_start;	// the field is marked as an explosion
						temp.action=10;
						temp.moved = 0;
						temp.animcount=0;
						temp.direction=0;
					}
				}
			}
		}
  		center.direction = filltype;	// the explosion center contains additionally the filltype
	}
	else {	// manage an explosion
		short count = center.animcount;
		for(int i=X-1; i<=X+1; i++) {		// cycle trough the nine fields around the explosion center
			for(int j=Y-1; j<=Y+1; j++) {
				if(i>=0 && j>=0 && j< *dimY && i< *dimX) {		// if we are within the map boundaries
					fields& temp = operator()(i,j);	// reference to the current field
					if((temp.type<2 || temp.type>7) && temp.type!=10)	// everything except metal walls is exploding
						if(temp.animcount == count) temp.animcount+=1;	 // increase counter

				}
			}
		}

		if(center.animcount>40) {	// finish the explosion
			short filltype = center.direction;
			short action=0;

			if(filltype == *diamond_frame_start) action = 4;	// choose the correct action for the filltype - 4 for diamond, 0 for stone or emtpy fields

			for(int i=X-1; i<=X+1; i++) {
				for(int j=Y-1; j<=Y+1; j++) {
					if(i>=0 && j>=0 && i< *dimX && j< *dimY) {		// if we are within the map boundaries
						fields& temp = operator()(i,j);
						if(temp.type==*explosion_frame_start && temp.animcount > 40) {	// check again if we really got an explosion field - it might be that another explosion around has killed one of this explosion fields
							temp = filltype;	// the explosion block is now filled with the filltype
							temp.action = action;
							temp.animcount = 0;
							temp.direction = 0;
							temp.moved = 0;
							draw(i,j, true);	// draw the new fields
						}
					}
				}
			}
		}
	}
	return;
}

/*
	manages stone pushing
*/
void field_mgm::push(const short& X, const short& Y, const char& type) {

	fields *target, *pusher, *center=&operator()(X,Y);	// Pointers to center (pushed stone), target and player

	if(center->type == -5) {	// an enemy was in the way but now the player can push. In this case X and Y point to the target / enemy!
		if(center->direction == 'R' || center->direction == 'r') {	// enemy is moving to the right thus the player is on the left
			if(X-2 == game_map->posX && Y == game_map->posY) {	// if the player is really on the left
				target = center;
				center = &operator()(X-1, Y);
				pusher = &operator()(X-2, Y);
			}
			else {	// if not then the player has given up pushing and we have to block the source field of the moving enemy as if normal
				center->type = -2;
				return;
			}
		}
		else if(center->direction == 'L' || center->direction == 'l') {	// enemy is moving to the left thus the player is on the right
			if(X+2 == game_map->posX && Y == game_map->posY) {	// if the player is really on the right
				target = center;
				center = &operator()(X+1, Y);
				pusher = &operator()(X+2, Y);
			}
			else {	// if not then the player has given up pushing and we have to block the source field of the moving enemy as if normal
				center->type = -2;
				return;
			}
		}
	}
	else {	// everything normal
		if(type == 'R' || type == 'r') {	// push right
			target = &operator()(X+1,Y);
			pusher = &operator()(X-1, Y);
		}
		else if(type == 'L' || type =='l') {	// push left
			target = &operator()(X-1, Y);
			pusher = &operator()(X+1, Y);
		}
	}


	target->type = center->type;	// the stone is on the new field
	target->direction = type;	// add the moving direction
	target->action = 11;	// set action to push
	target->moved = 0;	// by now 0 pixels have been moved
	target->animcount = 0;

	if(type=='L' || type=='R') {	// if 'L' or 'R' was given then the player moves with the stone
		center->type=0;	// now the player is where the stone has been before
		center->action=1;
		center->moved=0;
		center->animcount=0;
		center->direction = type;
		if(pusher->type==0) {	// the source field of the player is now blocked
			pusher->clear();
			pusher->type=-2;
		}
	}
	else {	// if 'l' or 'r' was given the player remains at his position and only the stone moves
		center->type = -2;	// the source field of the stone has to be blocked
		center->action = 6;
	}

	if(type == 'R') game_map->posX+=1;	// if the player moves with the stone the position has to be up to date
	if(type == 'L') game_map->posX-=1;

	return;
}

/*
	manage magic wall
*/
void field_mgm::magic(const short& X, const short& Y, const bool& transformed) {
	if(Y >= (*dimY)-1) return;	// if we are at the map bottom we do not have to do anything

	if(operator()(X,Y).type != *diamond_frame_start && operator()(X,Y).type != 8) return;	// if this is neither a diamond nor a stone we don't have to do anything
	if(transformed == false && (operator()(X, Y+1).type !=9 || operator()(X, Y+1).action != 1)) return;	// if the lower field is no magic wall and this object was not transformed before (indicated by transformed)

	fields &center = operator()(X,Y), &mauer = operator()(X,Y+1);	// reference to this field and the magic wall below it

	if(transformed == false) {	// check if the object is going to fall on a magic wall
		if(mauer == 9 && mauer.action == 1 && (center.action == 0 || center.action == 4)) {	// we really hit a magic wall
			if(game_map->magwall_start == -1) game_map->magwall_start = ::time(0);	// activate magic wall if not done so before
			center.action = 13;	// object is going to fall into the wall
			center.moved = 30;
		}
	}
	else {	// the object already passed the magic wall
		if(center.action != 14) {	// now check what is below the magic wall
			fields* down=NULL;	// the field below the wall
			if(Y < (*dimY)-2) down = &operator()(X, Y+2);	// pointer to the down field if we are not crossing the map bottom

			if(down!=NULL && (::time(0) - game_map->magwall_start < game_map->magwall_duration) && down->type == -1 && down->action == 0) {	// if the field below the wall is empty and the magic wall is still active
				if(center == 8) down->type = *diamond_frame_start;	// stone turns into diamond ...
				else down->type = 8;			// and diamond turns into stone
				down->action  = 14;	// now the object is dropping out of the wall
				down->moved = 30;
			}
			center.clear();	// the object that hit the wall does not exist any longer
		}
		// in this case the object is already dropping out of the wall
		else	if(center.moved < 0) drop_finish(X, Y, 3); // when the object has passed all 30 pixel then the drop is finished
	}

	return;
}

/*
	draw a single field
*/
void field_mgm::draw(const short& X, const short& Y, const bool& clear) {

	int xpos, ypos, hoehe, breite, rx=X-game_map->viewX, ry=Y-game_map->viewY;
	fields &center = operator()(X, Y);
	if((center == *diamond_frame_start && center.action!=4) || (center == 8 && center.action !=0) || (center >=*lava_frame_start && center.action!=1)) return;	// this animations will not be drawn - this is the job of map::drawstart

	xpos = rx*30;	// calculate position in pixels
	ypos = ry*30 + 30;

	if(game_map->scroll_state==1 && game_map->scrolldiff%30 != 0) {				// depending on scrollmovement add correction values to position
		if(game_map->scrolldirection=='R') xpos-= game_map->scrolldiff%30;
		else if(game_map->scrolldirection=='L') xpos+= game_map->scrolldiff%30;
		else if(game_map->scrolldirection=='U') ypos+= game_map->scrolldiff%30;
		else if(game_map->scrolldirection=='D') ypos-= game_map->scrolldiff%30;
	}

	if(ypos < 0) return;
	else if(ypos < 30) {	// if the object is not fully visible only draw a part of the frame
		hoehe = ypos;
		ypos = 30;
	}
	else {	// the size of the frame if we have to draw on the surface
		hoehe = 30;
		breite = 30;
	}

	if(clear == true || center == -1) {	// fill the field in background color
		surface::screenfill(xpos, ypos, hoehe, breite, game_map->BGColor.r, game_map->BGColor.g, game_map->BGColor.b);
	}
	if(center >= 0) {	// draw frame
		rect source(30-breite, 30-hoehe, hoehe, breite);
		rect target(xpos, ypos);
		game_map->frames[center.type].blit(source, target);
	}
	return;


}

/*
	manages the growth of lava
*/
void field_mgm::lava_grow(const short& X, const short& Y, bool& lava_moveable) {
	if(operator()(X,Y).type!=*lava_frame_start || operator()(X,Y).action != 1) return;	// if this field is no lava

	fields &center = operator()(X, Y);	// reference to this field

	if(game_map->lava_percentage > 0) {		// if there is still lava activated

		fields *left=0, *right=0, *down=0, *up=0;	// pointers to the fields around the lava
		short prob = game_map->lava_probability;	// prob is the current growing speed

		if(X>0)  left = &operator()(X-1, Y);		// set the pointers
		if(X< *dimX-1) right = &operator()(X+1, Y);
		if(Y>0) up = &operator()(X, Y-1);
		if(Y< *dimY-1) down = &operator()(X, Y+1);

		if(left!=NULL && (left->type == -1 || left->type == 1) && rand()%prob == 1) {	// if the choosen direction is empty or grass and random/propability allows the lava is growing in that direction
			left->clear();	// clear the field
			left->type = center.type;	// there is now lava on the field
			left->action = center.action;
			lava_moveable = true;	// lava can still move
		}
		else if(right!=NULL && (right->type == -1 || right->type == 1) && rand()%prob < 1) {
			right->clear();
			right->type = center.type;
			right->action = center.action;
			lava_moveable = true;
		}
		else if(down!=NULL && (down->type == -1 || down->type == 1) && rand()%prob < 1) {
			down->clear();
			down->type = center.type;
			down->action = center.action;
			lava_moveable = true;
		}
		else if(up!=NULL && (up->type == -1 || up->type == 1) && rand()%prob < 1) {
			up->clear();
			up->type = center.type;
			up->action = center.action;
			lava_moveable = true;
		}
		else if(lava_moveable == false) {	// if lava didn't grow check if it would have been possible at least
			if(left!=NULL && (left->type == -1 || left->type == 1)) lava_moveable = true;
			else if(right!=NULL && (right->type == -1 || right->type == 1)) lava_moveable = true;
			else if(down!=NULL && (down->type == -1 || down->type == 1)) lava_moveable = true;
			else if(up!=NULL && (up->type == -1 || up->type == 1)) lava_moveable = true;
		}
	}
	else if(game_map->lava_percentage == -1 || game_map->lava_percentage == -2) {	// lava amount has become to big and turns now into stone
		center = 8;
		center.action = 0;
 		game_map->lava_percentage = -2;	// tell map::physics that it is time to redraw the whole screen so that the stone become visible
	}
	else if(game_map->lava_percentage == -3) {		// lava is trapped and cannot move thus it turns into diamond
		center = *diamond_frame_start;
		center.action = 4;
	}
	return;
}

/*
manage enemy movement
*/
void field_mgm::ai(short X, short Y) {

	short type = operator()(X,Y).type;
	if(type!=*fly_frame_start && type!=*bfly_frame_start && type!=*sfly_frame_start) return; 	// if on this field is no enemy return

	fields &center=operator()(X, Y), *left=0, *right=0, *down=0, *up=0, *upleft=0, *upright=0, *downleft=0, *downright=0, *new_one=0;		// pointers to the fields around

	if(X>0) left = &operator()(X-1, Y);	// if within the map borders initialise the pointers
	if(Y>0) up = &operator()(X, Y-1);	// ...
	if(X< *dimX-1) right = &operator()(X+1, Y);
	if(Y< *dimY-1) down = &operator()(X, Y+1);
	if(X>0 && Y>0) upleft = &operator()(X-1, Y-1);
	if(X< *dimX-1 && Y< *dimY-1) downright = &operator()(X+1, Y+1);
	if(X< *dimX-1 && Y>0) upright = &operator()(X+1, Y-1);
	if(X>0 && Y< *dimY-1) downleft = &operator()(X-1, Y+1);

	char old_moved = center.direction, new_direction=0;

	// check if we touch the player or lava
	if( (left != NULL && (left->type==0 || (left->type==*lava_frame_start && left->action==1) || (left->type==-2 && left->action==1 && left->direction!='L'))) || (right != NULL && (right->type == 0 || (right->type==*lava_frame_start && right->action==1) || (right->type==-2 && right->action==1 && right->direction!='R'))) || (up!=NULL && (up->type==0 || (up->type==*lava_frame_start && up->action==1) || (up->type==-2 && up->action==1 && up->direction!='U'))) || (down!=NULL && (down->type==0 || (down->type==*lava_frame_start && down->action==1) || (down->type==-2 && down->action==1 && down->direction!='D')))) {
		if(center.type == *fly_frame_start) hit(X, Y, -1);	// explode and leave the correct filltype
		else if(center.type == *bfly_frame_start) hit(X, Y, *diamond_frame_start);
		else hit(X, Y, 8);
		return;
	}
	else if(old_moved == 0) {	// enemy did'nt move before
		if( (right!=NULL && (right->type==-1 || (right->type==-2 && right->action == 1)) && ( (up==NULL || up->type!=-1) || (upright==NULL || upright->type!=-1) ) )) {
			// the upper or upper right field is not empty and the right field is empty -> move along the upper line
			new_direction = 'R';
		}
		else if( (down!=NULL && (down->type==-1 || (down->type==-2 && down->action == 1) )) && ( (right==NULL || right->type!=-1) || (downright==NULL || downright->type!=-1) ) ) {
			// move along the right line
			new_direction = 'D';
		}
		else if( (left!=NULL && (left->type==-1 || (left->type==-2 && left->action == 1))) && ( (down==NULL || down->type!=-1) || (downleft==NULL || downleft->type!=-1) ) ) {
			// move along the lower line
			new_direction = 'L';
		}
		else if( (up!=NULL && (up->type==-1 || (up->type==-2 && up->action == 1)) && ( (left==NULL || left->type!=-1) || (upleft==NULL || upleft->type!=-1) ) )) {
			// move along the left line
			new_direction = 'U';
		}
		else if( (down!=NULL && (down->type==-1 || (down->type==-2 && down->action == 1))) &&
			(left!=NULL && (left->type==-1 || (left->type==-2 && left->action == 1))) &&
			(right!=NULL && (right->type==-1 || (right->type==-2 && right->action == 1))) &&
			(up!=NULL && (up->type==-1 || (up->type==-2 && up->action == 1))) ) {
			// all directions are free and there is no possibility to move along any border -> simply go right and look if its better then
			new_direction = 'R';
			center.moved = -1;
		}
	}
	else {	// enemy was moving before
		if(old_moved == 'R') {	// enemy was moving right
			if( (right!=NULL && (right->type==-1 || (right->type==-2 && right->action == 1))) &&  (up==NULL || up->type!=-1)  ) {
			// moves on right
				new_direction = 'R';
			}
			else if( (up!=NULL && (up->type==-1 || (up->type==-2 && up->action == 1)) && ( upleft==NULL || upleft->type!=-1) )) {
			// moves now up
				new_direction = 'U';
			}
			else if( ( (up==NULL || up->type!=-1) && (right==NULL || right->type!=-1) ) && (down!=NULL && (down->type==-1 || (down->type==-2 && down->action == 1) )) ) {
			// moves now down
				new_direction = 'D';
			}
			else if( (up==NULL || up->type!=-1) && (right==NULL || right->type!=-1) && (down==NULL || down->type!=-1) && (left!=NULL && (left->type==-1 || (left->type==-2 && left->action == 1))) ) {
			// moves back left
				new_direction = 'L';
			}
			else if( center.moved == -1 && (down!=NULL && (down->type==-1 || (down->type==-2 && down->action == 1) ))) {
			// if there is no border to move along the enemy rotate in a circle
				new_direction = 'D';
			}
		}
		else if(old_moved == 'D') {	// enemy was moving down
			if( (down!=NULL && (down->type==-1 || (down->type==-2 && down->action == 1) )) &&  (right==NULL || right->type!=-1) ) {
			// moves on down
				new_direction = 'D';
			}
			else if( (right!=NULL && (right->type==-1 || (right->type==-2 && right->action == 1))) && (upright==NULL || upright->type!=-1) ) {
			// moves right
				new_direction = 'R';
			}
			else if( (right==NULL || right->type!=-1) && (down==NULL || down->type!=-1) && (left!=NULL && (left->type==-1 || (left->type==-2 && left->action == 1))) ) {
			// moves left
				new_direction = 'L';
			}
			else if( (right==NULL || right->type!=-1) && (down==NULL || down->type!=-1) && (left==NULL || left->type!=-1) && (up!=NULL && (up->type==-1 || (up->type==-2 && up->action == 1)) )) {
			// moves back up
				new_direction = 'U';
			}
			else if( center.moved == -1 && (left!=NULL && (left->type==-1 || (left->type==-2 && left->action == 1)))) {
			// if there is no border to move along the enemy rotates in a circle
				new_direction = 'L';
			}
		}
		else if(old_moved == 'L') {	// enemy was moving left
			if( (left!=NULL && (left->type==-1 || (left->type==-2 && left->action == 1))) && (down==NULL || down->type!=-1) ) {
			// moves on left
				new_direction = 'L';
			}
			else if( (down!=NULL && (down->type==-1 || (down->type==-2 && down->action == 1) )) && (downright==NULL || downright->type!=-1) ) {
			// moes down
				new_direction = 'D';
			}
			else if( (down==NULL || down->type!=-1) && (left==NULL || left->type!=-1) && (up!=NULL && (up->type==-1 || (up->type==-2 && up->action == 1)) )) {
			// moves up
				new_direction = 'U';
			}
			else if( (down==NULL || down->type!=-1) && (left==NULL || left->type!=-1) && (up==NULL || up->type!=-1) && (right!=NULL && (right->type==-1 || (right->type==-2 && right->action == 1))) ) {
			// moves back right
				new_direction = 'R';
			}
			else if( center.moved == -1 && (up!=NULL && (up->type==-1 || (up->type==-2 && up->action == 1) ))) {
			// if there is no border to move along the enemy rotates in a circle
				new_direction = 'U';
			}
		}
		else if(old_moved == 'U') {	// enemy was moving up
			if( (up!=NULL && (up->type==-1 || (up->type==-2 && up->action == 1)) && (left==NULL || left->type!=-1)  )) {
			// moves on up
				new_direction = 'U';
			}
			else if( (left!=NULL && (left->type==-1 || (left->type==-2 && left->action == 1))) && (downleft==NULL || downleft->type!=-1) ) {
			// moves left
				new_direction = 'L';
			}
			else if( (left==NULL || left->type!=-1) && (up==NULL || up->type!=-1) && (right!=NULL && (right->type==-1 || (right->type==-2 && right->action == 1))) ) {
			// moves right
				new_direction = 'R';
			}
			else if( (left==NULL || left->type!=-1) && (up==NULL || up->type!=-1) && (right==NULL || right->type!=-1) && (down!=NULL && (down->type==-1 || (down->type==-2 && down->action == 1) )) ) {
			// moves back down
				new_direction = 'D';
			}
			else if( center.moved == -1 ) {
			// the enemy was doing a circle and know can check again if there is a border to move along
				center.moved = 0;
			}
		}
	}

	if(new_direction!=0) {	// if the enemy has found a direction to move to
		center.direction = new_direction;
		if(center.direction == 'R') X+=1;	// correct the position
		else if(center.direction == 'L') X-=1;
		else if(center.direction == 'U') Y-=1;
		else if(center.direction == 'D') Y+=1;
		new_one = &operator()(X, Y);	// point to the new position
		new_one->direction = center.direction;
		new_one->moved = 5;
		new_one->type = center.type;
		new_one->action = 2;
		new_one->animcount = -1;	// tells ai_move that it does not need to increase moved because it already happened here
		center.type = -2;	// block source field
		center.action = 2;
		if(center.moved!=-1) center.moved = 0;

		// check if the player is somewhere at the diagonals - to kill him
		short fill;
		if(new_one->type == *fly_frame_start) fill = -1;	// choose the correct filltype
		else if(new_one->type == *sfly_frame_start) fill = 8;
		else fill = *diamond_frame_start;

		if(new_one->direction == 'R') {	// check upper right and lower right field
			if(upright!=NULL && upright->type == 0 && (upright->direction == 'L' || upright->direction == 'D'))
				hit(X,Y, fill);
			else if(downright!=NULL && downright->type == 0 && (downright->direction == 'L' || downright->direction == 'U'))
				hit(X,Y, fill);
		}
		else if(new_one->direction == 'L') {	// check upper left and lower left field
			if(upleft!=NULL && upleft->type == 0 && (upleft->direction == 'R' || upleft->direction =='D'))
				hit(X,Y, fill);

			else if(downleft!=NULL && downleft->type == 0 && (downleft->direction == 'R' || downleft->direction == 'U'))
				hit(X,Y, fill);

		}
		else if(new_one->direction == 'U') {	// check upper left and upper right field
			if(upleft!=NULL && upleft->type == 0 && (upleft->direction == 'D' || upleft->direction == 'R'))
				hit(X, Y, fill);
			else if(upright!=NULL && upright->type == 0 && (upright->direction == 'D' || upright->direction == 'L'))
				hit(X ,Y, fill);
		}
		else if(new_one->direction == 'D') {	// check lower left and lower right field
			if(downleft!=NULL && downleft->type == 0 && (downleft->direction == 'U' || downleft->direction == 'R'))
				hit(X, Y, fill);
			else if(downright!=NULL && downright->type == 0 && (downright->direction == 'U' || downright->direction =='L'))
				hit(X, Y, fill);
		}
	}
	else {	// if the enemy had no chance to move we mark it in the direction variable
		center.direction = 0;
	}

	return;

}

/*
	performs the movement of enemies
*/
void field_mgm::ai_move(const short& X, const short& Y) {

	short type = operator()(X,Y).type;

	// if this is not a moving enemy, return
	if((type != *fly_frame_start && type !=*bfly_frame_start && type != *sfly_frame_start) || operator()(X,Y).action!=2) return;

	fields& center = operator()(X, Y);

	if(center.animcount == -1) {	// moved increase has already been done (see ai(...) )
		center.animcount = 0;
		return;
	}

	center.moved+=5;	// increase moved

	if(center.moved > 30) {	// movement is done
		short Xn=X, Yn=Y;

		if(center.direction == 'R') Xn = X-1;
		else if(center.direction == 'D') Yn = Y-1;
		else if(center.direction == 'L') Xn = X+1;
		else if(center.direction == 'U') Yn = Y+1;

		fields &source = operator()(Xn, Yn);

		if(source.type==-5) push(Xn, Yn, source.direction); // a stone shall be pushed (see push(...) )

		if(source.type==-2) {	// source field can be deleted
			if(source.moved == -1) center.moved = -1;
			source.clear();
		}

		center.action = 1;
		if(center.moved != -1) center.moved = 0;
	}

	return;
}

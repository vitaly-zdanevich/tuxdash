/*
    TuxDash 0.8
    main.cpp - main game loop, manage controls, initialize game ...

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

// standard libraries
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

// SDL and SDL_ttf
#include <SDL.h>
#include <SDL_ttf.h>

using namespace std;

// global variables
int globalstart;	// time
unsigned short speed = 20;	// speed limit
SDLKey key[7];	// variable keys for game control
char* TuxHomeDirectory;	// directory for TuxDash file in users home directory ( ~/.tuxdash )

// TuxDash files
#include "ostring.h"	// own string class
#include "rect.h"	// simple class for easy use of SDL_Rect variables
#include "surface.h"	// class for more comfotable interfaces to SDL
#include "fields.h"	// a simple class for array objects
#include "field_mgm.h"	// contains functions operated over several fields
#include "elements.h"	// a linked list data type for menu elements
#include "menu.h"	// menu management
#include "map.h"	// functions concerning global game operations
#include "menu.cpp"
#include "map.cpp"
#include "field_mgm.cpp"

/*
	writes properties to the config file
	friend of class map
*/

void writeconfig(const map& game_map) {
	ofstream config((ostring)TuxHomeDirectory + "/config");	// open config file
	if(config == NULL) {	// error check
		cout << "Warning: Couldn't write to file " << (ostring)TuxHomeDirectory + "/config" << endl;
		return;
	}
	else {	// Datei schreiben
		if(game_map.fullscreen == false) {	// window mode is chosen
			config << "# Fullscreen enable/disable" << endl;
			config << "Fullscreen = 0" << endl;
			config << "# Width of screen in blocks" << endl;
			config << "screenX = " << game_map.screenX << endl;
			config << "# Height of screen in blocks" << endl;
			config << "screenY = " << game_map.screenY << endl;
		}
		else {	// fullscreen mode is chosen
			config << "# Fullscreen enable/disable" << endl;
			config << "Fullscreen = 1" << endl;
			config << "# Fullscreen resolution" << endl;
			config << "FullscreenX = " << game_map.fullscreenX << endl;
			config << "FullscreenY = " << game_map.fullscreenY << endl;
		}
		config << "# Theme Folder - path to a valid theme" << endl;
		config << "theme = " << game_map.theme << endl;
		config << "# Distance in X direction, before scrolling starts" << endl;
		config << "scrolldistX = " << game_map.scrolldistX << endl;
		config << "# Distance in Y direction, before scrolling starts" << endl;
		config << "scrolldistY = " << game_map.scrolldistY << endl;
		config << "# Number of fields to scroll in X direction" << endl;
		config << "scrollamountX = " << game_map.scrollamountX << endl;
		config << "# Number of fields to scroll in Y direction" << endl;
		config << "scrollamountY = " << game_map.scrollamountY << endl;
		config << "# Game speed - 30 is default" << endl;
		config << "speed = " << (speed-50)*-1 << endl;
		config << "#  Display framerate yes/no" << endl;
		config << "framerate = ";
		if(game_map.framerate >= 0) config  << "1" << endl;
		else config << "0" << endl;
		config << "# ASCII values for controls" << endl;
		config << "left = " << (int)key[0] << endl;
		config << "right = " << (int)key[1] << endl;
		config << "up = " << (int)key[2] << endl;
		config << "down = " << (int)key[3] << endl;
		config << "use = " << (int)key[4] << endl;
		config << "scroll = " << (int)key[5] << endl;
		config << "pause = " << (int)key[6] << endl;
		config.close();
		cout << "Configuration has been saved" << endl;
	}

	return;
}

/*
	reads properties from the config file
	friend of class map
*/

void readconfig(class map& game_map) {
	ifstream config((ostring)TuxHomeDirectory + "/config");	// open config file
	if(config==0) {	// error check
		cout << "Warning: Couldn't find configuration file " << (ostring)TuxHomeDirectory + "/config" << ". Using default values." << endl;
		return;
	}
	else {	// Datei lesen
		int line=0;
		char temp;
		ostring property, value;

		for(int i=0; config.eof() != true; i++) {
			config.get(temp);
			if(i==0 && temp == '#') {		// skip comment line
				do {
					config.get(temp);
				}
				while(temp!='\n' && config.eof() != true);
				line+=1;	// next line
				i=-1;	// new line
			}
			else if(temp != '\n' && temp != ' ') property(i, temp);	// neither line end nor space character
			else if(temp == ' ') {	// found space character, get value
				config.get(temp);	// look at the next character
				if(temp == '=') {
					property(i, '\0');
					config.get(temp);
					if(temp == ' ') {	// here comes the value
						i = 0;
						while(temp != '\n' && config.eof() != true) {
							config.get(temp);
							value(i, temp);
							i+=1;
						}

						value(i-1, '\0');
						line+=1;	// next line
						i = -1;	// afterwards we go on with a new line

						// check value
						int value_int = atoi(value);
						for(int j=0; j<=property.len(); j++) property(j, tolower(property[j]));
						if(property == "fullscreen") {	// value for fullscreen on/off
							if(value_int == 1) game_map.fullscreen = true;
							else if(value_int == 0) game_map.fullscreen = false;
							else {
								cout << "Wrong Fullscreen value was given in config file: " << value_int << endl;
								cout << "Fullscreen-Mode is disabled" << endl;
								game_map.fullscreen = false;
							}
						}
						else if(property == "fullscreenx") {	// value for fullscreen x-resolution
							if(value_int == 640 || value_int == 800 || value_int == 1024 || value_int == 1280) game_map.fullscreenX = value_int;
							else {
								cout << "Wrong X-Resolution was given for Fullscreen-Mode: " << value_int << endl;
								cout << "Fullscreen-Mode is disabled" << endl;
								game_map.fullscreen = false;
							}
						}
						else if(property == "fullscreeny") {	// value for fullscreen y-resolution
							if((value_int == 480 && game_map.fullscreenX == 640) || (value_int == 600 && game_map.fullscreenX == 800) || (value_int == 768 && game_map.fullscreenX == 1024) || (value_int == 1024 && game_map.fullscreenX == 1280)) game_map.fullscreenY = value_int;
							else {
								cout << "Wrong Y-Resolution was given for Fullscreen-Mode or X-Resolution does not match Y-Resolution: " << value_int << endl;
								cout << "Fullscreen-Mode is disabled" << endl;
								game_map.fullscreen = false;
							}
						}
						else if(property == "screenx") {		// value for window x-size
							if(value_int >= 10 && value_int <= 53) game_map.widthX = game_map.screenX = value_int;
							else {
								cout << "Caution: In config file in line " << line << endl;
								cout << "Value " << value_int << " for screenX is not within the valid range. Minimum is 10, maximum is 53" << endl;
							}
						}
						else if(property == "screeny") {		// value for window y-size
							if(value_int >= 10 && value_int <= 40) game_map.widthY = game_map.screenY = value_int;
							else {
								cout << "Caution: In config file in line " << line << endl;
								cout << "Value " << value_int << " for screenY is not within the valid range. Minimum is 10, maximum is 40" << endl;
							}
						}
						else if(property == "scrolldistx") {	// value for scrolldistance x
							if(value_int >= 0 && value_int < ((int)(game_map.widthX/2) - 2)) game_map.scrolldistX = value_int;
							else {
								cout << "Caution: in config file in line " << line << endl;
								cout << "Value " << value_int << " for scrolldistX is not within the valid range. Minimum is 0, maximum is half of screenwidth - 2" << endl;
							}
						}
						else if(property == "scrolldisty") {	// value for scrolldistance y
							if(value_int >= 0 && value_int < ((int)(game_map.widthY/2) - 2)) game_map.scrolldistY = value_int;
							else {
								cout << "Caution: in config file in line " << line << endl;
								cout << "Value " << value_int << " for scrolldistY is not within the valid range. Minimum is 0, maximum is half of screenheight - 2" << endl;
							}
						}
						else if(property == "scrollamountx") {	// value for scrollamount x
							if(value_int >= 3 && value_int < game_map.screenX - game_map.scrolldistX) game_map.scrollamountX = value_int;
							else {
								cout << "Caution: In config file in line " << line << endl;
								cout << "Value " << value_int << " for scrollamountX is not within the valid range. Minimum is 3, maximum is " << game_map.screenX - game_map.scrolldistX << endl;
							}
						}
						else if(property == "scrollamounty") {	// value for scrollamount y
							if(value_int >= 2 && value_int < game_map.screenY - game_map.scrolldistY) game_map.scrollamountY = value_int;
							else {
								cout << "Caution: In config file in line " << line << endl;
								cout << "Value " << value_int << " for scrollamountY is not within the valid range. Minimum is 3, maximum is " << game_map.screenY - game_map.scrolldistY << endl;
							}
						}
						else if(property == "speed") {	// value for game speed
							if(value_int >= 0 && value_int <= 50) speed = (value_int-50)*-1;
							else {
								cout << "Caution: In config file in line " << line << endl;
								cout << "Value " << value_int << " for speed is not within the valid range. Minimum is 0, maximum is 50" << endl;
							}
						}
						else if(property == "framerate") {	// display framerate on/off
							if(value_int == 0) game_map.framerate = -1;
							else if(value_int == 1) game_map.framerate = 0;
							else {
								cout << "Caution: In config file in line " << line << endl;
								cout << "Value " << value_int << " for framerate is noth valid" << endl;
								cout << "Framerate is disabled" << endl;
							}
						}
						else if(property == "theme") {	// theme-folder
							game_map.theme = value;
						}
						else if(property == "left") key[0] = (SDLKey)value_int;	// control keys
						else if(property == "right") key[1] = (SDLKey)value_int;
						else if(property == "up") key[2] = (SDLKey)value_int;
						else if(property == "down") key[3] = (SDLKey)value_int;
						else if(property == "use") key[4] = (SDLKey)value_int;
						else if(property == "scroll") key[5] = (SDLKey)value_int;
						else if(property == "pause") key[6] = (SDLKey)value_int;

						else cout << "Warning: error in line " << line << " in config file " << endl;		// no valid option was given - output error message
					}
				}
			}
			else if(temp == '\n') {
				i = 0;	// reached line end, check next property
				line+=1;	// next line
			}

		}
	}
	return;
}

/*
	output usage information if wrong arguments are given in arg_mgm
*/

void usage(class ostring text) {
	cout << "Usage: start [-colordepth=16|32]" << endl << endl;
	cout << text << endl;
	return;
}

/*
	process possibly given arguments from the command line
*/

int arg_mgm(int argc, char* argv[], class map& game_map) {
	if(argc>1) {	// parameter exists
		ostring temp;	// to copy parameter string
		short found=0;	// counts valid arguments

		for(int i=0; i<argc; i++) {	// check every parameter
   			if(strlen(argv[i]) == 14) {	// check for colordepth argument
				for(int j=0; j<12; j++) temp(j, argv[i][j]);	// copy paremeter name

				if(temp == "-colordepth=") {	// colordepth argument found
					int colordepth;
					ostring colordepth_string;
					colordepth_string(0, argv[i][12]);	// copy colordepth digits
					colordepth_string(1, argv[i][13]);
					colordepth = atoi(colordepth_string);	// change to integer
					if(colordepth != 16 && colordepth != 32) {	// neither 16 nor 32 bits were given
						usage("the given colordepth is not valid (must be 16 or 32)");	// quit after usage output
						return 1;
					}
					else {	// correct value was given
						game_map.bitdepth = colordepth;	// take over the found bitdepth value
						cout << "use colordepth: " << colordepth << endl;
						found+=1;	// increase valid parameters by one
					}
				}
			}
		}
		if(found+1 != argc) {	// if not all arguments could be processed validly we exit after usage output
			usage("an invalid argument was given");
			return 1;
		}
	}

	return 0;
}

int main(int argc, char* argv[]) {
	cout << endl << " ** Starting TuxDash version 0.8 ** " << endl << endl;	// Startup message
	cout << " Copyright (C) 2003 Matthias Gerstner" << endl;
	cout << " Tuxdash comes with ABSOLUTELY NO WARRANTY; for details see README." << endl;
	cout << " This is free software and you are welcome to redistribute it under certain" << endl;
	cout << " conditions; see GPL for details" << endl;

	// standard key alignment
	key[0] = SDLK_LEFT;
	key[1] = SDLK_RIGHT;
	key[2] = SDLK_UP;
	key[3] = SDLK_DOWN;
	key[4] = SDLK_a;
	key[5] = SDLK_LALT;
	key[6] = SDLK_p;

	// set tuxdash's config / working directory
	char* HomeDirectory;
	char* CurrentDirectory;
	HomeDirectory = getenv("HOME");	// get users home directory
	CurrentDirectory = getenv("PWD");	// get TuxDash's working directory
	TuxHomeDirectory = new char[strlen(HomeDirectory)+strlen("/.tuxdash")+1];	// align space for the string containing the path to tuxdash's config directory
	strcpy(TuxHomeDirectory, HomeDirectory);
	strcat(TuxHomeDirectory, "/.tuxdash");

	if(chdir(TuxHomeDirectory) == -1) {
		cout << " Create directory " << TuxHomeDirectory << endl;
		if(mkdir(TuxHomeDirectory, 0711)==-1) {
			cerr << " Error: Could'nt create " << TuxHomeDirectory << ". Please check if you have got the right permissions in your home directory." << endl;
			return 1;
		}
		else {
			mkdir((ostring)TuxHomeDirectory + "/themes", 0711);	// create the themes folder. The default themes are not copied there, but the folder is created for possible additional themes added by the player
			chdir(CurrentDirectory);
			system((ostring)"cp -r maps savegames config " + TuxHomeDirectory);
		}
	}
	cout << endl << " Using " << TuxHomeDirectory << " for configuration, map and savegame files" << endl;
	chdir(CurrentDirectory);
	// finished with check of working directory

	int running=1, start, stop, framestart = time(0), frames=0, frame_count = 0;
	// int mousex, mousey;	// variables for mouse control
	globalstart = time(0);
	srand(time(0));	// initialize rand()
	surface::set_ttf("fonts/DENMARK.TTF");	// load a global TTF for the menu
	map game_map;		// initialize a map objekt
	if(arg_mgm(argc, argv, game_map)) running = 0;		// process arguments and exit main if an error occurs
	if(game_map.init()) running = 0;	// initialize screen and global values  (exit main if user quit in menu at the first time)

	Uint8 *keystate;
	// Uint8 mousestate;	// for mouse control
	SDL_Event event;

	while(running == 1)  {
		start = SDL_GetTicks();	// time at the frame start
		keystate = SDL_GetKeyState(NULL);	// get current key state
		//mousestate = SDL_GetMouseState(&mausx, &mausy);	// get current mouse state

		while(SDL_PollEvent(&event)) {
			if(event.type == SDL_QUIT) {
				running = 0;			// exit 'while' if SDL_QUIT event occurs
			}
		}

		if(keystate[SDLK_ESCAPE]) {
			if(game_map.menue.open()) running = 0;	// open menu and quit if requested
			game_map.timer_reset();	// we have to reset the time counter, because we spent time in the menu
		}
		else if(keystate[key[5]]) {	// scroll key
			if(keystate[key[0]]) game_map.scroll('L');	// scroll one field left, right, down, up
			else if(keystate[key[1]]) game_map.scroll('R');
			else if(keystate[key[3]]) game_map.scroll('D');
			else if(keystate[key[2]]) game_map.scroll('U');
		}
		else if(keystate[key[1]] ) {	// right cursor
			if(keystate[key[4]]) game_map.pickup('R');	// use
			else game_map.move('R');		// move
		}
		else if(keystate[key[0]]) {	// left cursor
			if(keystate[key[4]]) game_map.pickup('L');	// use
			else game_map.move('L');		// move
		}
		else if(keystate[key[2]]) {	// cursor up
			if(keystate[key[4]]) game_map.pickup('U');	// use
			else game_map.move('U');		// move
		}
		else if(keystate[key[3]]) {	// cursor down
			if(keystate[key[4]]) game_map.pickup('D');	// use
			else game_map.move('D');		// move
		}
		else if(keystate[SDLK_SPACE]) {		// space key
			game_map.level_finish(1);		// restart after death or timeout
		}
		else if(keystate[SDLK_LSHIFT] || keystate[SDLK_RSHIFT]) {	// left or right shift
			// empty
		}
		else if(keystate[key[6]]) {	// pause key
			game_map.statusmsg(true);		// stop game - pause mode
		}
		/*else if(mousestate == SDL_BUTTON_LEFT) {	// left mousebutton
			game_map.debug(mausx/30, (mausy/30)-1, 'M');	// debugging with mouse
		}*/

		game_map.draw_screen(0); // draw scene
		game_map.timecounter();	// keep time counter up to date
		game_map.statusmsg();	// if necessary display messages in the status bar

		stop=SDL_GetTicks();	// time at frame end
		if((stop - start) < speed) SDL_Delay(speed - (stop-start));	// if we didn't use up the specified time per frame we wait some ms

		if( time(0) - framestart  > frame_count) {	// display fps as value is calculated
			if(game_map.framerate != -1) game_map.framerate = frames;
			frame_count+=1;
			frames = 0;
		}
		frames += 1;
	}

	cout << " --" << endl << " TuxDash ended " << endl << endl;	// display shut down message
	return 0;
}

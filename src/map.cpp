/*
    TuxDash 0.8
    map.cpp - functions concerning global map/game operations

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

/*
	set default values for all important variables
	read the config file
	set default paths
*/
map::map() {
	frames = NULL;	// frames-array is not initialised yet
	framerate = -1;	// show framerate is deactivated by default
	exit_open = false;	// just some default start arguments ...
	lfinish = false;
	status_visible = true;
	diamonds = 0;
	diamonds_needed = 0;
	animationcounter = 0;
	score = 0;
	time = -2;
	time_limit = 0;
	BGColor.r = 0;
	BGColor.g = 0;
	BGColor.b = 0;
	dimX = dimY = 0;
	viewX = viewY = 0;

	scrolldistX = 3;
	scrolldistY = 2;
	screenX = 21;
	screenY = 16;
	widthX=screenX;
	widthY=screenY;
	scrollamountX = screenX/2;
	scrollamountY = screenY/2;
	fullscreenX = 640;
	fullscreenY = 480;
	bitdepth = 32;

	scroll_state = 0;
	scrolldirection = 0;
	scrolldiff = 0;
	theme = "themes/tux/";

	playerdeath = false;
	magwall_start=-1;
	magwall_duration = 50;
	global_action = 0;	// no global action took place by now

	menue_start = true;	// TuxDash has just been started and the menu shall be displayed

	readconfig(*this);	// read configuration values from the configuration file

	mapfolder = (ostring)TuxHomeDirectory + "/maps/";	// default map folder
	savefolder = (ostring)TuxHomeDirectory + "/savegames/";	// default save folder
	path = mapfolder;		// path starts with the folder
	filename = "default";	// filename is 'default'
	path += filename;		// concatenate path and filename

	cout << endl << " -- TuxDash sucessfully started -- " << endl << endl;	// print status message

	return;
}

/*
	do more initialisation:
		set up sdl window
		open menu
		load random map if necessary
*/
int map::init() {
	int width, height;

	if(fullscreen == true) {	// if fullscreen mode is activated calculate how many fields can be displayed on the screen
		screenX = (int)fullscreenX / 30;
		screenY = (int)(fullscreenY / 30) - 1;	// 30 pixels are reserved for the statusbar
		width = fullscreenX;
		height = fullscreenY;
	}
	else {		// if window mode is activated calculate the necessary screen size for the given width and height
		width = screenX*30;
		height = (screenY*30)+30;	// 30 pixels extra for the statusbar
	}

 	surface sdl_window(width, height, bitdepth, fullscreen);	// create sdl window

	sdl_window.settitle("TuxDash");	// set window title

	widthX=screenX;	// width and height of the map is similar to the screen dimensions (possible smaller values because of a smaller map are set by the loadmap functions)
	widthY=screenY;

	menue.init(*this);	// initialise menu

	if(menue_start == true) {	// if je just started TuXDash
		menue_start = false;	// in future we don't need to open the menu if the screen is reinitialised or something
		loadimages();	// load images from files and add it to the frames array
		if (menue.open() == 1) return 1;		// if the user wanted to quit we return 1 to main
	}

	if(dimX==0 && dimY==0) randommap();	// if the user has not loaded some kind of map we create a random map

	draw_screen(1); 	// draw screen

	return 0;
}

/*
	just delete the frames array
*/
map::~map() {
	delete[] frames;
	return;
}

/*
	load map or savegame from file
*/
int map::loadmap(const ostring& map_name, const int& type) {

	if(global_action == 0 || SDL_GetTicks() - global_action > 300) global_action = 0; // don't load a map more often than every 300 ms (important for shortcuts)
	else return 0;

	short temp;
	bool gameload;
	ifstream file;
	ostring map=map_name;

	if(type == -1) map = filename;	// if type is -1 restart the current map
	else if(type == 0) gameload = false;	// if type is 0 load a map from file
	else gameload = true;	// if type is 1 load a game from file

	if(map.cut(map.len()-4, map.len()-1) != ".txd" && gameload == false) map += ".txd";		// add file ending '.txd' if it is not already so
	else if(map.cut(map.len()-4, map.len()-1) != ".txg" && gameload == true) map += ".txg";	// add file ending '.txg' if it is not already so
	else if(type == -1) {	// in case of restart map determine if it is a savegame or a map
		if(map.cut(map.len()-4, map.len()-1) == ".txd") gameload = false;
		else gameload = true;
	}

	if(!map.IsNull()) {	// copy new path in class variables
		if(gameload == false) {	// copy path for map
			path = mapfolder;
			filename = map;
			path += filename;
			cout << "Load map from file: " << path << endl;	// print status message
			file.open(path);	// open file
			if(!file) {	// error handling
				cout << endl <<  "couldn't open file " << path << ". loading of map failed" << endl;
				return 1;	// exit main
			}
		}
		else {	// copy path for savegame
			path = savefolder;
			filename = map;
			path += filename;
			cout << "Load game from file: " << path << endl;	// print status message
			file.open(path);	// open file
			if(!file) {	// error handling
				cout << endl <<  "couldn't open file " << path << ". loading of game failed" << endl;
				return 1;	// exit main
			}
		}
	}

	char signature[20];	// check file signature (validate version correctness and that this is really a map / savegame)
	file.read(reinterpret_cast<char*>(&signature), sizeof(signature));
	ostring signature2 = signature;	// copy string in ostring

	if(signature2 == "TuxDash Map 1.0" && gameload != false) {
		cout << "Error while loading save game: map file was given as savegame" << endl;
		return 1;
	}
	else if(signature2 == "TuxDash Game 1.0" && gameload != true) {
		cout << "Error while loading map: savegame was given as map file" << endl;
		return 1;
	}
	else if(signature2 != "TuxDash Map 1.0" && signature2 != "TuxDash Game 1.0") {
		cout << "Error while loading: no valid TuxDash file was specified" << endl;
		return 1;
	}

	if(gameload == false) {	// load from map file
		for(int i=0; i<13; i++) {	// read all global data from file
			file.read(reinterpret_cast<char*>(&temp), sizeof(temp));	// read one short from file
			if(i == 0) dimX = temp;
			else if(i == 1) dimY = temp;
			else if(i == 2) posX = temp;
			else if(i == 3) posY = temp;
			else if(i == 4) viewX = temp;
			else if(i == 5) viewY = temp;
			else if(i == 6) exitX = temp;
			else if(i == 7) exitY = temp;
			else if(i == 8) diamonds_needed = temp;
			else if(i == 9) time_limit = temp;
			else if(i==10) lava_percentage = temp;
			else if(i==11) lava_probability = temp;
			else if(i==12) magwall_duration = temp;
		}

		array.init(dimX,dimY, this);	// initialise array with new map dimension
		if(dimX < screenX) widthX = dimX;	// if map is smaller than screen adjust widthX/Y
		else widthX = screenX;	// otherwise width dimension is screen dimension
		if(dimY < screenY) widthY = dimY;
		else widthY = screenY;

		if(viewX + widthX > dimX) viewX = dimX - widthX;	// check against failure in viewX/Y
		if(viewY + widthY > dimY) viewY = dimY - widthY;
		if(viewX < 0) viewX = 0;
		if(viewY < 0) viewY = 0;

		for(int x=0; x<dimX; x++) {	// read in all field values
			for(int y=0; y<dimY; y++) {
				for(int i=0; i<2; i++) {	// read in type and action for every field
					file.read(reinterpret_cast<char*>(&temp), sizeof(temp));

					if(i==0) 	// read in field type
				 		array(x,y).type = temp;
					else if(i==1) 	// read in field action
						array(x,y).action = temp;
				}
				array(x,y).animcount = 0;	// the other field variables are 0
				array(x,y).moved = 0;
				array(x,y).direction = 0;
			}
		}

		playerdeath = true;	// tell level_finish to restart level
		surface::screenfill(0, 0, (screenY+1)*30, screenX*30, BGColor.r, BGColor.g, BGColor.b);	// fill screen black so that nothing is remaining of old maps
		level_finish(2);	// restart level
	}

	else {	// load from savegame file
		for(int i=0; i<27; i++) {	// read all global data from file
			file.read(reinterpret_cast<char*>(&temp), sizeof(temp));	// read one short from file

			if(i == 0) dimX = temp;
			else if(i == 1) dimY = temp;
			else if(i == 2) posX = temp;
			else if(i == 3) posY = temp;
			else if(i == 4) viewX = temp;
			else if(i == 5) viewY = temp;
			else if(i == 6) exitX = temp;
			else if(i == 7) exitY = temp;
			else if(i == 8) diamonds_needed = temp;
			else if(i == 9) time_limit = temp;
			else if(i==10) lava_percentage = temp;
			else if(i==11) lava_probability = temp;
			else if(i==12) magwall_duration = temp;
			else if(i==13) scroll_state = temp;
			else if(i==14) scrolldirection = temp;
			else if(i==15) scrolldiff = temp;
			else if(i==16) exit_open = temp;
			else if(i==17) lfinish = temp;
			else if(i==18) status_visible = temp;
			else if(i==19) auto_scroll = temp;
			else if(i==20) playerdeath = temp;
			else if(i==21) diamonds = temp;
			else if(i==22) score = temp;
			else if(i==23) time = temp;
			else if(i==24) time_limit = temp;
			else if(i==25) lava_amount = temp;
			else if(i==26) magwall_start = temp;

		}

		array.init(dimX,dimY, this);	// reinitialize array with new dimension
		if(dimX < screenX) widthX = dimX;	// if map is smaller than screen adjust widthX/Y
		else widthX = screenX;	// otherwise width dimension is screen dimension
		if(dimY < screenY) widthY = dimY;
		else widthY = screenY;
		if(viewX + widthX > dimX) viewX = dimX - widthX;	// check against failure in viewX/Y
		if(viewY + widthY > dimY) viewY = dimY - widthY;
		if(viewX < 0) viewX = 0;
		if(viewY < 0) viewY = 0;

		for(int x=0; x<dimX; x++) {	// read in all fields
			for(int y=0; y<dimY; y++) {
				for(int i=0; i<5; i++) {
					file.read(reinterpret_cast<char*>(&temp), sizeof(temp));

					if(i==0) {	// read field type
				 		array(x,y).type = temp;
					}
					else if(i==1) {	// read field action
						array(x,y).action = temp;
					}
					else if(i==2) {	// read field moved value
						array(x,y).moved = temp;
					}
					else if(i==3) {	// read field direction value
						array(x,y).direction = temp;
					}
					else if(i==4) {	// read field animcount value
						array(x,y).animcount = temp;
					}
				}
			}
		}
		surface::screenfill(0, 0, (screenY+1)*30, screenX*30, BGColor.r, BGColor.g, BGColor.b);	// fill screen black so that nothing is remaining of old maps
		timer_reset();	// take over time values from the savegame
		draw_screen(1);
	}

	global_action = SDL_GetTicks();	// prevents from doing another loadmap (important when using keys for load/save)

	return 0;
}

	/*
		copy map from source file to target file
	*/

int map::copymap(const ostring& filename) {
	ofstream out_file;
	ostring temp_path;

	if(filename == "") {	// error checking
		cout << "No Filename was given!";
		cout << endl << "-- error in copymap --" << endl;
		return 1;
	}

	temp_path = mapfolder;	// new path is map folder plus filename
	temp_path += filename;
	if(temp_path.cut(temp_path.len()-2, temp_path.len()-1) != ".txd") temp_path += ".txd";	// add '.txd' to path if it does not exist already
	out_file.open(temp_path);

	cout << "-- copy map --" << endl;		// print status message
	ifstream in_file(path);	// open source file

	if(in_file == 0) {	// error checking
		cout << "Couldn't open sourcefile \"" << filename << "\"" << endl;
		cout << endl << "-- error in copymap --" << endl;
		return 1;
	}

	if(out_file == 0) {	// error checking
		cout << "Couldn't open target file \"" << temp_path << "\" for writing " << endl;
		cout << endl << "-- error in copymap --" << endl;
		return 1;
	}

	while(in_file.eof() != true) out_file.put(in_file.get());	// just copy source file to target file byte for byte
	cout << "\t saved map in " << temp_path << endl;	// print final status message
	in_file.close();
	out_file.close();
	cout << " -- success -- " << endl;

	return 0;
}

/*
	save current loaded map in file
	or save current game state as savegame in file
*/

int map::savemap(const ostring& new_name, const bool gamesave) {
	if(SDL_GetTicks() - global_action > 300) global_action = 0;	// we only continue if the last global action is 300 ms ago
	else return 1;

	ofstream file;

	if(new_name == "") {	// no filename was given
		if(gamesave == false) cout << "map::savemap : no filename to save the map was specified " << endl;
		else 	cout << "map::savemap : no filename to save the game was specified " << endl;
		return 1;
	}

	if(gamesave == false) {	// prepare everything to save the map
		filename = new_name;	// copy new path into game variables
		if(filename.cut(filename.len()-2, filename.len()-1) != ".txd") filename += ".txd";	// check if '.txd' must be added
		path = mapfolder;
		path += filename;
		file.open(path);	// open file
		if(file == NULL) {
			cout << "map::savemap :  error while saving map to file '" << path << "'" << endl;
			return 1;
		}
		cout << "saving map as " << path << endl;
	}
	else {	// prepare everything to save the game
		filename = new_name;	// copy new path into game variables
		if(filename.cut(filename.len()-2, filename.len()-1) != ".txg") filename += ".txg";	// check if '.txg' must be added
		path = savefolder;
		path += filename;
		file.open(path);	// open file
		if(file == NULL) {
			cout << "map::savemap : error while saving game to file '" << path << "'" << endl;
			return 1;
		}
		cout << "saving game as " << path << endl;
	}

	global_action = SDL_GetTicks();

	short temp;

	if(gamesave == false) {	// save map to file
		char signature[20] = "TuxDash Map 1.0";	// write signature into map file
		file.write(reinterpret_cast<char*>(&signature), sizeof(signature));
		// save global variables
		for(int i=0; i<13; i++) {
			if(i==0) temp = dimX;
			else if(i==1) temp = dimY;
			else if(i==2) temp = posX;
			else if(i==3) temp = posY;
			else if(i==4) temp = viewX;
			else if(i==5) temp = viewY;
			else if(i==6) temp = exitX;
			else if(i==7) temp = exitY;
			else if(i==8) temp = diamonds_needed;
			else if(i==9) temp = time_limit;
			else if(i==10) temp = lava_percentage;
			else if(i==11) temp = lava_probability;
			else if(i==12) temp = magwall_duration;

			file.write(reinterpret_cast<char*>(&temp), sizeof(temp));
		}

		// save every field

		for(int x=0; x<dimX; x++) {
			for(int y=0; y<dimY; y++) {
					for(int i=0; i<2; i++) {
						if(i==0) temp = array(x,y).type;
						else if(i==1) temp = array(x,y).action;
						file.write(reinterpret_cast<char*>(&temp), sizeof(temp));
					}
			}
		}
	}
	else {	// save game to file
		char signature[20] = "TuxDash Game 1.0";	// write signature into game file
		file.write(reinterpret_cast<char*>(&signature), sizeof(signature));
		// save global variables
		for(int i=0; i<27; i++) {
			if(i==0) temp = dimX;
			else if(i==1) temp = dimY;
			else if(i==2) temp = posX;
			else if(i==3) temp = posY;
			else if(i==4) temp = viewX;
			else if(i==5) temp = viewY;
			else if(i==6) temp = exitX;
			else if(i==7) temp = exitY;
			else if(i==8) temp = diamonds_needed;
			else if(i==9) temp = time_limit;
			else if(i==10) temp = lava_percentage;
			else if(i==11) temp = lava_probability;
			else if(i==12) temp = magwall_duration;
			else if(i==13) temp = scroll_state;
			else if(i==14) temp = scrolldirection;
			else if(i==15) temp = scrolldiff;
			else if(i==16) temp = exit_open;
			else if(i==17) temp = lfinish;
			else if(i==18) temp = status_visible;
			else if(i==19) temp = auto_scroll;
			else if(i==20) temp = playerdeath;
			else if(i==21) temp = diamonds;
			else if(i==22) temp = score;
			else if(i==23) temp = time;
			else if(i==24) temp = time_limit;
			else if(i==25) temp = lava_amount;
			else if(i==26) temp = magwall_start;

			file.write(reinterpret_cast<char*>(&temp), sizeof(temp));
		}

		// save every field

		for(int x=0; x<dimX; x++) {
			for(int y=0; y<dimY; y++) {
					for(int i=0; i<5; i++) {
						if(i==0) temp = array(x,y).type;
						else if(i==1) temp = array(x,y).action;
						else if(i==2) temp = array(x,y).moved;
						else if(i==3) temp = array(x,y).direction;
						else if(i==4) temp = array(x,y).animcount;
						file.write(reinterpret_cast<char*>(&temp), sizeof(temp));
					}
			}
		}
	}
	return 0;
}


/*
	draws only animations or draw the screen once for all fields
*/
void map::draw_screen(const int& type) {
	short xpos=0, ypos=0;	// xpos and ypos contain the correct relative position in pixels of the current field on the screen

	if(scroll_state==1) {				// if scrolling is active add correction values to xpos/ypos
		if(scrolldirection=='R') xpos =  - scrolldiff%30;
		else if(scrolldirection=='L') xpos = scrolldiff%30;
		else if(scrolldirection=='U') ypos = scrolldiff%30;
		else if(scrolldirection=='D') ypos = - scrolldiff%30;
	}

	/* *** only draw the animations *** */
	if(type == 0) {
		rect target, source; // source and target coordinate variables
		short xpos2, ypos2;	// help variables
		int widthX2=widthX, widthY2=widthY, viewX2=viewX, viewY2=viewY;	// help variables
		short frame;	// specifies the frame that has to be blitted

		animationcounter+=1;	// increase counter for animations

		// check fields beyond the visible screen border too, because objects moving from the outside into the screen must be drawn
		if(viewY+widthY <= dimY-1) widthY2+=1;	// if the lower border of the map has not been reached increase Y area by 1
		if(viewY>0) {	// if the upper border of the map has not been reached increase Y area by one
			viewY2-=1;
			widthY2+=1;
		}
		if(viewX+widthX <= dimX-1) widthX2+=1;	// if the right border of the map has not been reached increase X area by 1
		if(viewX>0) {	// if the left border of the map has not been reached increase X area by 1
			viewX2-=1;
			widthX2+=1;
		}

		if(viewX2 + widthX2 >= dimX) widthX2 = dimX - viewX2;	// error checking
		if(viewY2 + widthY2 >= dimY) widthY2 = dimY - viewY2;

		for(int i=viewX2; i<viewX2+widthX2; i++) {	// cycle trough all visible fields including the field rows beyond the border
			for(int j=viewY2; j<viewY2+widthY2; j++) {	// i and j point to the current array / field position

				const short center_type=array(i,j).type, center_action=array(i,j).action;	// copy the type and action of the current field into seperate variables

				if(center_type != -1 && (center_type < 1 || center_type >= 8)) {	// empty fields, grass, barriers or walls are not processed
					short rx=i-viewX, ry=j-viewY;	// relative position on the screen (in fields not in pixels)
					xpos2 = xpos + rx*30;		// relative x,y-values (in pixels)
					ypos2 = ypos + ry*30 + 30;
					target.set(0, 0, 30, 30);
					source.set(0, 0, 30, 30);
					class fields& center=array(i,j);	// pointer to the current field

					// draw diamonds and stones
					if (((center_type==diamond_frame_start || center_type==8) && ((center_action>=4 && center_action<=8) || (center_action>=12 && center_action<=14))) || (center_type==-4 && center.animcount < 5)) {
						if(center_type == -4) center.animcount+=1;	// this is a diamond that is picked up currently by pickup(). animcount is used for a short timeout before the player takes the diamond
						if(center_type!=8) frame = (animationcounter%(diamond_frame_count*diamond_frame_speed) / diamond_frame_speed) + diamond_frame_start;	 // if this is a diamond choose the right animationframe
						else if(center==8) frame = 8;	// if this is a stone just choose the static stone frame

						// correct setting of the coordinates and drawing

						if(center_action==5 || center_action == 13) {	// falling diamond or stone (13 = falling into magic wall)
							ypos2-=center.moved;		// correct ypos by the amount of pixels the object already moved

							if((array(i,j-1) >= explosion_frame_start && array(i,j-1) <= explosion_frame_start+explosion_frame_count)) {		// object is falling out of an explosion
								ypos2+=center.moved;	// we first start drawing at the border of the explosion
								source.set('y', center.moved);
								source.set('h', 30-center.moved);
							}
							else if(center.action == 13) ypos2+=30;	// if object enters magic wall

							// only fill source field black if above the object is an empty field and the field is in the visible screen area
							if((array(i, j-1) < explosion_frame_start || array(i, j-1) > explosion_frame_start+explosion_frame_count) && ypos2-5 >= ypos2+center.moved-30 && (ypos2-5 >=30) &&  (array(i, j-1)==-2 || center_action == 13 ))
								surface::screenfill(xpos2, ypos2-5, 5, 30, BGColor.r, BGColor.g, BGColor.b); // fill source field black

						}
						else if(center_action==7) {	// object topples left
							 xpos2+=center.moved;	// correct x-position of the object

							 if(array(i+1, j) >= explosion_frame_start && array(i+1,j) <= explosion_frame_start+explosion_frame_count) {	// object topples out of an explosion
								xpos2-=center.moved;	// correct values
								source.set('w', 30 - center.moved);
							 }
							 else if(center.moved!=30) {	// fill source field in background color - but not if it is just starting to topple
								if(ypos2>=30) surface::screenfill(xpos2+30, ypos2, 30, 5, BGColor.r, BGColor.g, BGColor.b); // fill source field in background color if in visible screen area
							}
						}
						else if(center_action==8) {	// object topples right
							xpos2-=center.moved;		// correct x-position of the object

							if(array(i-1, j) >= explosion_frame_start && array(i-1,j) <= explosion_frame_start+explosion_frame_count) {	// object topples out of an explosion
								xpos2+=center.moved;	// correct values
								source.set('x', center.moved);
								source.set('w', 30-center.moved);
							}
							else if(center.moved!=30) {		// fill source field in background color - but not if it is just starting to topple
								if(ypos2>=30) surface::screenfill(xpos2-5, ypos2, 30, 5, BGColor.r, BGColor.g, BGColor.b); // fill source field in background color if in visible area
							}
						}

						target.set(xpos2, ypos2);	// set position in target variable

						if((center_action==4 && (animationcounter%diamond_frame_speed==0 || // only blit if the animation frame is changing - important for game speed
						(j>=widthY2+viewY2-2 && scroll_state==1 && scrolldirection=='D') ||	// responsible for correct drawing of diamonds if scrolling is active ...
						(j<=viewY && scroll_state==1 && scrolldirection=='U') ||
						(i>=widthX2+viewX2-2 && scroll_state==1 && scrolldirection=='R') ||
						(i<=viewX && scroll_state==1 && scrolldirection=='L')			// ... end
						)) || (center_action!=4)) {	// if diamond is doing something special blit it to

							if(ypos2>=30 && ypos2 <= (widthY+1)*30+30 && xpos2>=-30 && xpos2 <= widthX*30) {	// only blit if inside visible area
								if(center_action != 13 && center_action != 14) frames[frame].blit(source, target); // blit if there is no magic wall involved
								else {	// special case for magic wall
									if(center_action == 13) source.set(0, 0, center.moved, 30);	// object is entering magic wall
									else if(center_action == 14) source.set(0, center.moved, 30 - center.moved, 30); // object is exiting magic wall
									frames[frame].blit(source, target);	// blit
								}
							}
							else if(ypos2<30 && ypos2 >= 0) { // if object is falling from above into the visible screen area
								if(center_action != 13 && center_action != 14) {	// if nothing special just blit ...
									target.set(xpos2, 30);
									source.set(0, 30-ypos2, ypos2, 30);
									frames[frame].blit(source, target);
								}
								else {	// special case for magic wall
									if(center_action == 13) {	// enter magic wall
										short hoehe;
										if(ypos2 < center.moved) hoehe = ypos2;
										else hoehe = center.moved;
										target.set(xpos2, 30);
										source.set(0, 30-ypos2, hoehe, 30);
										frames[frame].blit(source, target);
									}
									else if(center_action == 14 && j >= viewY) {	// exit magic wall
          									ypos2 = center.moved;
										target.set(xpos2, 30);
										source.set(0, ypos2, 30-ypos2, 30);
										frames[frame].blit(source, target);
									}
								}
							}
						}
					}
					// draw enemies
					else if(center_type==fly_frame_start || center_type==bfly_frame_start || center_type==sfly_frame_start) {
						if(center_action == 2) {	// enemy is moving
							bool fill=true;	// if we fill the source field in background color

							if(center.direction=='R') {	// is moving right
								if(array(i-1,j).type < explosion_frame_start || array(i-1,j).type > explosion_frame_start+explosion_frame_count) {	// everything normal
									xpos2-= (30 - center.moved);	// correct x-position
									if(ypos2>=30) target.set(xpos2-5, ypos2, 30, 5);	// set target position
									else if(ypos2<30 && ypos2>= 0) {	// enemy is moving around the upper screen border
										target.set(xpos2-5, 30, ypos2, 5);	// only draw a part
										fill = false;
									}
									else fill = false;
								}
								else {	// enemy is moving out of an explosion
									source.set(30-center.moved, 0, 30, 30-(30-center.moved));	// correct values
									target.set(xpos2, ypos2, 0 ,0);
									fill = false;
								}

							}
							else if(center.direction=='L') {	// is moving left
								if(array(i+1,j).type < explosion_frame_start || array(i+1,j).type > explosion_frame_start+explosion_frame_count) { // everything normal
									xpos2+= (30 - center.moved);	// correct x-position
									if(ypos2>= 30) target.set(xpos2+30, ypos2, 30, 5);	// set target position
									else if(ypos2<30 && ypos2 >= 0) {	// enemy is moving around the upper screen border
										target.set(xpos2+30, 30, ypos2, 5);	// only draw a part
										fill = false;
									}
									else fill = false;
								}
								else {	// enemy is moving out of an explosion
									source.set(0, 0, 30, center.moved);	// correct values
									target.set(xpos2+(30-center.moved), ypos2, 0, 0);
									fill = false;
								}

							}
							else if(center.direction=='D') {	// is moving down
								if(array(i, j-1).type < explosion_frame_start || array(i, j-1).type > explosion_frame_start+explosion_frame_count) {	// everything normal
									ypos2-= (30 - center.moved);	// correct y-position
									if((ypos2-5) >= 30) target.set(xpos2, ypos2-5, 5, 30);	// set target position
									else if(ypos2-5 < 25) fill = false;	// enemy is moving around the upper screen border
									else if(ypos2-5 < 30) {	// only draw a part
										target.set(xpos2, 30, 30-(ypos2-5), 30);
										fill = false;
									}
									else fill = false;
								}
								else {	// enemy is moving out of an explosion
									source.set(0, 30 - center.moved, 30 - (30-center.moved), 30);	// correct values
									target.set(xpos2, ypos2, 0, 0);
									fill = false;
								}

							}
							else if(center.direction=='U') {	// is moving up
								if(array(i, j+1).type < explosion_frame_start || array(i, j+1).type > explosion_frame_start+explosion_frame_count) {	// everything is normal
									ypos2+= (30 - center.moved);	// correct y-position
									if((ypos2+30) >= 30) {	// set target position
										target.set(xpos2, ypos2+30, 5, 30);	// enemy is moving around the upper screen border
									}
									else if(ypos2+30 < 25) fill = false;
									else if((ypos2+30) < 30) {	// only draw a part
										target.set(xpos2, 30, 5-(30-(ypos2+30)), 30);
									}
									else fill = false;
								}
								else {	// enemy is moving out of an explosion
									source.set(0, 0, center.moved, 30);	// correct values
									target.set(xpos, ypos2+(30-center.moved), 0, 0);
								}
							}

							if(fill == true) surface::screenfill(target, BGColor.r, BGColor.g, BGColor.b);	// fill source field in background color if allowed

						}

						bool draw = true;	// if we draw the frame at all

						if(center_type == fly_frame_start) {		// normal enemy
							if(animationcounter%fly_frame_speed !=0 && center.action != 2 && (scroll_state==0 || (i-viewX>=2 && i-viewX<=widthX-2 && j-viewY>=2 && j-viewY<=widthY-2))) draw= false;	// no need to draw the frame
							else frame = animationcounter%(fly_frame_count*fly_frame_speed)/fly_frame_speed + fly_frame_start;	// choose the next frame to blit
						}
						else if(center_type == bfly_frame_start) {	// butterfly
							if(animationcounter%bfly_frame_speed !=0 && center.action != 2 && (scroll_state==0 || (i-viewX>=2 && i-viewX<=widthX-2 && j-viewY>=2 && j-viewY<=widthY-2))) draw= false;	// no need to draw the frame
							else frame = animationcounter%(bfly_frame_count*bfly_frame_speed)/bfly_frame_speed + bfly_frame_start;	// choose the next frame to blit
						 }
						 else if(center_type == sfly_frame_start) {	// stone fly
							if(animationcounter%sfly_frame_speed !=0 && center.action != 2 && (scroll_state==0 || (i-viewX>=2 && i-viewX<=widthX-2 && j-viewY>=2 && j-viewY<=widthY-2))) draw= false;	// no need to draw the frame
							else frame = animationcounter%(sfly_frame_count*sfly_frame_speed)/sfly_frame_speed + sfly_frame_start;	// choose the next frame to blit
						 }

						if(draw == true && ypos2 >= 30 && ypos2 <= widthY+2*30 && xpos2 >= -30 && xpos2 <= widthX*30) frames[frame].blit(xpos2, ypos2);	// if draw is true blit the frame
						else if(draw == true && ypos2 >=0) {	// only draw a part at the upper border
							rect source(0, 30-ypos2, ypos2, 30);
							rect target(xpos2, 30);
							frames[frame].blit(source,target);
						}
					}

					// animate lava
					else if(center_type == lava_frame_start && center_action == 1) {
					 	frame = animationcounter%(lava_frame_count*lava_frame_speed)/lava_frame_speed + lava_frame_start;	// choose the correct frame
						if((animationcounter%lava_frame_speed == 0 || (scroll_state==1 && (i-viewX<2 || i-viewX>widthX-2 || j-viewY<2 || j-viewY>widthY-2))) && ypos2 >= 30 && ypos2<=(widthY+1)*30 && xpos2>=-30 && xpos2<=widthX*30) frames[frame].blit(xpos2, ypos2);	// only blit the frame if it is needed
						else if (ypos2 < 30) {	// only blit a part of the frame
							source.set(0, 30 - ypos2, ypos2, 30);
							target.set(xpos2, 30);
							frames[frame].blit(source, target);
						}
					}
					// animate magic wall
					else if(center_type == 9 && center_action == 1) {
						// draw normal wall, nothing special
						if(magwall_start==-1 || (::time(0) - magwall_start > magwall_duration))  {
							if(ypos2 >= 30 && ypos2<=(widthY+1)*30 && xpos2>=-30 && xpos2 <=widthX*30) frames[9].blit(xpos2, ypos2);
							else if(ypos2 < 30 && ypos2 >= 0) {	// only blit a part of the frame
								source.set(0, 30-ypos2, ypos2, 30);
								target.set(xpos2, 30);
								frames[9].blit(source, target);
							}
						}
						else {	// wall is active, draw the animated wall
							frame = animationcounter%(mwall_frame_count*mwall_frame_speed)/mwall_frame_speed + mwall_frame_start;	// choose the correct frame
							if((animationcounter%mwall_frame_speed == 0 || scroll_state==1) && ypos2 >= 30 && ypos2<=(widthY+1)*30 && xpos2>=-30 && xpos2 <= widthX*30) frames[frame].blit(xpos2, ypos2);	// only blit the frame if it is needed
							else if(ypos2<30 && ypos2 >=0) {	// only blit a part of the frame
								source.set(0, 30-ypos2, ypos2, 30);
								target.set(xpos2, 30);
								frames[frame].blit(source, target);
							}
						}
					}

					// animate an explosion
					else if(center_type == explosion_frame_start && center_action == 10 && center.direction!=0) {

						frame = center.animcount%(explosion_frame_count*explosion_frame_speed)/explosion_frame_speed + explosion_frame_start;	// choose the correct frame

						if(center.animcount%explosion_frame_speed==0 || frame == explosion_frame_start+explosion_frame_count) {	// only blit the frame if it is needed
							for(int x=i-1; x<=i+1; x++) {	// cycle trough the 9 field block around the explosion
								for(int y=j-1; y<=j+1; y++) {
									if(x>=0 && y>=0 && x<dimX && y<dimY) {	// if x/y is within the map borders
										if((array(x, y).type<2 || array(x, y).type>7) && array(x, y).animcount == center.animcount && array(x,y).type!=10) {	// only draw the explosion if there is no metal wall and if this is really the correct explosion
											xpos2 = xpos + (x-viewX)*30;	// correct values
											ypos2 = ypos + (y-viewY)*30 + 30;
											target.set(xpos2, ypos2, 30, 30);
											if(ypos2>=30 && ypos2<=(widthY+1)*30 && xpos2 >= -30 && xpos2 <= widthX*30) {
												if(frame<explosion_frame_start+explosion_frame_count) frames[frame].blit(target);	// normal blit
												if(frame>=explosion_frame_start+explosion_frame_count) surface::screenfill(target, BGColor.r, BGColor.g, BGColor.b);	// fill field in background color
											}
											else if(ypos2 < 30 && ypos2 >= 0) {	// only draw a part of the frame
												source.set(0, 30 - ypos2, ypos2, 30);
												target.set(xpos2, 30);
												if(frame<explosion_frame_start+explosion_frame_count) frames[frame].blit(source, target);	// blit
												if(frame>=explosion_frame_start+explosion_frame_count)	surface::screenfill(target, BGColor.r, BGColor.g, BGColor.b); // fill in background color
											}
										}
									}
								}
							}
						}
					}

					// choose animation type when player is idle
					else if(center_type==0 && center_action==0) {
						target.set(xpos2, ypos2);	// set position
						center.animcount+=1;		// increase animcount
						if(center.animcount==2) {	// draw player normal again after movement
							if(ypos2>=30) frames[0].blit(target);	// normal blit
							else {	// blit only a part
								source.set(0, 30-ypos2, ypos2, 30);
								target.set(xpos2, 30);
								frames[0].blit(source, target);
							}
						}
						else if(center.animcount > 150) {	// start animation after some time
							center.animcount = 0;			// reset counter
							bool anim_type=rand()%2;	// choose one of two animation types
							if(anim_type == 0) center.action = 2;	// animation 1
							else center.action = 3; // animation 2

						}
					}
					// animate player movement
					else if(center_type==0 && center_action==1 && center.moved>0) {
						if(center.direction=='R') {	// moves right
							xpos2-= 30 - center.moved;	// set position
							frame = animationcounter%(manwalkr_frame_count*manwalkr_frame_speed)/manwalkr_frame_speed + manwalkr_frame_start;	// choose frame
							target.set(xpos2, ypos2);
							xpos2-=5;
						}
						else if(center.direction=='L') {	// moves left
							xpos2+= 30-center.moved;	// set position
							frame = animationcounter%(manwalkl_frame_count*manwalkl_frame_speed)/manwalkl_frame_speed + manwalkl_frame_start;	// choose frame
							target.set(xpos2, ypos2);
							xpos2+=5;
						}
						else if(center.direction=='U') {	// moves up
							ypos2+= 30 - center.moved;	// set position
							frame = animationcounter%(manwalku_frame_count*manwalku_frame_speed)/manwalku_frame_speed + manwalku_frame_start;	// choose frame
							target.set(xpos2, ypos2);
							ypos2+=5;
						}
						else if(center.direction=='D') {	// moves down
							ypos2-= 30 - center.moved;	// set position
							frame = animationcounter%(manwalkd_frame_count*manwalkd_frame_speed)/manwalkd_frame_speed + manwalkd_frame_start;	// choose frame
							target.set(xpos2, ypos2);
							ypos2-=5;
						}

						if(ypos2>=30) surface::screenfill(xpos2, ypos2, 30, 30, BGColor.r, BGColor.g, BGColor.b);	// fill the source field in background color
						else surface::screenfill(xpos2, 30, ypos2, BGColor.r, BGColor.g, BGColor.b);

						if(ypos2>=30) frames[frame].blit(target);	// blit new frame
						else {
							source.set(0, 30-ypos2, ypos2, 30);
							target.set(xpos2, 30);
							frames[frame].blit(source, target);
						}
					}


					// animate player when idle
					else if(center_type == 0 && (center_action==2 || center_action==3) && playerdeath!=true) {	// animation 1 or 2
						short frame;
						if(ypos2 >= 30) {
							target.set(xpos2, ypos2);	// set position
							source.set(0, 0, 30, 30);
						}
						else 	{
							target.set(xpos2, 30);
							source.set(0, 30-ypos2, ypos2, 30);
						}

						if(center_action == 2) frame = animationcounter%(mananim2_frame_count*mananim2_frame_speed)/mananim2_frame_speed + mananim2_frame_start;	// choose frame
						else frame = animationcounter%(mananim1_frame_count*mananim1_frame_speed)/mananim1_frame_speed + mananim1_frame_start;	// choose frame

						center.animcount+=1;		// increase counter

						if((frame == mananim2_frame_start || frame == mananim1_frame_start) && center.animcount>5 && rand()%25 == 0) {	// choose random animation end
							center.action = 0;		// set everything back, idle status again
							center.animcount = 0;
						}

						if(center_action == 0) frames[0].blit(source, target);	// blit normal player frame if animation is over
						else 	frames[frame].blit(source, target);	// othwerwise blit the correct animation frame
					}

					// animate player when 'using'
					else if(center_type==0 && center_action==4) {
						center.animcount+=1;
						/* 	this checks if the player is still 'using'
							in move(...) animcount is set to 0 and here its increased by one
							if animcount is > 1 then the player has stopped 'using'
						*/
						center.moved+=1;
						if(ypos2 >= 30) {	// set position
							target.set(xpos2, ypos2);
							source.set(0, 0, 30, 30);
						}
						else {
							target.set(xpos2, 30);
							source.set(0, 30-ypos2, ypos2, 30);
						}

						if(center.animcount>1) {	// end of 'using' blit the normal player frame again
							center.action = 0;	// set everything back
							center.direction = 0;
							center.moved = 0;
							center.animcount = 0;
							frames[0].blit(source, target);
						}
						else {	// choose next frame
							if(center.direction=='R') frame = center.moved%20/4 + manwalkr_frame_start; // right
							else if(center.direction=='L') frame = center.moved%20/4 + manwalkl_frame_start;	// left
							else if(center.direction=='D') frame = center.moved%20/4 + manwalkd_frame_start; // down
							else frame = center.moved%20/4 + manwalku_frame_start;	// up

							if(ypos2 >= 30) frames[frame].blit(source, target);	// if visible blit the frame
						}
					}

					// player moves stone
					else if(center_type==8 && center_action==11) {
						center.moved+=5;	// move the next 5 pixels

						if(center.moved>30) {	 // movement is finished, set everything back to normal
							center.action = 0;
							center.moved = 0;
							if(center.direction=='r') {	// player was pushing right by 'using'
								array(i-1, j).clear();	// clear the source field of the stone
							}
							else if(center.direction=='l') {	// player was pushing right by 'using'
								array(i+1, j).clear();	// clear the source field of the stone
							}
							center.direction = 0;
						}
						else {	// draw
							// set position
							if(center.direction=='R') xpos2-= (30 - center.moved);	// player is pushing right by walking
							else if(center.direction=='L') xpos2+= (30 - center.moved);	// player is pushing left by walking
							else if(center.direction=='r') {	// player is pushing right by 'using'
								xpos2-= (30 - center.moved);
								if(ypos2>=30) surface::screenfill(xpos2-5, ypos2, 30, 5, BGColor.r, BGColor.g, BGColor.b);
								else surface::screenfill(xpos2-5, 30-ypos2, ypos2, 5, BGColor.r, BGColor.g, BGColor.b);
							}
							else if(center.direction=='l') {	// player is pushing left by 'using'
								xpos2+= (30 - center.moved);
								if(ypos2>=30) surface::screenfill(xpos2+30, ypos2, 30, 5, BGColor.r, BGColor.g, BGColor.b);
								else surface::screenfill(xpos2+30, 30-ypos2, ypos2, 5, BGColor.r, BGColor.g, BGColor.b);
							}

							if(ypos2 >= 30) {	// set rect variables for normal blit
								source.set(0, 0, 30, 30);
								target.set(xpos2, ypos2);
							}
							else {	// set rect variables for partial blit
								source.set(0, 30-ypos2, ypos2, 30);
								target.set(ypos2, 30);
							}

							if(ypos2>=30) frames[8].blit(source, target); 	// blit
						}
					}

					// player is digging ('using') grass away
					else if(center_type==-3 || center_type ==-4) {
						center.animcount+=1;	// wait some frames before the grass is turning into an empty field
						if(center.animcount==6) surface::screenfill(xpos2, ypos2, 30, 30, BGColor.r, BGColor.g, BGColor.b);	// fill the frass field in background color
						else if(center.animcount==12) {	// wait some frames again, before the player can enter the field
							center.clear();		// free field
							if(j!=0) {	// if there is a stone or diamond above the cleared field, let it drop down immediatly
								fields& up = array(i, j-1);
								if( (up.type==8 && up.action==0) || (up==diamond_frame_start && up.action==4) ) {
									center.type = -3;
									center.action = -1;
								}
							}
						}

					}

					// animate player entrance
					else if(center_type==gateway_frame_start && center_action==1) {
						if(ypos2 >= 30)  {	// set position
							target.set(xpos2, ypos2);
						}
						else {
							source.set(0, 30-ypos2, ypos2, 30);
							target.set(xpos2, 30);
						}

						if(center.moved != -1) {	// player is not yet free
							frame = center.animcount%(gateway_frame_count*gateway_frame_speed)/gateway_frame_speed + gateway_frame_start;	// choose frame
							frames[frame].blit(source, target);	// blit frame
						}
						else {				// release player
							// draw a single explosion before the player is free
							frame = center.animcount%(gateway_open_frame_count*gateway_open_frame_speed)/gateway_open_frame_speed + gateway_open_frame_start;	// choose frame
							frames[frame].blit(source, target);	// blit frame
						}

					}

					// animate open exit
					else if(center_type==exit_frame_start && center_action==2 && exit_open==true) {

						frame = animationcounter%(exit_frame_count*exit_frame_speed)/exit_frame_speed + exit_frame_start;	// choose frame

						if(ypos2>=30) {	// normal blit
							target.set(xpos2, ypos2, 30, 30);	// set position
							frames[frame].blit(target);	// blit
						}
						else {	// partial blit
							source.set(0, 30-ypos2, ypos2, 30);	// set position
							target.set(xpos2, 30);
							frames[frame].blit(source, target);
						}
					}
				}
			}
		}

		// if fullscreen mode is active, fill the unused areas at the screen border (only whole fields are drawn)
		if(fullscreen == true) {
			if(fullscreenX%30 != 0) {
				surface::screenfill(fullscreenX-fullscreenX%30, 0, fullscreenY, fullscreenX%30);
			}
			if(fullscreenY%30 != 0) {
				surface::screenfill(0, fullscreenY-fullscreenY%30, fullscreenY%30, fullscreenX);
			}
		}

		surface::refresh();	// SDL screen update, make changes visible
		scroll(0);			// manage scrolling
		physik();	// manage movement of all objects on the map
	}

	/* *** Every frame including static frames will be blitted in this case *** */
	else {
		surface::screenfill(0, 30, surface::w(), surface::h()-30, BGColor.r, BGColor.g, BGColor.b);	// fill the screen in black

		statusbar(5);	// draw the complete statusbar again

		for(int i=viewX; i<viewX+widthX; i++) {	// cycle trough all fields and draw them on the screen
			for(int j=viewY; j<viewY+widthY; j++) {
				if(array(i, j).type >= -1) {
					array.draw(i, j, 0);
				}
			}
		}
	}

	return;
}


/*

//	Debug by clicking on fields with the mouse cursor


void map::debug(short X, short Y, char direction) const {
	if(direction!=0) {
		if(direction=='L') {
			X = posX-1;
			Y = posY;
		}
		else if(direction =='M') {
			X+= viewX;
			Y+= viewY;
		}
	}

	fields& temp = array(X,Y);
	cout << "-- DEBUG Output --" << endl;
	cout << "Field " << X << "/" << Y << endl;
	cout << "Type = " << temp.type<< " Action = " << temp.action << endl;
	cout << "direction = " << temp.direction << " Animcount = " << temp.animcount << endl;
	cout << "moved = " << temp.moved << endl << endl;

	return;
}
*/

/*
	Load BMP's into the frames array
*/
void map::loadimages() {
	int i, count, start;
	ostring path_temp;

	assert(loadtheme());	// read the theme config file in loadtheme. if there occurs an error assert exits

	delete[] frames;

	number_frame_count = 10;	// the number_frame_count is static

	diamond_frame_start = 30;	// start at array element 30 to insert the dynamic frames
	manwalkr_frame_start = diamond_frame_start + diamond_frame_count;	// specifiy correct array indexes for every frame type
	manwalkl_frame_start = manwalkr_frame_start + manwalkr_frame_count;
	manwalku_frame_start = manwalkl_frame_start + manwalkl_frame_count;
	manwalkd_frame_start = manwalku_frame_start + manwalku_frame_count;
	mananim1_frame_start = manwalkd_frame_start + manwalkd_frame_count;
	mananim2_frame_start = mananim1_frame_start + mananim1_frame_count;
	explosion_frame_start = mananim2_frame_start + mananim2_frame_count;
	gateway_open_frame_start = explosion_frame_start + explosion_frame_count;
	gateway_frame_start = gateway_open_frame_start + gateway_open_frame_count;
	exit_frame_start = gateway_frame_start + gateway_frame_count;
	mwall_frame_start = exit_frame_start + exit_frame_count;
	lava_frame_start = mwall_frame_start + mwall_frame_count;
	fly_frame_start = lava_frame_start + lava_frame_count;
	bfly_frame_start = fly_frame_start + fly_frame_count;
	sfly_frame_start = bfly_frame_start + bfly_frame_count;
	number_frame_start = sfly_frame_start + sfly_frame_count;

	frames = new class surface[number_frame_start+number_frame_count+1];	// initialize the frames array with the new frame amount

	frames[0].init(theme + "static/man.bmp");	// the frames below index 30 are static
	frames[1].init(theme + "static/grass.bmp");
	frames[2].init(theme + "static/barrier_v.bmp");
	frames[3].init(theme + "static/barrier_lu.bmp");
	frames[4].init(theme + "static/barrier_ru.bmp");
	frames[5].init(theme + "static/barrier_ll.bmp");
	frames[6].init(theme + "static/barrier_rl.bmp");
	frames[7].init(theme + "static/barrier_h.bmp");
	frames[8].init(theme + "static/stone.bmp");
	frames[9].init(theme + "static/wall.bmp");
	frames[10].init(theme + "static/barrier.bmp");
	frames[11].init(theme + "static/cnt_dmd.bmp");
	frames[12].init(theme + "static/cnt_clk.bmp");
	frames[13].init(theme + "static/cnt_dol.bmp");
	frames[14].init(theme + "static/menu_bg.bmp");

	for(i=0; i<17; i++) {	// load all dynamic frames into the array
		if(i==0) {
			start = diamond_frame_start;
			count = diamond_frame_count;
			path_temp = "anim/diamond";
		}
		else if(i==1) {
			start = manwalkr_frame_start;
			count = manwalkr_frame_count;
			path_temp = "anim/man_walk_r";
		}
		else if(i==2) {
			start = manwalkl_frame_start;
			count = manwalkr_frame_count;
			path_temp = "anim/man_walk_l";
		}
		else if(i==3) {
			start = manwalkd_frame_start;
			count = manwalkd_frame_count;
			path_temp = "anim/man_walk_d";
		}
		else if(i==4) {
			start = manwalku_frame_start;
			count = manwalku_frame_count;
			path_temp = "anim/man_walk_u";
		}
		else if(i==5) {
			start = mananim1_frame_start;
			count = mananim1_frame_count;
			path_temp = "anim/man_anim";
		}
		else if(i==6) {
			start = mananim2_frame_start;
			count = mananim2_frame_count;
			path_temp = "anim/man_flik";
		}
		else if(i==7) {
			start = explosion_frame_start;
			count = explosion_frame_count;
			path_temp = "anim/exp";
		}
		else if(i==8) {
			start = gateway_frame_start;
			count = gateway_frame_count;
			path_temp = "anim/gateway";
		}
		else if(i==9) {
			start = mwall_frame_start;
			count = mwall_frame_count;
			path_temp = "anim/wall_m";
		}
		else if(i==10) {
			start = lava_frame_start;
			count = lava_frame_count;
			path_temp = "anim/lava";
		}
		else if(i==11) {
			start = fly_frame_start;
			count = fly_frame_count;
			path_temp = "anim/fly";
		}
		else if(i==12) {
			start = bfly_frame_start;
			count = bfly_frame_count;
			path_temp = "anim/butterfly";
		}
		else if(i==13) {
			start = sfly_frame_start;
			count = sfly_frame_count;
			path_temp = "anim/stonefly";
		}
		else if(i==14) {
			start = number_frame_start;
			count = number_frame_count;
			path_temp = "static/number_";
		}
		else if(i==15) {
			start = exit_frame_start;
			count = exit_frame_count;
			path_temp = "anim/exit";
		}
		else if(i==16) {
			start = gateway_open_frame_start;
			count = gateway_open_frame_count;
			path_temp = "anim/entrance_open";
		}

		for(int j=start; j<start+count; j++) {
			if(i!=14) frames[j].init(theme + path_temp + (ostring)(j - start + 1) + ".bmp");	// except the static number_? every frame type starts with 1
			else frames[j].init(theme + path_temp + (ostring)(j - start) + ".bmp");
		}
	}


	for(int i=0; i<11; i++) frames[i].colorize(0, 0, 0, BGColor.r, BGColor.g, BGColor.b);	// make black color (0, 0, 0) transparent to background color for every frame
	for(int i=14; i<15; i++) frames[i].colorize(0, 0, 0, BGColor.r, BGColor.g, BGColor.b);
	for(int i=30; i<number_frame_start; i++) frames[i].colorize(0, 0, 0, BGColor.r, BGColor.g, BGColor.b);

	return;
}

/*
	manage player movement
*/
void map::move(const char& direction) {
	if(playerdeath!=false) return;	// if player is death there should'nt move to much any more :-)

	else if(posX==0 && direction=='L') return;	// if the player is at the map border we don't continue
	else if(posY==0 && direction=='U') return;
	else if(posX==dimX-1 && direction=='R') return;
	else if(posY==dimY-1 && direction=='D') return;

	class fields& pos = array(posX, posY);	// reference to the current player position

	// if player is not already moving, set a new moving direction
	if(pos.type==0 && ((pos.action != 1 && pos.direction==0) || pos.action==4)) {
		if(direction=='R') {	// player wants to move right
			fields& right = array(posX+1, posY);
			if(right.type==-1 || right.type==1 || (right.type==diamond_frame_start && right.action==4) || (right.type==exit_frame_start && right.action==2)) {
				if(right.type==diamond_frame_start && right.action == 4) {	// pickup diamond
					right.clear();
					statusbar(0);	// update statusbar information
					statusbar(2);
				}
				else if(right.type==exit_frame_start)	// enter exit
					lfinish = true;

				posX+=1;
			}
			else if((right.type==8 && right.action==0) && posX+2<dimX && (array(posX+2, posY) == -1 || (array(posX+2, posY).type == -2 && array(posX+2, posY).action==2))) { // push stone to the right
				if(rand()%40==0) {	// randomly decide if the stone can start to move
					if(array(posX+2, posY).type == -1) array.push(posX+1, posY, 'R');	// push right by walking
					else {
						array(posX+2, posY).type = -5;
						array(posX+2, posY).direction = 'R';
					}
				}
				else {	// if not start player animation
					if(pos.action!=4) {
						pos.action = 4;
						pos.direction = 'R';
						pos.moved = 0;
					}
					pos.animcount = 0;
				}
				return;
			}
			else return;	// if there is no possibility to push anything

		}
		else if(direction=='L') {	// player wants to move left
			fields& left = array(posX-1, posY);
			if(left.type==-1 || left.type==1 || (left.type==diamond_frame_start && left.action==4) || (left.type==exit_frame_start && left.action==2)) {
				if(left.type==diamond_frame_start && left.action == 4) {	// pickup diamond
					left.clear();
					statusbar(0);	// update statusbar information
					statusbar(2);
				}
				else if(left.type==exit_frame_start)	// enter exit
					lfinish=true;

				posX-=1;
			}
			else if((left.type==8 && left.action==0) && posX-2>=0 && (array(posX-2, posY).type == -1 || (array(posX-2, posY).type == -2 && array(posX-2, posY).action == 2))) { // push stone to the left
				if(rand()%40==0) {	// randomly decide if the stone can start to move
					if(array(posX-2, posY) == -1) array.push(posX-1, posY, 'L');	// push left by walking
					else {
						array(posX-2, posY).type = -5;
						array(posX-2, posY).direction = 'L';
					}
				}
				else {	// if not start player animation
					if(pos.action!=4) {
						pos.action = 4;
						pos.direction = 'L';
						pos.moved = 0;
					}
					pos.animcount = 0;
					return;
				}
			}
			else return;
		}
		else if(direction=='U') {	// player wants to move up
			fields& up = array(posX, posY-1);
			if(up.type==-1 || up.type==1 || (up.type==diamond_frame_start && up.action==4) || (up.type==exit_frame_start && up.action==2)) {
				if(up.type==diamond_frame_start && up.action == 4) {	// pickup diamond
					up.clear();
					statusbar(0);	// update statusbar information
					statusbar(2);
				}
				else if(up.type==exit_frame_start) {	// enter exit
					lfinish=true;
				}
				posY-=1;
			}
			else return;
		}
		else if(direction=='D') {
			fields& down = array(posX, posY+1);
			if(down.type==-1 || down.type==1 || (down.type==diamond_frame_start && down.action==4) || (down.type==exit_frame_start && down.action==2)) {
				if(down.type==diamond_frame_start && down.action == 4) {	// pickup diamond
					down.clear();
					statusbar(0);	// update statusbar information
					statusbar(2);
				}
				else if(down.type==exit_frame_start) {		// enter exit
					lfinish=true;
				}
				posY+=1;
			}
			else return;
		}

		class fields& pos_new = array(posX, posY);
		pos_new.type=0;
		pos_new.direction=direction;	// new direction is the given one
		pos_new.moved=0;	// 0 pixels have been moven right now
		pos_new.action=1;	// set moving as action
		pos_new.animcount=0;

		pos.type = -2;	// type of source field is set to -2
		pos.action=1;
		pos.animcount = 0;
		pos.moved = 0;
		pos.direction = direction;
		auto_scroll = true;	// if the player is not visible on the current screen, auto scroll to his position
	}

	return;
}

/*
	do player movement and animation
*/
void map::playermove() {

	class fields& pos = array(posX, posY);	// reference to the current player position

	if(pos.type==0 && pos.action==1) {	// only go on if the player is currently moving
		class fields* pos_old;	// pointer to the source field of the player

		// determine by moving direction where the source field is
		if(pos.direction=='R')	pos_old = &array(posX-1, posY);
		else if(pos.direction=='L') pos_old = &array(posX+1, posY);
		else if(pos.direction=='U') pos_old = &array(posX, posY+1);
		else if(pos.direction=='D') pos_old = &array(posX, posY-1);

		pos.moved+=5;	// player has moved another 5 pixels
		if(pos_old->type == -2) pos_old->moved+=5;		// copy the move data to the source field
		pos.animcount+=1;	// increase animcount

		if(pos.moved>30) {	// if the player has finished moving one field there is some work to do
			pos.type = 0;
			pos.action = 0;	// on the target field is now the player without any special action
			pos.moved = 0;
			pos.direction = 0;
			pos.animcount = pos_old->animcount;
			if(pos_old->type == -2) pos_old->clear(); // the source field is now epty
			if(lfinish==true) {	// if the player went in the exit we finish the level
				playerdeath=true;
				level_finish(0);
			}
		}
	}
	return;
}


/*
	manage screen scroll movement
*/
void map::scroll(const char& type) {

	if(type != 0 && scroll_state!=1) auto_scroll = false;	// don't do auto scroll
	else if(type != 0 && scroll_state == 1) return;	// if scrolling is already active, return

	if(scroll_state!=1 && auto_scroll == true) {	// auto scroll is active
		scroll_state = 1;	// set scrolling to active
		scrolldiff = 0;	// no pixels are moved till now
		if((posX > viewX+widthX-scrolldistX-1) && (viewX+widthX <= dimX-1)) scrolldirection = 'R';	// set the correct scrolldirection
		else if((posX < viewX+scrolldistX) && (viewX > 0)) scrolldirection = 'L';
		else if((posY > viewY+widthY-scrolldistY-1) && (viewY+widthY <= dimY-1)) scrolldirection = 'D';
		else if((posY < viewY+scrolldistY) && (viewY > 0)) scrolldirection = 'U';
		else scroll_state=0;
	}

	if(scroll_state!=1 && type==0) return;		// if the scroll flag has not been set, do nothing
	else if(type != 0) {
		scrolldirection = type;	// set the given scrolldirection
		scroll_state = 1;	// scrolling is active now
		if(type == 'L' || type == 'R') scrolldiff = (scrollamountX-1)*30;	// set the correct scrolldifference
		else if(type == 'U' || type == 'D') scrolldiff = (scrollamountY-1)*30;
		else cout << "error in: scroll(" << type << ")" << endl;	// if direction is not correct print error message
	}


	// check if scrolling is over
	if(((scrolldirection=='R' || scrolldirection=='L') && scrolldiff >= (scrollamountX * 30)) || (scrolldirection=='R' && viewX + widthX == dimX) || (scrolldirection=='L' && viewX == 0)) {
		// if enough fields have been scrolled (determined by scrollamount) stop scrolling and set everything back
		scroll_state = 0;
		scrolldirection = 0;
		scrolldiff = 0;
		return;
	}
	else if( ((scrolldirection=='D' || scrolldirection=='U') && scrolldiff>= (scrollamountY * 30)) || (scrolldirection=='D' && viewY + widthY == dimY) || (scrolldirection=='U' && viewY == 0) ) {
		// if enough fields have been scrolled (determined by scrollamount) stop scrolling and set everything back
		scroll_state = 0;
		scrolldirection = 0;
		scrolldiff = 0;
		return;
	}

	// do scrolling
	if(scrolldirection=='R') {	// scroll right
		rect quelle(15, 30, (screenY*30), (screenX*30)-15), target(0, 30, (screenY*30), (screenX*30)-15);
		surface::screenblit(quelle, target);	// first move the whole screen
		short x = viewX+widthX;
		for(short y=viewY; y<viewY+widthY; y++) {	// draw the column of fields that scrolls into the screen area
			rect dest(((x-viewX)*30-30)+15 , (y-viewY)*30+30, 30, 15), source(scrolldiff%30, 0, 30, 15);
			if(array(x,y).type >= 0 && array(x,y).type < 30 && array(x,y).action==0) frames[array(x, y).type].blit(source, dest);	//  only blit static fields the others are drawn by draw_screen
			else surface::screenfill(dest, BGColor.r, BGColor.g, BGColor.b);	// otherwise fill the field in background color
		}
	}
	else if(scrolldirection=='L') {	// scroll left
		rect quelle(0, 30, (screenY*30), (screenX*30)-15), target(15, 30, (screenY*30), (screenX*30)-15);
		surface::screenblit(quelle, target);	// first move the whole screen
		short x = viewX-1;
		for(short y=viewY; y<viewY+widthY; y++) {	// draw the column of fields that scrolls into the screen area
			rect dest((x-viewX)*30+30, (y-viewY)*30+30, 30, 15), source(15-scrolldiff%30, 0, 30, 15);
			if(array(x,y).type >= 0 && array(x,y).type < 30 && array(x,y).action==0) frames[array(x, y).type].blit(source, dest);	//  only blit static fields the others are drawn by draw_screen
			else surface::screenfill(dest, BGColor.r, BGColor.g, BGColor.b);	// otherwise fill the field in background color
		}
	}
	else if(scrolldirection=='D') {	// scroll down
		rect quelle(0, 45, (screenY*30)-15, screenX*30), target(0, 30, (screenY*30)-15, screenX*30);
		surface::screenblit(quelle, target);	// first move the whole screen
		short y = viewY+widthY;
		for(short x=viewX; x<viewX+widthX; x++) {	// draw the row of fields that scrolls into the screen area
			rect dest((x-viewX)*30, (y-viewY)*30+15, 15, 30), source(0, scrolldiff%30, 15, 30);
			if(array(x,y).type >= 0 && array(x,y).type < 30 && (array(x,y).action==0)) frames[array(x, y).type].blit(source, dest);	//  only blit static fields the others are drawn by draw_screen
			else surface::screenfill(dest, BGColor.r, BGColor.g, BGColor.b);	// otherwise fill the field in background color
		}
	}
	else if(scrolldirection=='U') {	// scroll up
		rect quelle(0, 30, (screenY*30)-15, (screenX*30)), target(0, 45, (screenY*30)-15, screenX*30);
		surface::screenblit(quelle, target);	// first move the whole screen
		short y = viewY-1;
		for(short x=viewX; x<viewX+widthX; x++) {	// draw the row of fields that scrolls into the screen area
			rect dest((x-viewX)*30, (y-viewY)*30+60, 15, 30), source(0, 15-scrolldiff%30, 15, 30);
			if(array(x,y).type >= 0 && array(x,y).type < 30 && array(x,y).action==0) frames[array(x, y).type].blit(source, dest);	//  only blit static fields the others are drawn by draw_screen
			else surface::screenfill(dest, BGColor.r, BGColor.g, BGColor.b);	// otherwise fill the field in background color
		}
	}

	scrolldiff+=15;	// 15 more pixels have been scrolled

	if(scrolldiff%30==0) {		// a whole row or column has been scrolled into the screen, update view coordinates
		if(scrolldirection=='R') viewX+=1;
		else if(scrolldirection=='L') viewX-=1;
		else if(scrolldirection=='D') viewY+=1;
		else if(scrolldirection=='U') viewY-=1;
	}

	return;
}

/*
	manage object movement on the whole map
*/

void map::physik() {

	playermove();	// move the player

	bool lava_moveable = false;	// indicates if lava is blocked or not
	lava_amount = 0;	// counts the amount of lava on the map

	if(playerdeath==3) return;	// game is paused, nothing moves now

	if(playerdeath == true) {
		if(array(posX, posY).type == gateway_frame_start && array(posX, posY).action == 1) {	// level is starting, player is appearing
			fields& center = array(posX, posY);
			center.animcount += 1;	// increase the counter thus we know how long we have to go on
			if(center.animcount%(gateway_frame_count*2)-1==0 && center.moved != -1) {	// all 21 frames increase the counter by one
				center.moved+=1;
				if(center.moved > 8) {	// when gateway animation was repeated 4 times let player appear
					center.animcount = 0;
					center.moved = -1;
				}
			}

			if(center.moved == -1 && center.animcount == 42) {	// if explosion was animated too, player appears finally
				time = -1;	// start time counter
				status_visible = true;	// show statusbar
				playerdeath = false;	// player is alive
				center = 0;	// gateway is now the player
				center.moved = 0;
				center.direction = 0;
				center.action = 0;
				statusbar(5);	// draw all statusbar elements
				array.draw(posX, posY, 0);
			}
		}
	}

	if(lava_probability > 20 && animationcounter%75 == 0) {	// increases speed of lava growth
		if(lava_prob2 / 25 < 2) {
			lava_probability-=2;
			lava_prob2+=2;
		}
		else if(lava_prob2 / 25 <= 50) {
			lava_probability-= lava_prob2 / 25;
			lava_prob2+= lava_prob2 / 25;
		}
		else 	lava_probability-= 50;

	}
	if(lava_probability < 0) lava_probability = 20;		// check that lava_probability does not turn negative

	for(int X=dimX-1; X>=0; X--) {		// cycle trough the whole game map - very time expensive ...
		for(int Y=dimY-1; Y>=0; Y--) {
			class fields &center = array(X,Y); // reference to the current field
			short center_type=center.type, center_action=center.action;

			// check for diamonds / stones / lava / explosions
			if(center_type >= 30 || center_type == 8) {
				// found explosion center
				if(center.direction!=0 && center_type==explosion_frame_start) array.hit(X, Y, -1);	// manage the explosion in hit()

				// diamond or stone
				else if(center_type == diamond_frame_start || center_type == 8) {
					class fields* down;	// pointer to the field below
					if(Y<dimY-1) down = &array(X, Y+1);

					if(center_action == 5 || center_action == 13 || center_action == 14) { // center is falling down currently (13 and 14 is falling into magic wall)
						center.moved-=5;		// 5 pixel less to fall
						if(center.moved<0) {		// finished falling down
							if(center_action == 5) array.drop_finish(X, Y, 0);	// finish normal drop
							else if (center_action == 13 || center_action == 14) array.magic(X, Y, true);	// finish magic wall drop
						}
					}
					else if(center_action == 7 || center_action == 8) {		// center is toppling
						center.moved-=5;	// 5 pixels less to topple
						if(center.moved<0) 	array.drop_finish(X,Y,1); // finish topple
					}
					else if ((center_type == diamond_frame_start && center_action==4) || center_action==0) {	// center is inactive
						array.drop(X,Y); // check for drop
						array.magic(X, Y, 0); // check for magic wall
						array.topple(X, Y); // check for topple
					}
				}

				else if((center_type == fly_frame_start || center_type == bfly_frame_start || center_type == sfly_frame_start) && center_action == 1) array.ai(X,Y); // control enemies
				else if((center_type == fly_frame_start || center_type == bfly_frame_start || center_type == sfly_frame_start) && center_action == 2) array.ai_move(X, Y); // control enemy movement

				// manage lava
				else if(center_type == lava_frame_start && center_action == 1) {
					lava_amount+=1;	// another field of lava counted
					array.lava_grow(X, Y, lava_moveable);	// manage lava growth
				}
			}
		}
	}

	if(lava_percentage > 0 && lava_amount > ((dimX*dimY)/100) * lava_percentage)	lava_percentage = -1;	// more than x percent of the map are filled with lava
	else if(lava_percentage == -2) {	// turn lava into stone
		lava_percentage = -3;
		if(scroll_state == 1 && scrolldiff%30 != 0) lava_percentage = -2; // if scrolling is active, wait until no partial fields are visible any more, because otherwise there would occur draw errors
		draw_screen(1);
	}

	if(lava_percentage != -3 && lava_moveable!=true && lava_percentage != -2) lava_percentage = -3; // lava is blocked and turns into diamond


	return;
}



/*
manage the statusbar
				type -> draw one the counters
					0 = gathered diamonds
					1 = needed diamonds
					2 = score
					3 = time counter
					5 = draw all counters
*/
void map::statusbar(const short& type, const bool raise) {
	short pluspixel = (int)((screenX - 21) * 30) / 2;	// calculate the center of the screen
	int length, i;
	ostring number;

	if(type == 5) {	// initialise and draw all elements of the statusbar again
		surface::screenfill(0, 0, 29, screenX*30, 0, 0, 0);	// fill the whole bar in black
		statusbar(0, false);	// draw all counters ...
		statusbar(1);
		statusbar(2, false);
		statusbar(3);
		statusbar(4);		// ...
		frames[11].blit(15 + pluspixel, 5);		// draw all symbolds
		frames[11].blit(130 + pluspixel, 5);
		if(framerate != -1) surface::ttf_write("FPS", 20, 255, 255, 255, 495+pluspixel, 3);
		frames[12].blit(395 + pluspixel, 5);
		frames[13].blit(250 + pluspixel, 5);
		surface::screenfill(0, 29, 1, screenX*30, 255, 255, 255); // draw a white line
		return;
	}

	if(raise==true) {		// increase gathered diamonds or score
		if(type == 0) diamonds+=1;	// diamonds
		else if(type == 2) score+=5;	// score
	}

	// depending on type and length allocate an array of the right length
	if(type == 0 || type == 1) {	// diamond counter
		length = 5;	// determines maximum number of digits the number can have
		if(type == 0) number = diamonds;	// gathered diamonds
		else number = diamonds_needed;	// neede diamonds
	}
	else if(type == 2) {	// score counter
		length = 7;	// maximum 7 digits
		number = score;
	}
	else if(type == 3) {	// time counter
		length = 4;	// maximum 4 digits
		if((time == -2 && playerdeath==true) || time == -1) number = time_limit;	// level is just starting and player is not yet alive -> time is not decreasing
		else if(time == -2 && playerdeath == 2) number = 0;		// time is over -> time is constant 0
		else number = time;	// otherwise the remaining time is displayed
	}
	else if(type == 4) {	// framerate counter
		length = 3;	// maximum 3 digits
		number = framerate;
 		if(framerate == -1) length = 0;	// if framerate is deactivated display nothing
	}
	// draw every single digit in the statusbar
	rect pos;
	for(i=0; i<length; i++) {
		if(type == 0) pos.set(135+((i+1)*15)+pluspixel, 5, 0, 0);	// choose the right positions
		else if(type == 1) pos.set(20+((i+1)*15)+pluspixel, 5, 0, 0);
		else if(type == 2) pos.set(253+((i+1)*15)+pluspixel, 5, 0, 0);
		else if(type == 3) pos.set(400+((i+1)*15)+pluspixel, 5, 0, 0);
		else if(type == 4) pos.set(530+((i+1)*15)+pluspixel, 5, 0, 0);
		if(i >= length - number.len() ) frames[number_frame_start+(number[i - (length - number.len())])-48].blit(pos);
		else frames[number_frame_start].blit(pos);
	}

	// draw flash if exit has opened by gathering enough diamonds
	if(exit_open==false && diamonds >= diamonds_needed && scrolldiff%30==0) flash();

	return;
}

/*
	if player pushes stones, digs grass away or picks up diamonds by using the 'use button'
*/
void map::pickup(const char& direction) {
	if(playerdeath == true) return;	// if player is death he can't use things :-)

	fields *target, &pos=array(posX, posY);	// reference to current player position and pointer to target field
	if(pos.action==2 || pos.action==3) pos.action = 0;	// deactivate player animations

	if(pos.action!=0 && pos.action!=4) return;	// if player ist doing something else return
	else if(direction=='R' && posX<dimX-1) target = &array(posX+1, posY);	// set pointer to target field
	else if(direction=='L' && posX>0) target = &array(posX-1, posY);
	else if(direction=='U' && posY>0) target = &array(posX, posY-1);
	else if(direction=='D' && posY<dimY-1) target = &array(posX, posY+1);
	else return;

	if(target->type==1 || (target->type==diamond_frame_start && target->action == 4)) {
		// dig grass away
		if(target->type==1) {
			target->type = -3;	// delay a little
			target->animcount = 0;
		}
		// pickup diamond
		else {
			target->type = -4;	// delay a little
			target->animcount = -5;
			statusbar(0);
			statusbar(2);
		}
		pos.action = 4;
		pos.animcount = 0;
		pos.moved = 0;
		pos.direction = direction;
		return;
	}

	// push stone
	else if( (target->type==8 && target->action==0) &&  ((direction=='R' && (posX < dimX-2) && (array(posX+2, posY)==-1 || (array(posX+2, posY).type == -2 && array(posX+2, posY).action ==2))) || (direction=='L' && posX>1 && (array(posX-2, posY)==-1 || (array(posX-2,posY)==-2 && array(posX-2,posY).action==2))))) {
		if(rand()%40==0) {	// randomly decide if the stone moves or not
			if(direction=='R') {
				if(array(posX+2, posY).type == -1) array.push(posX+1, posY, 'r');
				else {
					array(posX+2, posY).type = -5;
					array(posX+2, posY).direction = 'r';
				}
			}
			else {
				if(array(posX-2, posY).type == -1) array.push(posX-1, posY, 'l');
				else {
					array(posX-2, posY).type = -5;
					array(posX-2, posY).direction = 'l';
				}
			}
		}
	}

	// use exit
	else if( target->type==exit_frame_start && target->action==2 ) {
		lfinish=true;
		playerdeath = true;
		level_finish(0);
	}

	// animate player use
	if(pos.action!=4) {
		pos.action = 4;
		pos.moved = 0;
		pos.animcount = 0;
	}
	pos.direction = direction;
	pos.animcount=0;

	return;
}

// flash the screen as exit opens
void map::flash() {
	rect screen(0, 30, screenY*30, screenX*30);	// the whole screen without the statusbar
	exit_open = true;	// set variable thus other functions know that exit is open
	array(exitX, exitY).action = 2;	// set action variable of exit to 2

	for(int i=0; i<=8; i++) {
		surface::screenfill(screen, i*32, i*32, i*32);	// flash
		surface::refresh();
	}
	surface::screenfill(screen, BGColor.r, BGColor.g, BGColor.b);	// fill in backgroundcolor again
	draw_screen(1);

	return;
}

/*
	finish the current level and prepare everything correctly for loading a new map
	type 0 = exit was used, restart level
	type 1 = restart of map after timout or death
	type 2 = map was loaded, set some values
*/

void map::level_finish(const short& type) {
	if(type == 1 && playerdeath!=2) return;	// if 1 is specified but player is still alive, return

	if(type != 2) time = -1;	// new map is loaded and time is not displayed
	else time = -2;	// time reset

	score = 0;	// score reset
	diamonds = 0;	// diamond reset
	lfinish = false;	// level is not finished any more
	exit_open = false;	// exit is close again

	if(playerdeath!=false && type!=2) loadmap("", -1);	//  if map is not loaded already, restart current map

	magwall_start = -1;	// set back magic walls
	lava_amount = 0;	// set back lava
	lava_prob2 = 0;
	scroll_state=0;			// set back scroll
	scrolldiff=0;
	auto_scroll = true;	// auto_scroll is true so the screen scrolls to the player entrance on the reloaded map

	draw_screen(1);		// draw map

	return;
}

/*
	reset the time counter
*/

void map::timer_reset() {
	if(time > 0) time_limit = time;
	globalstart = ::time(0);
	return;
}

/*
	calculates the time remaining in a level and makes an update trough statusbar()

	enables pause mode

	keeps time constant during level start

	time = -1 --> restart time count
	time = -2 --> time is over
*/
void map::timecounter() {

	if(time == -1 || playerdeath == 3) {		// restart time count or pause
		if(time == -1) time = time_limit;	// restart time
		else time_limit = time;	// keep time

		globalstart = ::time(0);	// level starting time is now
		return;
	}
	else if(playerdeath == 2 && time == -2) return;	// don't do anything with the time because time is over
	else if(status_visible==false) return;	// if statusbar is not visible right now
	else if(time == -2) return;	// time is over

	int current= ::time(0);	// current time
	short temp = (current - globalstart);	// time gone since levelstart
	if( (time_limit-temp) < time) {	// if remaining time is at least a second lower then currently displayed time
		if(time == 0 && (time_limit - temp < 0)) {	// if time_limit is crossed
			time = -2;			// set timeout
			playerdeath = 2;
			return;
		}
		time = time_limit - temp;	// set and draw the time in the statusbar
		statusbar(3);
		statusbar(4);
	}
	return;
}

/*
	create a map with custom parameters
*/

int map::custommap_start(int ndimX, int ndimY, int diamonds, int stones, int grass, int border, int magic_wall, int dur, int lava, int percent, int speed, int flies, int bflies, int sflies, int wall, int metal_wall) {

	if(SDL_GetTicks() - global_action > 300 || global_action==0) global_action = 0;	// only continue if the last global action is 300 ms ago
	else return 1;

	int x1, x2, y1, y2, dia_number=0, empty = 100 - grass - diamonds - stones - flies - bflies - sflies;

	lava_percentage = -1;
	lava_probability = -1;

	time_limit = -1;

	dimX = ndimX;	// set the new dimension
	dimY = ndimY;
	widthX = screenX;	// set the width to screen size
	widthY = screenY;

	// if dimension is less than 3 correct the values
	if(dimX <= 0) dimX = 3;
	if(dimY <= 0) dimY = 3;

	// set dim? to width? if necessary
	if(dimX < widthX) widthX = dimX;
	if(dimY < widthY) widthY = dimY;

	array.init(dimX, dimY, this);		// reinitialize array with new dimensions

	if(border == 0) {	// set the loop limits with border
		x1 = 0;
		x2 = dimX-1;
		y1 = 0;
		y2 = dimY-1;
	}
	else {	// set the loop limits without border
		x1 = 1;
		x2 = dimX-2;
		y1 = 1;
		y2 = dimY-2;
	}

	for(int x=x1; x<=x2; x++) {	// cycle trough all fields and fill them with objects
		for(int y=y1; y<=y2; y++) {
			int i;
			int luck;
			short temp=-2, action=0;
			do {
				for(i=0; i<grass && temp==-2; i++) {	// choose field type according to the given propabilities
					luck = rand();
					if(luck%100 == 0) temp = 1;
				}
				for(i=0; i<diamonds && temp==-2; i++) {
					luck = rand();
					if(luck%100 == 0) {
						temp = diamond_frame_start;
						action = 4;
					}
				}
				for(i=0; i<stones && temp==-2; i++) {
					luck = rand();
					if(luck%100 == 0) temp = 8;
				}
				for(i=0; i<flies && temp==-2; i++) {
					luck = rand();
					if(luck%100 == 0) {
						temp = fly_frame_start;
						action = 1;
					}
				}
				for(i=0; i<bflies && temp==-2; i++) {
					luck = rand();
					if(luck%100 == 0) {
						temp = bfly_frame_start;
						action = 1;
					}
				}
				for(i=0; i<sflies && temp==-2; i++) {
					luck = rand();
					if(luck%100 == 0) {
						temp = sfly_frame_start;
						action = 1;
					}
				}
				for(i=0; i<empty && temp==-2; i++) {
					luck = rand();
					if(luck%100 == 0) temp = -1;
				}
				for(i=0; i<wall && temp==-2; i++) {
					luck = rand();
					if(luck%100 == 0) temp = 9;
				}
				for(i=0; i<metal_wall && temp==-2; i++) {
					luck = rand();
					if(luck%100 == 0) temp = 10;
				}
			}
			while(temp==-2);
			if(temp==diamond_frame_start) dia_number+=1;	// count amount of diamonds on the field
			array(x, y).type = temp;	// set the choosen value type
			array(x, y).action = action;
		}
	}

	if(border==1) {	// if a border is wanted, set it now
		array(0,0).settype(3, 0);
		array(0,dimY-1).settype(5, 0);
		array(dimX-1, 0).settype(4, 0);
		array(dimX-1, dimY-1).settype(6, 0);
		for(int i=1; i<dimX-1; i++) {
			array(i, 0).settype(7, 0);
			array(i, dimY-1).settype(7, 0);
		}
		for(int i=1; i<dimY-1; i++) {
			array(0, i).settype(2, 0);
			array(dimX-1, i).settype(2, 0);
		}
	}

	if(magic_wall >= 1) {	// if a magic wall is wanted, set this as well
		short startx = rand()%(dimX - magic_wall), y=0;	// random x position of wall
		while(y<=1 && y >= dimY-1) y = rand()%dimY;	// random y position of wall

		for(int i = startx; i <= startx + magic_wall; i++) {	// create the wall of the given length
			if(array(i,y) == diamond_frame_start) dia_number-=1;	// correct diamond amount on map if we overwrite one
			array(i,y).settype(9, 1);
			if(array(i,y-1) == diamond_frame_start) dia_number-=1;
			array(i,y-1).settype(1, 0);	// set a row of grass above the wall thus it is not activated accidently at level start
		}
	}

	if(lava == 1) {	// if lava is wanted set it
		speed = (1500 / 50) * (100 - speed);	// calculate a correct speed value
		if(speed <= 0) speed = 1;
		short x = rand()%dimX, y = rand()%dimY;	// set one field of lava somewhere on the map
		if(array(x, y).type == diamond_frame_start) dia_number-=1;
		array(x,y).settype(lava_frame_start, 1);
		if(x>0 && (array(x-1,y).type==fly_frame_start || array(x-1,y).type==sfly_frame_start || array(x-1,y).type==bfly_frame_start)) array(x-1,y).settype(1,0);	// check that there is no enemy touching the lava
		if(x<dimX-1 && (array(x+1,y).type==fly_frame_start || array(x+1,y).type==sfly_frame_start || array(x+1,y).type==bfly_frame_start)) array(x+1,y).settype(1,0);
		if(y>0 && (array(x,y-1).type==fly_frame_start || array(x,y-1).type==sfly_frame_start || array(x,y-1).type==bfly_frame_start)) array(x,y-1).settype(1,0);
		if(y<dimY-1 && (array(x,y+1).type==fly_frame_start || array(x,y+1).type==sfly_frame_start || array(x,y+1).type==bfly_frame_start)) array(x,y+1).settype(1,0);

		lava_percentage = percent;
		lava_probability = speed;

	}

	int x=rand()%dimX, y=rand()%dimY;	// choose exit on map
	array(x, y).type = exit_frame_start;
	array(x, y).action = 0;
	exitX = x;
	exitY = y;

	x = rand()%dimX, y=rand()%dimY;	// choose player position on map
	array(x, y).type = gateway_frame_start;
	array(x, y).action = 1;
	posX = x;
	posY = y;
	viewX = rand()%dimX;	// choose viewing position randomly
	viewY = rand()%dimY;
	if(viewX < 0) viewX = 0;	// validate view position
	if(viewY < 0) viewY = 0;
	if(viewX > dimX-widthX) viewX = dimX-widthX;
	if(viewY > dimY-widthY) viewY = dimY-widthY;

	if(x>0 && array(x-1,y)>lava_frame_start) array(x-1,y).settype(1,0);		// no enemies should touch the player in the first place
	if(x<dimX-1 && array(x+1,y)>lava_frame_start) array(x+1,y).settype(1,0);
	if(y>0 && array(x,y-1)>lava_frame_start) array(x,y-1).settype(1,0);
	if(y<dimY-1 && array(x,y+1)>lava_frame_start) array(x,y+1).settype(1,0);
	// simple check if the player can't move in the beginning
	// it would be a rather hard task to validate that a level is solvable
	if((x==0 || (array(x-1,y)!=1 && array(x-1,y)!=4 && array(x-1,y)!=-1)) && (x==dimX-1 || (array(x+1,y)!=1 && array(x+1,y)!=4 && array(x+1,y)!=-1)) &&
		(y==0 || (array(x,y-1)!=1 && array(x,y-1)!=4 && array(x,y-1)!=-1)) && (y==dimY-1 || (array(x,y+1)!=1 && array(x,y+1)!=4 && array(x,y+1)!=-1))) {
		if(x!=0) array(x-1,y).settype(1, 0);
		else if(x!=dimX-1) array(x+1,y).settype(1,0);
		else if(y!=0) array(x,y-1).settype(1,0);
		else if(y!=dimY-1) array(x,y+1).settype(1,0);
	}

	return dia_number;	// return amount of diamonds on map and let user decide how much time is available and how many diamonds are needed (done by custommap_finish)
}

/*
	finishes the creation of a custom map
	takes time and needed diamonds to all information to save the map to file
*/

void map::custommap_finish(int time, int needed) {

	if(needed <= 0) needed = 1;	// set needed diamonds to 1 at least
	diamonds_needed = needed;
	time_limit = time;
	if(time_limit <= 0) time_limit = needed*2;
	if(time_limit > 9999) time_limit = 9999; 	// only 4 digits for time - should be enough

	surface::screenfill(0, 0, (screenY+1)*30, screenX*30, BGColor.r, BGColor.g, BGColor.b);	// fill the screen in background color
	playerdeath = true;
	level_finish(2);	// set all variables to levelstart
	savemap("random");	// save map in random
	global_action = SDL_GetTicks();

	return;
}


/*
	displays messages in the statusbar
	activates/deactivates pause mode
*/

const void map::statusmsg(const bool pause=false) {
	short pluspixel = (int)((screenX - 21) * 30) / 2;
	rect pos(pluspixel,0);

	if(SDL_GetTicks() - global_action > 300 && pause == true && (playerdeath==false || playerdeath==3)) {	// only continue if the last global action is more than 300 ms ago
		if(playerdeath == 3) {	// if pause was already activated, pause is over now
			status_visible=true;
			playerdeath = false;
			statusbar(5);
		}
		else playerdeath = 3; // otherwise pause is started

		global_action = SDL_GetTicks();	// new global action
	}

	if(playerdeath!=false) {	// if player is dead / game is paused / level is starting
		if(animationcounter%150==0) {	// every 150. frames display the game state
			if(status_visible==true) {		// draw message
				surface::screenfill(pos.get('x'), pos.get('y'), 29, 630, 0, 0, 0);	// fill statusbar black
				pos.set('x', pluspixel+100);
				ostring text;
				status_visible=false;	// counter are not visible

				if(playerdeath==3) 	// draw pause message
					text = "PAUSE - Press 'p' to continue";
				else if(time==-2 && playerdeath==true)
					text = "TuxDash Version 0.8 Beta";	// draw TuXDash info on level start
				else if(playerdeath==2 && time!=-2)
					text = "Player is dead. Press SPACE to continue";	// draw death message
				else if(playerdeath==2 && time==-2) // Timeout anzeigen
					text = "Time is over. Press SPACE to continue";	// draw timeout message
				surface::ttf_write(text, 24, 255, 255, 255, pos);	// draw message in white

			}
			else {				// switch back to counters
				status_visible=true;	// counters visible, draw in statusbar
				statusbar(5);
			}
		}
	}

	return;
}

/*
	chooses random parameters to create a map with custommap
*/

void map::randommap() {
	// variables for the different level properties
	int dX, dY, stones=0, grass=0, diamonds=0, nflies=0, bflies=0, sflies=0, duration, percent, speed, magic_wall, wall=0, metal_wall=0, empty=100;
	// determine if enemies, level border or lava are activated
	bool enemies, border, lava;

	// choose size of map

	dX = rand()%200;	// maximum of 200 x 200 sized maps
	dY = rand()%200;
	if(dY < 3) dY = 3;	// minimum of 3 x 3 sized maps
	if(dX < 3) dX = 3;

	// choose terrain amounts on the map randomly
	empty -= grass = rand()%(empty);
	if(empty > 0) empty -= stones = rand()%(empty);
	if(empty > 0) empty -= diamonds = rand()%(empty);

	if((enemies = rand()%2) == 1) {	// decide randomly if enemies are on the map at all
		if(empty > 0) empty -= nflies = rand()%(empty);	// choose enemy types randomly
		if(empty > 0) empty -= bflies = rand()%(empty);
		if(empty > 0) empty -= sflies = rand()%(empty);
	}

	if((wall = rand()%2) == 1) if(empty > 0) empty -= wall = rand()%(empty);	// decide randomly if walls are on the map
	if((metal_wall = rand()%2) == 1) if(empty > 0) empty -= metal_wall = rand()%(empty);	// ... metal walls

	border = rand()%2;	// decide if a metal border is around the map

	lava = rand()%10;	// decide if lava is active

	if(lava == 1) {	// if lava is active
			speed = rand()%100;	// determine speed
			int limit=100;
			if(speed > 0) limit = 80;	// depending on the growing speed the lava limit is higher or lower
			if(speed > 25) limit = 60;
			if(speed > 50) limit = 40;
			if(speed > 75) limit = 20;
			percent = rand()%limit;	// choose randomly withing the limit-range a limit value
	}
	else {	// if lava is inactive, set variables to 0
		lava = 0;
		speed = 0;
		percent = 0;
	}

	magic_wall = random()%10;	// decide if a magic wall is in the level

	if(magic_wall == 1 && dimX > 4) {	// ... if there is
		magic_wall = rand()%dimX-4;	// choose random length
		duration = rand()%150;	// choose random activity length
	}
	else {
		magic_wall = 0;	// otherwise set values to 0
		duration = 0;
	}

	int needed, time, dia_exist;

	// do the first step of cerating the map, custommap_start returns diamonds on map
	dia_exist = custommap_start(dX, dY, diamonds, stones, grass, border, magic_wall, duration, lava, percent, speed, nflies, bflies, sflies, wall, metal_wall);

	// choose a amount of diamonds needed
	needed = (int)(dia_exist * 0.65 + bflies * 6);

	// choose a time limit
	time = (int)((1.4 * needed) - pow(needed, 0.7) + pow(stones, 1.5) - pow(empty, 1.3) + pow(nflies+bflies+sflies, 3.5) + (100*pow(0.97, dia_exist)));
	if(time <= 0) time = (int)(needed*1.25);
	if(time > 9999) time = 9999; 	// only 4 digits for time available

	custommap_finish(time, needed);	// finish map creation

	return;
}

/*
	 open the themecfg file and load values for frames
*/

int map::loadtheme() {
	ifstream config(theme + "themecfg");	// open config file

	if(!config) {	// error checking
		cout << "Warning: Couldn't find configuration file 'themecfg' for theme '" << theme << "' in folder " << theme << endl;	// print error message
		ostring TempString;

		config.clear();	// Try to find the theme is users home directory
		config.open((ostring)TuxHomeDirectory + "/" + theme);
		cout << "Try to find theme '" << theme << "' in " << TuxHomeDirectory << endl;
		if(!config) cout << "Could'nt find it there neither" << endl;

		if(!config && theme != "themes/original") {	// check if the original theme can be found
			theme = "themes/original/";
			config.clear();
			config.open(theme + "themecfg");
			cout << "Falling back to default theme 'original'" << endl;	// print status message
		}
		if(!config) {
			cout << "Could'nt find a valid theme package. Exit TuxDash " << endl;	// if original theme could not be found, exit
			return 0;
		}
	}

	int line=0;
	char temp;
	ostring property, value;

	for(int i=0; config.eof() != true; i++) {
		config.get(temp);
		if(i==0 && temp == '#') {		// skip comment line
			do config.get(temp);
			while(temp!='\n' && config.eof() != true);
			line+=1;	// next line
			i=-1;	// start a new line
		}
		else if(temp != '\n' && temp != ' ') {
			property(i, temp);	// neither line end nor space character found
		}
		else if(temp == ' ') {	// found space character
			property(i, '\0');	// terminate property string
			config.get(temp);	// check the next character
			if(temp == '=') {
				config.get(temp);
				if(temp == ' ') {	// know get the value
					i=0;
					while(temp != '\n' && config.eof() != true) {	// get the value till newline or eof occurs
						config.get(temp);
						if(temp != '\n') value(i, temp);
						i+=1;
					}

					value(i-1, '\0');
					i = -1;	// next cycle we have a new line
					line+=1;	// next line

					// check starts here
					int value_int = atoi(value);	// get int value
					property.lower();	// lower the property string
					if(property == "diamond_frames") diamond_frame_count = value_int;	// load the specified values
					else if(property == "diamond_speed") diamond_frame_speed = value_int;
					else if(property == "playeranim1_frames") mananim1_frame_count = value_int;
					else if(property == "playeranim1_speed") mananim1_frame_speed = value_int;
					else if(property == "playeranim2_frames") mananim2_frame_count = value_int;
					else if(property == "playeranim2_speed") mananim2_frame_speed = value_int;
					else if(property == "playerwalk_r_frames") manwalkr_frame_count = value_int;
					else if(property == "playerwalk_r_speed") manwalkr_frame_speed = value_int;
					else if(property == "playerwalk_l_frames") manwalkl_frame_count = value_int;
					else if(property == "playerwalk_l_speed") manwalkl_frame_speed = value_int;
					else if(property == "playerwalk_d_frames") manwalkd_frame_count = value_int;
					else if(property == "playerwalk_d_speed") manwalkd_frame_speed = value_int;
					else if(property == "playerwalk_u_frames") manwalku_frame_count = value_int;
					else if(property == "playerwalk_u_speed") manwalku_frame_speed = value_int;
					else if(property == "explosion_frames") explosion_frame_count = value_int;
					else if(property == "explosion_speed") explosion_frame_speed = value_int;
					else if(property == "lava_frames") lava_frame_count = value_int;
					else if(property == "lava_speed") lava_frame_speed = value_int;
					else if(property == "magicwall_frames") mwall_frame_count = value_int;
					else if(property == "magicwall_speed") mwall_frame_speed = value_int;
					else if(property == "entrance_frames") gateway_frame_count = value_int;
					else if(property == "entrance_speed") gateway_frame_speed = value_int;
					else if(property == "entranceopen_frames") gateway_open_frame_count = value_int;
					else if(property == "entranceopen_speed") gateway_open_frame_speed = value_int;
					else if(property == "exit_frames") exit_frame_count = value_int;
					else if(property == "exit_speed") exit_frame_speed = value_int;
					else if(property == "fly_frames") fly_frame_count = value_int;
					else if(property == "fly_speed") fly_frame_speed = value_int;
					else if(property == "butterfly_frames") bfly_frame_count = value_int;
					else if(property == "butterfly_speed") bfly_frame_speed = value_int;
					else if(property == "stonefly_frames") sfly_frame_count = value_int;
					else if(property == "stonefly_speed") sfly_frame_speed = value_int;
					else if(property == "bgcolor_r") BGColor.r = value_int;
					else if(property == "bgcolor_g") BGColor.g = value_int;
					else if(property == "bgcolor_b") BGColor.b = value_int;
					else if(property == "textcolor1_r") menue.text_color1.r = value_int;
					else if(property == "textcolor1_g") menue.text_color1.g = value_int;
					else if(property == "textcolor1_b") menue.text_color1.b = value_int;
					else if(property == "textcolor2_r") menue.text_color2.r = value_int;
					else if(property == "textcolor2_g") menue.text_color2.g = value_int;
					else if(property == "textcolor2_b") menue.text_color2.b = value_int;
					else if(property == "textselect_r") menue.text_select_color.r = value_int;
					else if(property == "textselect_g") menue.text_select_color.g = value_int;
					else if(property == "textselect_b") menue.text_select_color.b = value_int;
					else if(property == "boxbg_r") menue.boxbg_color.r = value_int;
					else if(property == "boxbg_g") menue.boxbg_color.g = value_int;
					else if(property == "boxbg_b") menue.boxbg_color.b = value_int;
					else if(property == "boxbg2_r") menue.boxbg_color2.r = value_int;
					else if(property == "boxbg2_g") menue.boxbg_color2.g = value_int;
					else if(property == "boxbg2_b") menue.boxbg_color2.b = value_int;
					else {	// if the property could not be identified, print out an error message
						cout << "Warning: error in line " << line << " in theme file " << endl;
						cout << "Line says: property: " << property << " value: " << value << endl;
					}
					// check ends here

				}
			}
		}
		else if(temp == '\n') {
			i = 0;	// eof is not reached, check the next argument
			line+=1;	// next line
		}

	}

	return 1;
}

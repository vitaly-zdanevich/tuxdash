/*
    TuxDash 0.8
    rect.cpp - simple interface/class for SDL_Rect variables

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

/*
	construct a default rect variable with 0 as values
*/

rect::rect() {
	rectangle.x = rectangle.y = rectangle.h = rectangle.w = 0;
	return;
}

/*
	construct a rect variable with given x and y variables
*/

rect::rect(const short x, const short y) {
	rectangle.x = x;
	rectangle.y = y;
	rectangle.h = 0;
	rectangle.w = 0;
	return;
}

/*
	construct a rect variable with all four variables
*/

rect::rect(const short x, const short y, const short h, const short w) {
	rectangle.x = x;
	rectangle.y = y;
	rectangle.h = h;
	rectangle.w = w;
	return;
}

/*
	set new values for rect variable
*/

void rect::set(const short x, const short y, const short h=0, const short w=0) {
	rectangle.x = x;
	rectangle.y = y;
	rectangle.h = h;
	rectangle.w = w;
	return;
}

/*
	set a single attribute to a new value
	specified by char
*/

void rect::set(const char var, const short value) {
	if(var=='x') rectangle.x = value;
	else if(var=='y') rectangle.y = value;
	else if(var=='w') rectangle.w = value;
	else if(var=='h') rectangle.h = value;
	return;
}

/*
	return a single attribute
	specified by char
*/

short rect::get(const char var) const {
	if(var=='x') return rectangle.x;
	else if(var=='y') return rectangle.y;
	else if(var=='h') return rectangle.h;
	else if(var=='w') return rectangle.w;
	else {
		cout << "rect::get : Ung�ltige Anforderung" << endl;
		return -1;
	}
}

/*
	get values for rect variable from standard SDL_Rect variable
*/

void rect::operator=(const SDL_Rect sdlrect) {
	rectangle.x = sdlrect.x;
	rectangle.y = sdlrect.y;
	rectangle.h = sdlrect.h;
	rectangle.w = sdlrect.w;
	return;
}

// cast rect to SDL_Rect

rect::operator SDL_Rect() const {
	return rectangle;
}

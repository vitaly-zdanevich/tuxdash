/*
    TuxDash 0.8
    rect.h - simple interface/class for SDL_Rect variables

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

#ifndef _rect_h
#define _rect_h 

#include <SDL.h>

using namespace std;

class rect {
		friend class surface;
		SDL_Rect rectangle;
	public:
		rect();	// construct empty rect variable
		rect(const short, const short);	// construct rect variable out of x and y value
		rect(const short, const short, const short, const short);	// construct rect variable out of all four variables
		void set(const short, const short, const short, const short);	// set all rect attributes new
		void set(const char, const short);		// change a single rect value
		short get(const char) const;		// return a single rect value
		void operator=(SDL_Rect);	// set values from standard sdl_rect variable
		operator SDL_Rect() const;	// cast rect to SDL_Rect
};

#include "rect.cpp"
#endif /* _rect_h */

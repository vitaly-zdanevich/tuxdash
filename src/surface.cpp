/*
    TuxDash 0.8
    surface.cpp - interfaces/class for SDL_Surface and SDL functions

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

class surface surface::window; // static pointer to screen surface

short surface::width=0;
short surface::height=0;
ostring surface::font;
bool surface::alphaset=false;

/*
	default constructor
	set area to NULL
*/

surface::surface() {
	area = NULL;
	return;
}

/*
	setup sdl window
*/

surface::surface(const short new_width, const short new_height, const int new_colordepth, const bool fullscreen) {

	width = new_width;
	height = new_height;

	if(width<=1280 && height <=1024) {	// only allow 1280 x 1024
		SDL_Init(SDL_INIT_VIDEO);	// initialise SDL_video
		atexit(SDL_Quit);		// prepare to clean up SDL initalization after termination of programm
		TTF_Init();		// initialize SDL_TTF library
		atexit(TTF_Quit);	// preparbe to clean up SDL_TTF initializastion after termination of programm
		if(fullscreen == false) area=SDL_SetVideoMode(width, height, new_colordepth, SDL_SWSURFACE|SDL_SRCALPHA);	// set SDL window
		else area = SDL_SetVideoMode(width, height, new_colordepth, SDL_SWSURFACE|SDL_SRCALPHA|SDL_FULLSCREEN);	// set SDL fullscreen mode
		window.area = area;	// set window object area to screen
		if(area==NULL) cout << "Error while setting up video mode!" << endl;
	}
	else {
		cout << "screen size is too big. please choose a maximum of 1280 x 1024" << endl;
	}
}

/*
	construct object by loading surface area from file
*/

surface::surface(const ostring& file) {
	area = NULL;
	init(file);
	return;
}

/*
	construct object by loading ttf surface into surface area
*/

surface::surface(const ostring& file, const ostring& text, int size, int r, int g, int b) {
	area = NULL;
	font_init(file, text, size, r, g, b);
	return;
}

/*
	set surface area to ttf surface
*/

void surface::font_init(const ostring &file, const ostring &text, int size, int r, int g, int b) {
	ostring file_tmp;
	if(file == "") file_tmp = font;
	else file_tmp = file;

	TTF_Font *font = TTF_OpenFont(file_tmp, size);
	SDL_Color color = {r, g, b, 0};

	area = TTF_RenderText_Solid(font, text, color);

	TTF_CloseFont(font);

	return;
}

/*
	set global ttf file
*/

void surface::set_ttf(const ostring &file) {
	font = file;
	return;
}

/*
	construct a temporary SDL_Surface, set it to ttf surface and blit it on the screen
*/

void surface::ttf_write(const ostring &text, int size, int r, int g, int b, class rect& pos) {
	SDL_Surface* text_surface;
	SDL_Rect temp;
	TTF_Font *font_tmp = TTF_OpenFont(font, size);
	SDL_Color farbe = {r, g, b, 0};
	text_surface = TTF_RenderText_Solid(font_tmp, text, farbe);
	TTF_CloseFont(font_tmp);
	temp = pos;
	SDL_BlitSurface(text_surface, 0, window.area, &temp);
	SDL_FreeSurface(text_surface);
	return;
}

/*
	same as above but with int coordinates rather than rect
*/

void surface::ttf_write(const ostring &text, int size, int r, int g, int b, int x, int y) {
	rect pos(x, y, 0, 0);
	ttf_write(text, size, r, g, b, pos);
	return;
}

/*
	load a new BMP from file into object surface
*/

void surface::init(const ostring &file) {
	SDL_Surface *temp = SDL_LoadBMP(file);
	if(temp==NULL) {
		cout << "surface::init : error while loading surface from BMP with path " << file << endl;
		return;
	}
	area = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	return;
}

/*
	change surface into screen format for fast blitting for all bitdepths
*/

void surface::displayformat() {
	SDL_Surface *temp = area;
	area = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);
	return;
}

/*

*/

surface::surface(const ostring& file, const class rect &target) {
	area=SDL_LoadBMP(file);

	SDL_Rect pos;
	pos.h =  area->h;
	pos.w = area->w;
	pos.x = target.get('x');
	pos.y = target.get('y');
	blit(pos);
	return;
}

/*
	destructor
	deletes SDL_Surfaces if not NULL
*/

surface::~surface() {
	if(area!=NULL && area!=window.area) SDL_FreeSurface(area);
	return;
}

/*
	fill screen at given position in given color
*/

void surface::screenfill(short x, short y, short h=0, short w=0, unsigned char r=0, unsigned char g=0, unsigned char b=0) {
	SDL_Rect target;
	Uint32 color=SDL_MapRGB(window.area->format, r, g, b);
	target.x = x;
	target.y = y;
	target.h = h;
	target.w = w;
 	if(x==-1 && y==-1 && h==-1 && w==-1) {
		target.h = window.area->h;
		target.w = window.area->w;
	}
	SDL_FillRect(window.area, &target, color);
	return;
}

/*
	same as above but with position given as rect instead of int's
*/

void surface::screenfill(const class rect &pos, unsigned char r=0, unsigned char g=0, unsigned char b=0) {
	SDL_Rect target;
	target = pos;
	Uint32 color=SDL_MapRGB(window.area->format, r, g, b);
	if(target.x==0 && target.y==0 && target.h==0 && target.w==0) {	// fill whole screen if all coordinates are 0
		target.h = window.area->h;
		target.w = window.area->w;
	}
	SDL_FillRect(window.area, &target, color);
	return;
}

/*
	fill area on object surface in given color
*/

void surface::fill(short x, short y, short h, short w, unsigned char r, unsigned char g, unsigned char b) {
	SDL_Rect target;
	Uint32 color=SDL_MapRGB(window.area->format, r, g, b);
	target.x = x;
	target.y = y;
	target.h = h;
	target.w = w;
	if(x==0 && y==0 && h==0 && w==0) {	// fill whole surface if all coordinates are 0
		target.h = window.area->h;
		target.w = window.area->w;
	}
	SDL_FillRect(window.area, &target, color);
	return;
}

/*
	same as above but with SDL_rect given instead of int's
*/

void surface::fill(const SDL_Rect &target, unsigned char r, unsigned char g, unsigned char b) {
	SDL_Rect temp;
	temp = target;
	Uint32 color;
	color=SDL_MapRGB(window.area->format, r, g, b);
	SDL_FillRect(area, &temp, color);
	return;
}

/*
	same as above but with rect given instead of SDL_rect or int's
*/

void surface::fill(const class rect &target, unsigned char r, unsigned char g, unsigned char b) {
	Uint32 color;
	color=SDL_MapRGB(window.area->format, r, g, b);
	SDL_Rect target2 = target;

	SDL_FillRect(window.area, &target2, color);
	return;
}

/*
	make changes on screen surface visible with SDL_Flip
*/

void surface::refresh() {
	if(window.area!=NULL) SDL_Flip(window.area);
	else cout << "surface::refresh : video mode has not been set yet!" << endl;
}

/*
	blit surface at given coordinates on screen
*/

void surface::blit(short x, short y) {
	rect target(x, y, 0, 0);
	blit(target);
	return;
}

/*
	same as above but coordinates are given as rect instead of int's
*/

void surface::blit(const class rect &target) {
	SDL_Rect temp;
	temp = target;
	SDL_BlitSurface(area, 0, window.area, &temp);
	return;
}

/*
	same as above but coordinates are given as SDL_Rect instead of int's or rect
*/

void surface::blit(const SDL_Rect& target) {
	SDL_Rect pos;
	pos = target;
	SDL_BlitSurface(area, 0, window.area, &pos);
	return;
}


/*
	blit source rect from surface at given coordinates on screen
*/

void surface::blit(const class rect &source, const class rect &target) {
	SDL_Rect src, trg;
	src = source;
	trg = target;
	SDL_BlitSurface(area, &src, window.area, &trg);
	return;
}

/*
	same as above but with coordinates given as SDL_Rect instead of rect
*/
void surface::blit(const SDL_Rect &source, const SDL_Rect &trg) {
	SDL_Rect src, target;
	src = source;
	target = trg;
	SDL_BlitSurface(area, &src, area, &target);
	return;
}


/*
	blit a source rect of the screen onto itself
*/

void surface::screenblit(const rect& source, const rect& target) {
	SDL_Rect src, trg;
	src = source;
	trg = target;

	SDL_BlitSurface(window.area, &src, window.area, &trg);

	return;
}

/*
	set alpha value for surface
*/

void surface::setalpha(const unsigned char& value) {
	if(alphaset==false) {
		if(SDL_SetAlpha(window.area, SDL_SRCALPHA, value)!=0) cout << "surface::setalpha : could not set alpha channel on screen surface" << endl;

		alphaset=true;
		return;
	}

	if(SDL_SetAlpha(window.area, SDL_SRCALPHA, value)!=0) cout << "surface::setalpha : could not set alpha channel on screen surface" << endl;

	return;
}

/*
	set transparency for surface to screen backround by SDL_SetColorKey
*/

void surface::transparency(const unsigned char& r, const unsigned char& g, const unsigned char& b) {
	Uint32 color = SDL_MapRGB(area->format, r, g, b);
	if(SDL_SetColorKey(area, SDL_SRCCOLORKEY, color)!=0) cout << "surface::transparency : could not set transparency!" << endl;
	return;
}

/*
	same as above for the screen
*/

void surface::screen_transparency(const unsigned char& r, const unsigned char& g, const unsigned char& b) {
	window.transparency(r, g, b);
	return;
}

/*
	fill a rect area on the screen with a pattern of a surface
*/

void surface::fill_pattern(const class rect& pos) {
	class rect position = pos;
	SDL_Rect target, cut, place;
	cut.x = cut.y = 0;
	target = position;
	place.w = area->w;
	place.h = area->h;

	if(target.x==0 && target.y==0 && target.w==0 && target.h==0) {
		position.set(0, 0, window.area->h, window.area->w);
	}
 	if(place.w > position.get('w')) target.w = position.get('w');
	else target.w = place.w;
	if(place.h > position.get('h')) target.h = position.get('h');
	else target.h = place.h;
	target.x = position.get('x');
	target.y = position.get('y');
	short x2, y2, w_rem, h_rem;
	x2 = position.get('w') / place.w;
	if((position.get('w') - (x2*place.w)) != 0) {
		w_rem = position.get('w') - (x2*place.w);
		x2+=1;
	}
	else w_rem=0;
	y2 = position.get('h') / place.h;
	if((position.get('h') - (y2*place.h)) !=0) {
		h_rem = position.get('h') - (x2*place.h);
		y2+=1;
	}
	else h_rem=0;
	for(int x=0; x<x2; x++) {
		if(x==(x2-1) && w_rem!=0) cut.w = w_rem;
		else cut.w = place.w;
		for(int y=0; y<y2; y++) {
			if(y==(y2-1) && h_rem!=0) cut.h = h_rem;
			else cut.h = place.h;
			SDL_BlitSurface(area, &cut, window.area, &target);
			target.y+=place.h;
		}
		target.y=position.get('y');
		target.x+=place.w;
	}
}

/*
	return pointer to screen surface object
*/

SDL_Surface* surface::getsf() {
	return area;
}

/*
	draw line from point 1 to point 2 in given color
*/

void surface::line(short x1, short y1, short x2, short y2, unsigned char r, unsigned char g, unsigned char b) {
	Uint32 color=SDL_MapRGB(area->format, r, g, b);
	short xdiff, ydiff, x=x1, y=y1, max;

	if (SDL_LockSurface(area) < 0) {
		cout << "could not lock surface memory!\n";
		return;
	}

	if(x1 > x2) xdiff=x1-x2;
	else xdiff=x2-x1;

	if(y1 > y2) ydiff=y1-y2;
	else ydiff=y2-y1;
	if(xdiff > ydiff) max=xdiff;
	else max=ydiff;

	int bit=area->format->BitsPerPixel;

	if(bit==32) {
		Uint32 *bufferp;
		for(int i=0; i<max; i++) {
			x = x1 + (i * (x2 - x1) / max);
			y = y1 + (i * (y2 - y1) / max);
			bufferp = (Uint32 *)area->pixels + y*area->pitch/4 + x;
			*bufferp = color;


		}
	}
	else if(bit==24) {
		Uint8 *bufferp;
		for(int i=0; i<max; i++) {
			x = x1 + (i * (x2 - x1) / max);
			y = y1 + (i * (y2 - y1) / max);
			bufferp = (Uint8 *) area->pixels + y*area->pitch + x*3;
			bufferp[0] = color & 0xff;
			bufferp[1] = (color >> 8) & 0xff;
			bufferp[2] = (color >> 16) & 0xff;

		}
	}

	SDL_UnlockSurface(area);
	return;
}

/*
	set title of screen window and task bar button
*/

void surface::settitle(const ostring &titel) {
	if(window.area!=NULL) SDL_WM_SetCaption(titel, titel);
	return;
}

/*
	return screen width
*/

short surface::w() {
	return width;
}

/*
	return screen height
*/

short surface::h() {
	return height;
}

/*
	draw a frame of given width and color to given position
*/

void surface::screen_drawframe(const short &x, const short& y, const short& h, const short& w, const short& width, const unsigned char& r, const unsigned char& g, const unsigned char& b) {
	screenfill(x, y, width, w, r, g, b);
	screenfill(x, y, h, width, r, g, b);
	screenfill(x+w-width, y, h, width, r, g, b);
	screenfill(x, y+h-width, width, w, r, g, b);
	return;
}

/*
	same as above but with rect coordinates instead of int's
*/

void surface::screen_drawframe(const class rect& pos, const short& width, const unsigned char& r, const unsigned char& g, const unsigned char& b) {
	int x = pos.get('x'), y = pos.get('y'), h = pos.get('h'), w = pos.get('w');
	screenfill(x, y, width, w, r, g, b);
	screenfill(x, y, h, width, r, g, b);
	screenfill(x+w-width, y, h, width, r, g, b);
	screenfill(x, y+h-width, width, w, r, g, b);
	return;
}

/*
	set source color to destination color in surface
*/

void surface::colorize(unsigned char src_r, unsigned char src_g, unsigned char src_b, unsigned char dst_r, unsigned char dst_g, unsigned char dst_b) {
	if(area->format->BytesPerPixel == 3) {
		cout << "surface::colorize currently does not support 24-Bit surfaces :-/" << endl;
		return;
	}
	else {
		Uint32 src, dst;
		Uint8 *pixel;
		src = SDL_MapRGB(area->format, src_r, src_g, src_b);
		dst = SDL_MapRGB(area->format, dst_r, dst_g, dst_b);
		SDL_LockSurface(area);
		for(int x=0; x<area->w; x++) {
			for(int y=0; y<area->h; y++) {
				pixel = (Uint8*)area->pixels + (x*area->format->BytesPerPixel) + (y*area->pitch);
				if(area->format->BytesPerPixel == 4) {
					if(*(Uint32*)pixel == src) *(Uint32*)pixel = dst;
				}
				else if(area->format->BytesPerPixel == 2) {
					if(*(Uint16*)pixel == src) *(Uint16*)pixel = dst;
				}
				else if(area->format->BytesPerPixel == 1) {
					if(*pixel == src) *pixel = dst;
				}
			}
		}
		SDL_UnlockSurface(area);
	}
	return;
}

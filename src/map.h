/*
    TuxDash 0.8
    map.h - functions concerning global map/game operations

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

#ifndef _map_h
#define _map_h

class map {
	private:
		friend class field_mgm;
		friend class menu_mgm;
		friend void readconfig(map&);
		friend void writeconfig(const map&);
		friend int arg_mgm(int, char**, class map&);

		class surface* frames;	// pointer to array containing the bmp's

		class field_mgm array;	// array of field objects accessable by X/Y values
		short dimX, dimY;	// dimension of map in fields
		short viewX, viewY;	// current view position on the map (upper left corner of the screen)
		short posX, posY;	// player position on the map
		short widthX, widthY;	// width of the visible screen (if the map is smaller in one or both dimensions than the screen is)
		short screenX, screenY;	// physical size of screen in fields
		short exitX, exitY;	// exit position on the map
		short scrolldistX, scrolldistY; // distance from the screen border the player must have before scrolling starts
		short scrollamountX, scrollamountY;	// amount of fields the screen scrolls in any direction
		bool scroll_state;		// if scrolling is currently active
		char scrolldirection;	// direction the screen currently scrolls to
		short scrolldiff;		// how many pixels already have been scrolled
		unsigned short animationcounter;	// is used to animated objects correctly

		bool exit_open;	// if the exit is already open
		bool lfinish;	// game status
		bool status_visible;	// if the status bar is visible
		bool auto_scroll;	// if (currently) automatic scrolling to the player is activated
		bool fullscreen;	// if window- or fullscreenmode is active
		short fullscreenX, fullscreenY;	// used fullscreen reslution (if fullscreen mode active)
		short bitdepth;	// colordepth
		short playerdeath;
		/*
			= 0 --> normal, player alive
			= 1 --> level is starting
			= 2 --> timeout or player is dead
			= 3 --> pause
		*/
		short diamonds;	// number of gathered diamonds
		short diamonds_needed;	// how many diamonds are needed to open the exit
		short score;
		short time;		// remaining level time
		short time_limit;	// time limit for the level
		short magwall_duration;	// number of secods the magic wall is active once activated
		int magwall_start;	// if and when the magic wall has been activated
		short lava_probability;		// propability for lava growing (the lower the faster it grows)
		short lava_prob2;		// helping variable for lava propability
		short lava_percentage;	// maximum lava amount in percent before it turns into stone
		short lava_amount;		// counts every field of lava to determine percent of lava on the map
		bool menue_start;		// if the games has just been started display the menu first
		int global_action;	// time in seconds when there last was a global action (like menu open or game save ...)
		ostring filename;		// filename of the current running level
		ostring mapfolder;	// folder in which maps are found
		ostring savefolder;	// folder in which savegames are found
		ostring path;		// complete path of folder and map running
		ostring theme;		// path to theme package currently used

		/*
			Frame management
			in order to allow dynamic frame numbers and animation speed variable numbers are needed to represent the
			correct index of frames in the frames-array
		*/
		int diamond_frame_start, diamond_frame_count, diamond_frame_speed;
		int manwalkr_frame_start, manwalkr_frame_count, manwalkr_frame_speed;
		int manwalkl_frame_start, manwalkl_frame_count, manwalkl_frame_speed;
		int manwalkd_frame_start, manwalkd_frame_count, manwalkd_frame_speed;
		int manwalku_frame_start, manwalku_frame_count, manwalku_frame_speed;
		int mananim1_frame_start, mananim1_frame_count, mananim1_frame_speed;
		int mananim2_frame_start, mananim2_frame_count, mananim2_frame_speed;
		int explosion_frame_start, explosion_frame_count, explosion_frame_speed;
		int gateway_frame_start, gateway_frame_count, gateway_frame_speed;
		int gateway_open_frame_start, gateway_open_frame_count, gateway_open_frame_speed;
		int mwall_frame_start, mwall_frame_count, mwall_frame_speed;
		int lava_frame_start, lava_frame_count, lava_frame_speed;
		int fly_frame_start, fly_frame_count, fly_frame_speed;
		int bfly_frame_start, bfly_frame_count, bfly_frame_speed;
		int sfly_frame_start, sfly_frame_count, sfly_frame_speed;
		int number_frame_start, number_frame_count;
		int exit_frame_start, exit_frame_count, exit_frame_speed;

		SDL_Color BGColor;	// background color used in game (usually specified by theme package)

		void loadimages();	// initialise frame array
		int loadtheme();		// read the theme-packages and get the correct values for frame_start�s  and frame_speed�s
		void statusbar(const short&, const bool=true);	// display game informations in the status bar
		void flash();	// display a short flash on the screen when enough diamonds have been gathered to open the exit
		void physik();	// manage movement of all objects in game
		void playermove();	// manage and animate player movement
	public:
		map();	// constructor
		~map();	// destructor
		int init();	// initialise the game - read config file and open menu
		void draw_screen(const int&);	// draw screen and animations
		void move(const char&);	// move the player
		void scroll(const char&);	// manage scrolling
		//void debug(short, short, char=0) const;		// enable debug output by mouse clicks
		void pickup(const char&);	// player 'use' management
		void level_finish(const short&);	// quit and restart level
		int copymap(const ostring&);	// copy map to a different filename
		int savemap(const ostring&, const bool = false);	// save current game state
		int custommap_start(int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int);	// start creating a custom map
		void custommap_finish(int, int);	// finish a custom map
		void randommap();	// create a random map
		int loadmap(const ostring&, const int&);	// load a map (savegame or normal map)
		void timecounter();	// manage time counter
		void timer_reset();	// reset the global time in main()
		const void statusmsg(const bool);	// display messages in the statusbar (levelstart, playerdeath, pause ...)

		class menu_mgm menue;	// menu object
		int framerate;	// current framerate
};


#endif /* map.h */

/*
    TuxDash 0.8
    fields.cpp - a simple class to manage for array objects

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

fields::fields() {
	/*
		*** Default constructor ***
		set default values for every field
	*/
	type = -1;
	moved = 0;
	direction= 0;
	action = 0; // Keine Bewegung - Keine Eigenschaft
	animcount=0;
	return;
}

inline bool fields::operator==(int zahl) {
	/*
	*** compare type with int ***
	*/

	if(zahl == type) return true;
	else return false;

}

inline bool fields::operator<(int zahl) {
	/*
	*** compare type with int ***
	*/

	if(type < zahl) return true;
	else return false;

}

inline bool fields::operator>(int zahl) {
	/*
	*** compare type with int ***
	*/

	if(type > zahl) return true;
	else return false;

}

inline bool fields::operator<=(int zahl) {
	/*
	*** compare type with int ***
	*/

	if(type <= zahl) return true;
	else return false;

}

inline bool fields::operator>=(int zahl) {
	/*
	*** compare type with int ***
	*/

	if(type >= zahl) return true;
	else return false;

}

inline bool fields::operator!=(int zahl) {
	/*
	*** compare type with int ***
	*/

	if(zahl != type) return true;
	else return false;

}

inline void fields::operator=(int zahl) {
	/*
		*** set type ***
	*/
	type = zahl;
	return;
}

void fields::settype(short new_type, short new_action) {
	type = new_type;
	action = new_action;
	return;
}



void fields::clear() {
	type = -1;
	action = 0;
	moved = 0;
	animcount = 0;
	direction = 0;
	return;
}


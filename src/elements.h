/*
    TuxDash 0.8
    elements.h - linked list for menu

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

#ifndef _elements_h
#define _elements_h

class element {
	private:
		friend class menu_mgm;
		int type;		// determines basic behaviour/use of this element
		int posx, posy;	// pixel position
		int width, height;
		int xgroup, ygroup;	// kind of table structure to determine where to navigate to
		int fontsize;	// size of text for this element
		unsigned char r, g, b;	// color
		bool selectable;	// if this element can be selected in any way
		bool dependency;	// dependencies that activate/deactivate this element, like a check box

		ostring value;	// value that is displayed
		ostring name;	// name for internal indentificatin of the element
		ostring depend;	// other element this element depends on
		ostring sec_level_depend;
		int value_type;	// kind of value allowed for this element (all chars, numbers, special behaviour)
		int max;	// maximum length for value


		element* next;	// next element in linked list
	public:
		element();
};

#include "elements.cpp"

#endif /* elements.h */

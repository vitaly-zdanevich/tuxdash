/*
    TuxDash 0.8
    ostring.cpp - simple object oriented string class

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

/*
	default constructor creates an empty but null terminated char array
*/
ostring::ostring() {
	content = NULL;
	length = 0;
	resize(1, false);	// resize to a 1 sized char array
	return;
}

/*
	construct a new ostring object and copy existing string into it
*/

ostring::ostring(const class ostring& second) {
	content = NULL;
	length = 0;
	if(second.length <= 0) cout << "ostring::ostring(ostring&): length of passed string is <= 0" << endl;
	else if(second.content == NULL) cout << "ostring::ostring(ostring&): passed string is NULL" << endl;
	resize(second.length, false);	// to the length of the passed object
	strcpy(content, second.content);	// copy string
	return;
}


/*
	construct a new ostring object and copy existring string into it
*/
ostring::ostring(const char* string) {
	content = NULL;
	length = 0;

	if(string!=NULL && strlen(string) > 0) {
		resize(strlen(string)+1, false);	// resize to length of passed string
		strcpy(content, string);	// copy string
	}
	else resize(1, false);	// if an empty string was passed, just create an empty, null terminated char array

	return;
}

/*
	construct a new ostring object and copy a single char into it
*/

ostring::ostring(const char zeichen) {
	content = NULL;
	length = 0;
	operator=(zeichen);	// add a single char
	return;
}

/*
	construct a new ostring object and copy integer to string
*/
ostring::ostring(const int& number) {
	content = NULL;
	length = 0;
	operator=(number);
	return;
}

/*
	destructor simply deletes the char array
*/

ostring::~ostring() {
	delete[] content;
	return;
}

/*
	operator= assigns a new string to the object
*/

ostring& ostring::operator=(const ostring& second) {
	if(this == &second) return *this;
	else if(second.length <= 0) cout << "ostring::operator=(ostring&): passed object is <= 0" << endl;
	else if(second.content == NULL) cout << "ostring::operator=(ostring&): passed object is NULL" << endl;
	resize(second.length, false);	// resize to the new string length
	strcpy(content, second.content);	// copy string

	return *this;
}

/*
	operator= assings a single char to the object
*/

ostring& ostring::operator=(const char character) {
	resize(2, false);	// resize to a char array with two elements
	content[0] = character;	// assign the character
	return *this;
}

/*
	operator= casts and assigns an integer to the string
*/

ostring& ostring::operator=(const int& number) {
	if(number == 0) {
		operator=('0');
		return *this;
	}
	int i=0, power, temp_length = 2, number2 = number;
	int digit;

	if(number2 < 0) number2*=-1;	// change negative number to positive

	while(number2 > 0) {
		resize(temp_length);
		power = (int)pow((double)10, (i+1));
		digit = number2%power;	// seperate every digit of the number
		number2 -= digit;
		content[i] = digit / (int)pow((double)10, i);
		content[i]+=48;	// add digit to char array
		i+=1;
		if(i >= (temp_length-1)) temp_length+=1;	// increase array size
	}


	temp_length-=1;
	content[temp_length-1]='\0';	// null terminate the string
	ostring temp = *this;	// copy the result string

	for(int i=0; i<temp_length-1; i++) {
		content[i] = temp.content[temp_length-i-2];	// reverse digit order
	}

	if(number<0) {	// if number is negative, add a minus to the string
		temp = '-';
		temp = temp + *this;
		operator=(temp);
	}
	return *this;	// return string
}

/*
	compare ostring with string
*/

bool ostring::operator==(const char* string) const {
	if(string == NULL || content == NULL) return false;

	if(strcmp(content, string) == 0) return true;
	else return false;
}

/*
	compare ostring with string
*/

bool ostring::operator!=(const char* string) const {
	if(operator==(string) == true) return false;
	else return true;
}

/*
	return a constant pointer to the char array thus ostring can be used everywhere like a normal string, except it is not writeable
*/

ostring::operator const char*() const {
	return content;
}


/*
	insert a char at the specified array number
*/

void ostring::operator() (unsigned int number, char insert) {
	// if index is not within the array boundaries, resize the array
	if((unsigned int)number >= length-1) resize(number+2);

	content[number] = insert;

	return;
}

/*
	return specified array index content
	number is an int instead of unsigned int because otherwise the operator would be amigous to the operator for char* (returned by operator const char*() )
*/
const char ostring::operator[] (int number) {
	if(number < 0) {
		cout << "ostring::operator[](int) : index " << number << " is negative. Returning element 0 instead" << endl;
		return content[0];
	}
	if((unsigned int)number >= length-1) {
		cout << "ostring::operator[](int) : index " << number << " does not exist. Returning element 0 instead" << endl;
		return content[0];
	}
	return content[number];
}

/*
	overloaded operator<< to print ostrings
*/

ostream& operator<<(ostream &stream, const ostring& text) {
	return (stream << text.content);
}

/*
	return length of string - with strlen
	so it does not return the length of the array but the length of the NULL-terminated string
*/

int ostring::len() const {
	return strlen(content);
}

/*
	add a string to the current string
*/

ostring& ostring::operator+=(const class ostring& second) {
	if(&second == this) {	// if the second object is this object, create a temporary string an copy it into this object
		ostring temp = *this;	// otherwise a segmentation fault would occur in strcat ...
		temp += *this;
		*this = temp;
		return *this;
	}
	int temp = length;
	resize(length + second.length - 1);
	content[temp-1] = '\0';
	strcat(content, second.content);
	return *this;
}

/*
	del the last character in the array by decreasing array size by one
*/

ostring& ostring::operator--() {
	if(length>1) resize(length-1);
	return *this;
}

/*
	resize the array to new_length
	if datasave is true, preserve array content, otherwise don't care
*/

void ostring::resize(unsigned int new_length, bool datasave) {
	char* temp;

	if(new_length < 1) {
		cout << "ostring::resize(int, bool) : warning: size < 1 specified. resize was not done. Given length was:" << new_length << endl;
		return;
	}

	if(datasave == true && content != NULL && length > 1 ) {	// if datasave is true, copy current array content to a temporary array
		temp = new char[length];
		for(unsigned int i=0; i<length; i++) temp[i] = content[i];
	}

	delete[] content;	// delete current array
	content = new char[new_length];	// reallocate array

	// copy temporary string back to new array
	if(datasave == true && content != NULL && length > 1) 	for(unsigned int i=0; i<new_length && i<length; i++) content[i] = temp[i];

	length = new_length;
	content[length-1] = '\0';
	return;
}

/*
	set the complete string to upper case
*/

ostring& ostring::upper() {
	for(unsigned int i=0; i<length; i++) content[i] = toupper(content[i]);
	return *this;
}

/*
	set the complete string to lower case
*/

ostring& ostring::lower() {
	for(unsigned int i=0; i<length; i++) content[i] = tolower(content[i]);
	return *this;
}

/*
	cut a part of the string from index start to index end
*/

ostring ostring::cut(unsigned int start, unsigned int end) {
	if(length <= 1) return '\0';

	if(start > end) {	// swap index boundaries if start is bigger than end
		int temp = start;
		start = end;
		end = temp;
	}

	if(start >= length-1) start = length - 2;
	else if(start < 0) start = 0;
	if(end >= length-1) end = length - 2;
	else if(end < 0) end = 0;

	ostring back;	// copy the string part to a new string

	for(unsigned int i = start; i<= end; i++) {
		back += content[i];
	}
	return back;
}

/*
	concatenate two strings to one string, return result as a new object
*/

ostring ostring::operator+(const class ostring& second) {
	ostring back;
	back = *this;
	back += second;

	return back;
}

/*
	return a bool wheter the string is empty (that means only a \0 is existing
*/
bool ostring::IsNull() {
	if(length==1 && content[0]=='\0') return true;
	return false;
}

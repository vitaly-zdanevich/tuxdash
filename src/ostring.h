/*
    TuxDash 0.8
    ostring.h - simple object oriented string class

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

#ifndef _ostring_h
#define _ostring_h
#include <iostream>
/*
	Yes I really wrote my own string class ;-)
	Maybe it's not 100 percent stable, but I hope so.
*/

using namespace std;

class ostring {
	friend ostream& operator<<(ostream&, const ostring&);
	private:
		char* content;	// the char array
		unsigned int length;	// length of char array
		void resize(unsigned int, bool=true);	// resize the char array
	public:
		ostring();
		ostring(const char*);
		ostring(const char);
		ostring(const ostring&);
		ostring(const int&);	// construct string out of int
		~ostring();
		ostring& operator=(const ostring&);
		ostring& operator=(const char);
		ostring& operator=(const int&);	// set string to int
		bool operator==(const char*) const;
		bool operator!=(const char*) const;
		ostring& operator+=(const class ostring&);	// concatenate a string to this string object
		ostring& operator--();	// cut off one char from the string
		ostring operator+(const class ostring&);	// concatenate two string objects and return the result
		const char operator[](int);	// get char of a given string index
		void operator()(unsigned int, char);	// set string index to given char
		operator const char*() const;	// cast ostring to const standard string
		ostring cut(unsigned int, unsigned int);	//	return part of a string
		int len() const;	// get length by strlen()
		ostring& lower();	// set string to lower case
		ostring& upper();	// set string to upper case
		bool IsNull();	// if the string is empty
};

#include "ostring.cpp"

#endif /* ostring.h */

/*
    TuxDash 0.8
    menu.cpp - menu management

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

/*
	initialise menu object with default arguments
*/

menu_mgm::menu_mgm() {
	windowx = windowy = 0;
	name =  "";
	selected = NULL;
	anchor = NULL;
	return;
}

/*
	construct and initialise a menu object
*/

menu_mgm::menu_mgm(class map& field) {
	init(field);
	return;
}

/*
	do initialisation of menu object
*/

void menu_mgm::init(class map& field) {
	map = &field;	// set pointer to map object
	windowx = ((map->screenX -21) / 2) * 30;	// set coordinates on screen where menu appears
	windowy = ((map->screenY - 16) / 2) * 30;
	windowy += 30;

	return;
}

/*
	destruct object; delete remaining elements in linked list
*/

menu_mgm::~menu_mgm() {
	del_elements();
	return;
}

/*
	add an element to the linked list and return a reference to it
*/

class element& menu_mgm::add() {

	class element* temp = anchor;
	if(anchor == NULL) {	// if anchor element does not exist yet, return that one
		anchor = new element;
		return *anchor;
	}
	else {	// otherwise find end of linked list and add an element
		while(temp->next != NULL) temp = temp->next;
		temp->next = new element;
		return *(temp->next);
	}


}

/*
	add a text element
*/

class element* menu_mgm::add_text(int x, int y, const ostring& text, bool selectable, int size, int xgroup, int ygroup, unsigned char r, unsigned char g, unsigned char b, int height, int width) {
	if(height == -1 || width == -1) {	// -1 is given for height and width use default values instead
		height = 30;
		int laenge = text.len();
		width = 60 + laenge*(10+(size/5));
	}
	if(r == 0 && g == 0 && b == 0) {	// if 0 is given for all color values, use default text-color instead
		r = text_color1.r;
		g = text_color1.g;
		b = text_color1.b;
	}
	class element& newone = add();	// add an element to the linked list
	newone.posx = x;	// set position
	newone.posy = y;
	newone.name = text;
	newone.selectable = selectable;	// determines if the user can select this element
	newone.type = 0;
	newone.r = r;
	newone.g = g;
	newone.b = b;
	newone.xgroup = xgroup;	// determines the tab position of the element
	newone.ygroup = ygroup;
	newone.fontsize = size;
	newone.height = height;
	newone.width = width;
	newone.max = 0;
	return &newone;
}

// add a selection box

class element* menu_mgm::add_box(int x, int y, const ostring& text, const ostring& value, bool selectable, int size, int xgroup, int ygroup, int max, int width, int height, unsigned char r, unsigned char g, unsigned char b, int value_type, bool dependency, char* depend) {
	class element& newone = add();
	newone.value = value;
	newone.posx = x;
	newone.posy = y;
	newone.name = text;
	newone.selectable = selectable;
	newone.type = 1;
	newone.r = r;
	newone.g = g;
	newone.b = b;
	newone.xgroup = xgroup;
	newone.ygroup = ygroup;
	newone.width = width;
	newone.height = height;
	newone.fontsize = size;
	newone.max = max;	// maximum number of chars allowed in the box
	newone.value_type = value_type;	// determines what kind of value is allowed in the box (numbers, letters, SDL_Keys)
	if(dependency == true) {	// adds a dependency - if the specified multiple choice element is not activated, this element is not activated too
		newone.dependency = true;
		newone.depend = depend;
	}
	return &newone;
}

// add selectable text

class element* menu_mgm::add_select(int x, int y, const ostring& text, const ostring& value, bool selectable, int size, int xgroup, int ygroup, const char* oneoftwo) {
	class element& newone = add();
	newone.value = value;
	newone.posx = x;
	newone.posy = y;
	newone.name = text;
	newone.selectable = selectable;
	newone.xgroup = xgroup;
	newone.ygroup = ygroup;
	newone.width = size;
	newone.height = size;
	newone.max = 1;
	newone.type = 2;
	newone.value_type = 2;
	if(oneoftwo != NULL) newone.sec_level_depend = oneoftwo;	// add a second dependency
	return &newone;
}

/*
	draw all menu elements
*/

void menu_mgm::draw_window() {
	rect position;
	element* temp = anchor;
	while(temp != NULL) {
		position.set(windowx+temp->posx, windowy+temp->posy, temp->height, temp->width);
		if(temp->type == 0) {
			if(selected->name == temp->name) surface::screenfill(windowx+temp->posx-30, windowy+temp->posy+3, temp->height, temp->width, text_select_color.r, text_select_color.g, text_select_color.b);
			surface::ttf_write(temp->name, temp->fontsize, temp->r, temp->g, temp->b, position);
		}
		else if(temp->type == 1) {
			if(temp->selectable == 1) surface::screenfill(position, boxbg_color.r, boxbg_color.g, boxbg_color.b);
			else surface::screenfill(position, boxbg_color2.r, boxbg_color2.g, boxbg_color2.b);

			if(selected->name == temp->name) surface::screen_drawframe(position, 2, text_color2.r, text_color2.g, text_color2.b);
			position.set(windowx+temp->posx+3, windowy+temp->posy+2, 0, 0);
			surface::ttf_write(temp->value, temp->fontsize, temp->r, temp->g, temp->b, position);

		}
		else if(temp->type == 2) {
			if(temp->selectable == 1) surface::screenfill(position, boxbg_color.r, boxbg_color.g, boxbg_color.b);
			else surface::screenfill(position, boxbg_color2.r, boxbg_color2.g, boxbg_color2.b);

			if(selected->name == temp->name) surface::screen_drawframe(position, 2, text_color2.r, text_color2.g, text_color2.b);
			if(temp->value == "1") {
				position.set(windowx+temp->posx+3, windowy+temp->posy+3, temp->height-6, temp->width-6);
				surface::screenfill(position, text_color2.r, text_color2.g, text_color2.b);
			}
		}
		temp = temp->next;
	}
	return;
}

/*
	determine what element is selected after user interaction
*/

void menu_mgm::selection_mgm(char direction) {
	bool done=false;
	class element &selection = *selected;
	class element *temp = anchor;

	if(direction == 'D' || direction == 'U') {
		int searchX = selection.xgroup;
		int searchY;
		class element *temp2=NULL;

		if(direction == 'D') searchY = selection.ygroup+1;
		else searchY = selection.ygroup-1;

		while(temp != NULL && done!=true) {
			if(direction == 'D' && temp->ygroup > searchY && temp->selectable == true && temp->xgroup == searchX) {
				if(temp2 == NULL) temp2 = temp;
				else if(temp2 != NULL && temp->ygroup < temp2->ygroup) temp2 = temp;
			}
			else if(direction == 'U' && temp->ygroup < searchY && temp->selectable == true && temp->xgroup == searchX) {
				if(temp2 == NULL) temp2 = temp;
				else if(temp2 != NULL && temp->ygroup > temp2->ygroup) temp2 = temp;
			}
			if(temp->xgroup == searchX && temp->ygroup == searchY && temp->selectable == true) 	done = true;
			if(done!=true) temp = temp->next;
		}

		if(temp2 != NULL && done!=true) {
					temp = temp2;
					done = true;
		}

		if(done != true) {
			if(direction == 'D') {
				if(searchY-1 != 0) {
					temp = anchor;
					searchY = 0;
					while(temp!=NULL && done!=true) {
						if(temp->xgroup == searchX && temp->ygroup == searchY && temp->selectable == true) done = true;
						if(done!=true) temp = temp->next;
					}
				}
			}
			else {
				int maxygroup = 0;
				temp = anchor;
				while(temp!=NULL) {
					if(temp->ygroup > maxygroup && temp->selectable == true) maxygroup = temp->ygroup;
					temp = temp->next;
				}
				if(maxygroup != searchY + 1) {
					temp = anchor;
					searchY = maxygroup;
					while(temp != NULL && done != true) {
						if(temp->xgroup == searchX && temp->ygroup == searchY && temp->selectable == true) done = true;
						if(done != true) temp = temp->next;
					}
				}
			}
		}
	}

	if(direction == 'L' || direction == 'R') {
		int searchY = selection.ygroup;
		int searchX;
		class element *temp2=NULL;

		if(direction == 'L') searchX = selection.xgroup -1;
		else searchX = selection.xgroup + 1;

		while(temp!=NULL && done!=true) {
			if(temp->xgroup == searchX && temp->ygroup != searchY && temp->selectable == true) {
				if(temp2 == NULL) temp2 = temp;
				else if(abs(temp2->ygroup - searchY) > abs(temp->ygroup - searchY)) temp2 = temp;
			}
			if(temp->xgroup == searchX && temp->ygroup == searchY && temp->selectable == true) done = true;
			if(done != true) temp = temp->next;
		}

		if(done!= true && temp2 != NULL) {
			temp = temp2;
			done = true;
		}

		if(done != true) {
			if(direction == 'R') {
				if( (searchX - 1) != 0) {
					temp = anchor;
					searchX = 0;
					while(temp != NULL && done!=true) {
						if(temp->ygroup == searchY && temp->xgroup == searchX && temp->selectable == true) done = true;
						if(done != true) temp = temp->next;
					}
				}
			}
			else {
				int maxxgroup = 0;
				temp = anchor;
				while(temp!=NULL) {
					if(temp->xgroup > maxxgroup && temp->selectable == true) maxxgroup = temp->xgroup;
					temp = temp->next;
				}

				if(maxxgroup != searchX+1) {
					temp = anchor;
					searchX = maxxgroup;
					while(temp != NULL && done!=true) {
						if(temp->xgroup == searchX && temp->ygroup == searchY && temp->selectable == true) done = true;
						if(done!=true) temp = temp->next;
					}
				}

			}
		}
	}

	if(done != true) return;
	else 	selected = temp;

	return;
}

/*
	find an element with the specified name in linked list and return reference
*/

class element& menu_mgm::find(const ostring& search) {
	class element* temp = anchor;

	while(temp != NULL && temp->name != search) {
		temp=temp->next;
	}

	return *temp;
}

/*
	delete all elements in linked list
*/

void menu_mgm::del_elements() {
	class element* temp = anchor, *temp2;

	while(temp != NULL) {
		temp2 = temp->next;
		delete temp;
		temp = temp2;
	}

	anchor = NULL;
	return;
}

/*
	clear screen and draw empty menu background
*/

void menu_mgm::clear_area() {
	surface::screenfill(windowx, windowy, 16*30, 21*30, 150, 150, 255);	// Den Bereich schwarz �bermalen

	for(int i=0; i<21; i++) {
		for(int j=0; j<16; j++) {
			map->frames[14].blit(windowx+(i*30), windowy+(j*30));	// Gras als Hintergrund malen
		}
	}

	surface::screen_drawframe(windowx, windowy, 16*30, 21*30, 2, 0, 128, 255);	// Rahmen drumherum
	return;
}

/*
	check what to do after user interaction
*/

int menu_mgm::key_mgm(SDLKey &taste, Uint8 *tastenstatus) {
	if(selected->selectable == true) {
		if(selected->value.len() < selected->max) {
			bool done = true;
			if((selected->value_type == 0 || selected->value_type == 1) && taste >= 46 && taste <= 57 && !tastenstatus[SDLK_LSHIFT] && !tastenstatus[SDLK_RSHIFT]) {
				selected->value += (char)taste;
			}
			else if(selected->value_type == 0 && taste >= SDLK_0 && taste <= SDLK_9 && (tastenstatus[SDLK_LSHIFT] || tastenstatus[SDLK_RSHIFT])) {
				if(taste == SDLK_7) selected->value += '/';
				else done = false;
			}
			else if(selected->value_type == 0 && taste >= SDLK_a && taste <= SDLK_z) {
				if(tastenstatus[SDLK_LSHIFT] || tastenstatus[SDLK_RSHIFT]) selected->value += (char)(taste-32);
				else selected->value += (char)taste;
			}
			else if(selected->value_type == 2 && selected->value == "") {	// Controls
				selected->value = keytoa(taste);
			}
			else done = false;
			if(done == true) return -1;
		}

		if(taste == SDLK_RETURN) {
			if(name == "SAVE") {
				map->copymap(selected->value);
				return 0;
			}
			else if(name == "LOAD") {
				map->loadmap(selected->value, 0);
				return 0;
			}
			else if(name == "GAMESAVE") {
				map->savemap(selected->value, true);
				return 0;
			}
			else if(name == "GAMELOAD") {
				map->loadmap(selected->value, true);
				return 0;
			}
			else if(name == "CUSTOM") {
				if(selected->name == "Cancel") return 0;
				else if(selected->name == "Create Map") {
					int mwall, diamanten;
					if(find("magicwall").value == "0") mwall = 0;
					else mwall = atoi(find("magiclength").value);
					diamanten = map->custommap_start(atoi(find("dimx").value), atoi(find("dimy").value), atoi(find("diamonds").value), atoi(find("stones").value), atoi(find("grass").value), atoi(find("mborder").value), mwall, atoi(find("magiclength").value), atoi(find("lavaselect").value), atoi(find("lavaamount").value), atoi(find("lavaspeed").value), atoi(find("nflies").value), atoi(find("bflies").value), atoi(find("sflies").value), atoi(find("walls").value), atoi(find("mwalls").value));
                			draw_custom2(diamanten);
     					return 4;
				}
			}
			else if(name == "CUSTOM2") {
				if(selected->name == "Start Map") {
					map->custommap_finish(atoi(find("time").value), atoi(find("needed").value));
					return -2;
				}
			}
			else if(name == "RANDOM") {
				if(selected->name == "Yes") {
					map->randommap();
				}
				else return 0;
				return -2;
			}
			else if(name == "MAIN") {
				if(selected->name == "Resume Game") return -2;	// Spiel fortsetzen
				else if(selected->name == "Restart Map") {
						map->loadmap(map->filename, -1);
						return -2;
				}
				else if(selected->name == "Save Map") return 1;	// Men� "Karte speichern" anzeigen
				else if(selected->name == "Load Map") return 2; // Men� "Karte laden" anzeigen
				else if(selected->name == "Save Game") return 7;
				else if(selected->name == "Load Game") return 8;
				else if(selected->name == "Create Custom Map") return 3; // Men� "Custom Map" anzeigen
				else if(selected->name == "Create Random Map") return 5;
				else if(selected->name == "Options") return 6;
				else if(selected->name == "Quit Game") return -3;	// Spiel beenden
			}
			else if(name == "OPTIONS") {
				if(selected->value_type == 2 && selected->name != "windowselect" && selected->name != "fullscreenselect") {		// Controls
					selected->value = "";
				}
				else if(selected->name == "Cancel") return 0;
				else if(selected->name == "Save Settings") {
					options_save();
					return 0;
				}
				else if(selected->value_type == 3) {
					if(selected->name == "resolution") {
						if(selected->value == "640x480") selected->value = "800x600";
						else if(selected->value == "800x600") selected->value = "1024x768";
						else if(selected->value == "1024x768") selected->value = "1280x1024";
						else if(selected->value == "1280x1024") selected->value = "640x480";
					}
				}
			}
			return -1;
		}
		else if(taste == SDLK_BACKSPACE && selected->value_type <= 1) {
			--(selected->value);
		}
		else if(taste == SDLK_SPACE) {
			if(selected->type == 2) {
				if(selected->value == "0") {
					if(selected->sec_level_depend != "") {
						find(selected->sec_level_depend).value = "0";
						change_deps(false, &find(selected->sec_level_depend));
					}
					selected->value = "1";
					change_deps(true);
				}
				else if(selected->value == "1") {
					if(selected->sec_level_depend != "") {
						find(selected->sec_level_depend).value = "1";
						change_deps(true, &find(selected->sec_level_depend));
					}
					selected->value = "0";
					change_deps(false);
				}
			}
		}
	}

	if(taste == SDLK_ESCAPE) {
		if(name != "MAIN") return 0;
		else return -2;
	}
	else if(taste == SDLK_DOWN) {	// Pfeiltaste runter
		selection_mgm('D');
		clear_area();
		draw_window();
	}
	else if(taste == SDLK_UP) { // Pfeiltaste hoch
		selection_mgm('U');
		clear_area();
		draw_window();
	}
	else if(taste == SDLK_LEFT) {
		selection_mgm('L');
		clear_area();
		draw_window();
	}
	else if(taste == SDLK_RIGHT) {
		selection_mgm('R');
		clear_area();
		draw_window();
	}

	return -1;
}

/*
	change dependency of an element
*/

void menu_mgm::change_deps(bool change, class element *object) {
	class element *temp = anchor;
	class element *victim;

	if(object != NULL) victim = object;
	else victim = selected;

	while(temp!=NULL) {
		if(temp->depend == victim->name) 	temp->selectable = change;
		temp = temp->next;
	}

	return;
}

/*
	cast SDLKey to ASCII string
*/

ostring menu_mgm::keytoa(SDLKey taste) {
	ostring tempstring;

	if((taste >SDLK_0 && taste <= SDLK_9) || (taste >= SDLK_a && taste <= SDLK_z)) tempstring = (char)taste;
	else if(taste == SDLK_UP) tempstring = "Cursor Up";
	else if(taste == SDLK_DOWN) tempstring =  "Cursor Down";
	else if(taste == SDLK_RIGHT) tempstring = "Cursor Right";
	else if(taste == SDLK_LEFT) tempstring = "Cursor Left";
	else if(taste == SDLK_LALT) tempstring = "Left Alt";
	else if(taste == SDLK_RALT) tempstring = "Right Alt";
	else if(taste == SDLK_LCTRL) tempstring = "Left Control";
	else if(taste == SDLK_RCTRL) tempstring = "Right Control";
	else if(taste == SDLK_SPACE) tempstring = "Space Bar";
	else if(taste == SDLK_LSHIFT) tempstring = "Left Shift";
	else if(taste == SDLK_RSHIFT) tempstring = "Right Shift";
	else if(taste == SDLK_KP0) tempstring = "Keypad 0";
	else if(taste == SDLK_KP1) tempstring = "Keypad 1";
	else if(taste == SDLK_KP2) tempstring = "Keypad 2";
	else if(taste == SDLK_KP3) tempstring = "Keypad 3";
	else if(taste == SDLK_KP4) tempstring = "Keypad 4";
	else if(taste == SDLK_KP5) tempstring = "Keypad 5";
	else if(taste == SDLK_KP6) tempstring = "Keypad 6";
	else if(taste == SDLK_KP7) tempstring = "Keypad 7";
	else if(taste == SDLK_KP8) tempstring = "Keypad 8";
	else if(taste == SDLK_KP9) tempstring = "Keypad 9";
	else if(taste == SDLK_KP_ENTER) tempstring = "Keypad Enter";
	else if(taste == SDLK_KP_PLUS) tempstring = "Keypad Plus";
	else if(taste == SDLK_KP_MINUS) tempstring = "Keypad Minus";
	else if(taste == SDLK_RETURN) tempstring = "Return";
	else if(taste == SDLK_HOME) tempstring = "Home";
	else if(taste == SDLK_END) tempstring = "End";
	else if(taste == SDLK_PAGEUP) tempstring = "Page Up";
	else if(taste == SDLK_PAGEDOWN) tempstring = "Page Down";
	else if(taste == SDLK_TAB) tempstring = "Tab";

	return tempstring;
}

/*
	Save options of menu screen
*/

void menu_mgm::options_save() {
	int screenX, screenY, scrolldistX, scrolldistY, scrollamountX, scrollamountY, new_speed;
	bool fullscreen, framerate;

	screenX = atoi(find("dimx").value);
	screenY = atoi(find("dimy").value);
	scrolldistX = atoi(find("scrolldistx").value);
	scrolldistY = atoi(find("scrolldisty").value);
	scrollamountX = atoi(find("scrollamountx").value);
	scrollamountY = atoi(find("scrollamounty").value);
	new_speed = atoi(find("speed").value);
	fullscreen = atoi(find("fullscreenselect").value);
	framerate = atoi(find("framerate").value);
	map->theme = find("themefolder").value;

	map->loadimages();

	if(framerate == 1) map->framerate = 0;
	else map->framerate = -1;

	if(fullscreen == true) {
		if(find("resolution").value == "640x480") {
			map->fullscreenX = 640;
			map->fullscreenY = 480;
		}
		else if(find("resolution").value == "800x600") {
			map->fullscreenX = 800;
			map->fullscreenY = 600;
		}
		else if(find("resolution").value == "1024x768") {
			map->fullscreenX = 1024;
			map->fullscreenY = 768;
		}
		else if(find("resolution").value == "1280x1024") {
			map->fullscreenX = 1280;
			map->fullscreenY = 1024;
		}
		map->init();
	}

	if(screenX!=map->screenX || screenY!=map->screenY || fullscreen != map->fullscreen) {
		map->fullscreen = fullscreen;
		map->screenX = screenX;
		map->screenY = screenY;
		map->init();
	}

	if(scrolldistX > (map->screenX*0.5 - 2)) scrolldistX = (int)(map->screenX*0.5 - 2);
	if(scrolldistY > (map->screenY*0.5-2)) scrolldistY = (int)(map->screenY*0.5 - 2);
	map->scrolldistX = scrolldistX;
	map->scrolldistY = scrolldistY;

	if(scrollamountX > (map->screenX - (2*map->scrolldistX) - 1)) scrollamountX = map->screenX - (2*map->scrolldistX);
	if(scrollamountY > (map->screenY - (2*map->scrolldistY) - 1)) scrollamountY = map->screenY - (2*map->scrolldistY);
	map->scrollamountX = scrollamountX;
	map->scrollamountY = scrollamountY;



	if(new_speed != (speed-50)*-1) {
		if(new_speed >= 0 && new_speed <= 50)	speed = (new_speed-50)*-1;
	}

	ostring temp;

	for(int i=0; i<7; i++) {
		if(i == 0) temp = "ctrl_left";
		else if(i == 1) temp = "ctrl_right";
		else if(i == 2) temp = "ctrl_up";
		else if(i == 3) temp = "ctrl_down";
		else if(i == 4) temp = "ctrl_use";
		else if(i == 5) temp = "ctrl_scroll";
		else if(i == 6) temp = "ctrl_pause";

		if(find(temp).value.len() == 1) key[i] = (SDLKey) find(temp).value[0];
		else {
			if(find(temp).value == "Cursor Up") key[i] = SDLK_UP;
			else if(find(temp).value == "Cursor Down") key[i] = SDLK_DOWN;
			else if(find(temp).value == "Cursor Left") key[i] = SDLK_LEFT;
			else if(find(temp).value == "Cursor Right") key[i] = SDLK_RIGHT;
			else if(find(temp).value == "Left Alt") key[i] = SDLK_LALT;
			else if(find(temp).value == "Right Alt") key[i] = SDLK_RALT;
			else if(find(temp).value == "Left Control") key[i] = SDLK_LCTRL;
			else if(find(temp).value == "Right Control") key[i] = SDLK_RCTRL;
			else if(find(temp).value == "Right Shift") key[i] = SDLK_RSHIFT;
			else if(find(temp).value == "Left Shift") key[i] = SDLK_LSHIFT;
			else if(find(temp).value == "Space Bar") key[i] = SDLK_SPACE;
			else if(find(temp).value == "Return") key[i] = SDLK_RETURN;
			else if(find(temp).value == "Keypad 0") key[i] = SDLK_KP0;
			else if(find(temp).value == "Keypad 1") key[i] = SDLK_KP1;
			else if(find(temp).value == "Keypad 2") key[i] = SDLK_KP2;
			else if(find(temp).value == "Keypad 3") key[i] = SDLK_KP3;
			else if(find(temp).value == "Keypad 4") key[i] = SDLK_KP4;
			else if(find(temp).value == "Keypad 5") key[i] = SDLK_KP5;
			else if(find(temp).value == "Keypad 6") key[i] = SDLK_KP6;
			else if(find(temp).value == "Keypad 7") key[i] = SDLK_KP7;
			else if(find(temp).value == "Keypad 8") key[i] = SDLK_KP8;
			else if(find(temp).value == "Keypad 9") key[i] = SDLK_KP9;
			else if(find(temp).value == "Keypad Enter") key[i] = SDLK_KP_ENTER;
			else if(find(temp).value == "Keypad Plus") key[i] = SDLK_KP_PLUS;
			else if(find(temp).value == "Keypad Minus") key[i] = SDLK_KP_MINUS;
			else if(find(temp).value == "Home") key[i] = SDLK_HOME;
			else if(find(temp).value == "End") key[i] = SDLK_END;
			else if(find(temp).value == "Page Up") key[i] = SDLK_PAGEUP;
			else if(find(temp).value == "Page Down") key[i] = SDLK_PAGEDOWN;
			else if(find(temp).value == "Tab") key[i] = SDLK_TAB;
		}
	}
	writeconfig(*map);
	return;
}

/*

	create and draw main menu screen
*/

void menu_mgm::draw_main() {
	clear_area();
	if(name != "MAIN") {
		int ypos = 110;
		del_elements();
		name = "MAIN";

		add_text(180, 30, "TuxDash Main Menu", 0, 30, -1, -1, text_color1.r, text_color1.g, text_color1.b);
		selected = add_text(220, ypos, "Resume Game", 1, 25, 0, 0, text_color2.r, text_color2.g, text_color2.b);
		add_text(220, ypos+=30, "Restart Map", 1, 25, 0, 1, text_color2.r, text_color2.g, text_color2.b);
		add_text(220, ypos+=30, "Save Map", 1, 25, 0, 2, text_color2.r, text_color2.g, text_color2.b);
		add_text(220, ypos+=30, "Load Map", 1, 25, 0, 3, text_color2.r, text_color2.g, text_color2.b);
		add_text(220, ypos+=30, "Save Game", 1, 25, 0, 4, text_color2.r, text_color2.g, text_color2.b);
		add_text(220, ypos+=30, "Load Game", 1, 25, 0, 5, text_color2.r, text_color2.g, text_color2.b);
		add_text(220, ypos+=30, "Create Custom Map", 1, 25, 0, 6, text_color2.r, text_color2.g, text_color2.b);
		add_text(220, ypos+=30, "Create Random Map", 1, 25, 0, 7, text_color2.r, text_color2.g, text_color2.b);
		add_text(220, ypos+=30, "Options", 1, 25, 0, 8, text_color2.r, text_color2.g, text_color2.b);
		add_text(220, ypos+=30, "Quit Game", 1, 25, 0, 10, text_color2.r, text_color2.g, text_color2.b);
	}
	draw_window();
	return;
}

/*
	create and draw save screen
*/

void menu_mgm::draw_save() {
	clear_area();

	if(name != "SAVE") {
		del_elements();
		name = "SAVE";

		add_text(200, 30, "Save Map", 0, 30, -1, -1);
		add_text(100, 75, "Please enter filename:", 0, 15, -1, -1);
		selected = add_box(100, 100, "savename", "", 1, 20, 0, 0, 50, 350, 30, text_color2.r, text_color2.g, text_color2.b);

		add_text(100, 430, "Enter: save map", 0, 19);
		add_text(300, 430, "ESC: cancel", 0, 19);

	}
	draw_window();

	return;
}

/*
	create and draw gamesave screen
*/

void menu_mgm::draw_gamesave() {
	clear_area();

	if(name != "GAMESAVE") {
		del_elements();
		name = "GAMESAVE";

		add_text(200, 30, "Save Game", 0, 30, -1, -1);
		add_text(100, 75, "Please enter filename:", 0, 15, -1, -1);
		selected = add_box(100, 100, "gamesavename", "", 1, 20, 0, 0, 50, 350, 30, text_color2.r, text_color2.g, text_color2.b);

		add_text(100, 430, "Enter: save game", 0, 19);
		add_text(300, 430, "ESC: cancel", 0, 19);

	}
	draw_window();

	return;
}

/*
	create and draw load screen
*/

void menu_mgm::draw_load() {
	clear_area();

	if(name != "LOAD") {
		del_elements();
		name = "LOAD";
		add_text(200, 30, "Load Map", 0, 30, -1, -1);
		add_text(100, 75, "Please enter filename:", 0, 15, -1, -1);
		selected = add_box(100, 100, "loadname", "", 1, 20, 0, 0, 50, 350, 30, text_color2.r, text_color2.g, text_color2.b);

		add_text(100, 430, "Enter: load map", 0, 19);
		add_text(300, 430, "ESC: cancel", 0, 19);
	}
	draw_window();

	return;
}

/*
	create and draw gameload screen
*/

void menu_mgm::draw_gameload() {
	clear_area();

	if(name != "GAMELOAD") {
		del_elements();
		name = "GAMELOAD";
		add_text(200, 30, "Load Game", 0, 30, -1, -1);
		add_text(100, 75, "Please enter filename:", 0, 15, -1, -1);
		selected = add_box(100, 100, "loadsavename", "", 1, 20, 0, 0, 50, 350, 30, text_color2.r, text_color2.g, text_color2.b);

		add_text(100, 430, "Enter: load game", 0, 19);
		add_text(300, 430, "ESC: cancel", 0, 19);
	}
	draw_window();

	return;
}

/*
	create and draw custom map screen
*/

void menu_mgm::draw_custom() {

	clear_area();

	if(name != "CUSTOM") {
		ostring temp;
		int posx = 50, posy = 75;
		del_elements();
		name = "CUSTOM";

		add_text(200, 30, "Create Custom Map", 0, 30);

		add_text(posx, posy, "Dimension X", 0, 15);
		selected = add_box(posx+130, posy, "dimx", temp=map->dimX, 1, 12, 0, 0, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "fields", 0, 13);

		add_text(posx, posy+=30, "Dimension Y", 0, 15);
		add_box(posx+130, posy, "dimy", temp=map->dimY, 1, 12, 0, 1, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "fields", 0, 13);

		add_text(posx, posy+=30, "Diamonds", 0, 15);
		add_box(posx+130, posy, "diamonds", "0", 1, 12, 0, 2, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "%", 0, 13);

		add_text(posx, posy+=30, "Stones", 0, 15);
		add_box(posx+130, posy, "stones", "0", 1, 12, 0, 3, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "%", 0, 13);

		add_text(posx, posy+=30, "Grass", 0, 15);
		add_box(posx+130, posy, "grass", "0", 1, 12, 0, 4, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "%", 0, 13);

		add_text(posx, posy+=30, "Normal flies", 0, 15);
		add_box(posx+130, posy, "nflies", "0", 1, 12, 0, 5, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "%", 0, 13);

		add_text(posx, posy+=30, "Butterflies", 0, 15);
		add_box(posx+130, posy, "bflies", "0", 1, 12, 0, 6, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "%", 0, 13);

		add_text(posx, posy+=30, "Stone flies", 0, 15);
		add_box(posx+130, posy, "sflies", "0", 1, 12, 0, 7, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "%", 0, 13);

		add_text(posx, posy+=30, "Walls", 0, 15);
		add_box(posx+130, posy, "walls", "0", 1, 12, 0, 8, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "%", 0, 13);

		add_text(posx, posy+=30, "Metal Walls", 0, 15);
		add_box(posx+130, posy, "mwalls", "0", 1, 12, 0, 9, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "%", 0, 13);

		add_text(posx, posy+=30, "Empty", 0, 15);
		add_box(posx+130, posy, "empty", "0", 0, 12, 0, 10, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(posx+175, posy+2, "%", 0, 13);

		posx = 350;
		posy = 75;

		add_text(posx, posy, "Metal Border", 0, 15);
		add_select(posx+130, posy, "mborder", "1", 1, 20, 1, 0);

		add_text(posx, posy+=45, "Magic Wall", 0, 15);
		add_select(posx+130, posy, "magicwall", "0", 1, 20, 1, 1);

		add_text(posx+=15, posy+=30, "- length", 0, 15);
		add_box(posx+130, posy, "magiclength", "40", 0, 12, 1, 2, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1, true, "magicwall");
		add_text(posx+175, posy+2, "fields", 0, 13);

		add_text(posx, posy+=30, "- duration", 0, 15);
		add_box(posx+130, posy, "magicduration", "70", 0, 12, 1, 3, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1, true, "magicwall");
		add_text(posx+175, posy+2, "sec", 0, 13);

		add_text(posx-=15, posy+=45, "Lava", 0, 15);
		add_select(posx+130, posy, "lavaselect", "0", 1, 20, 1, 4);

		add_text(posx+=15, posy+=30, "- maximum amount", 0, 15);
		add_box(posx+130, posy, "lavaamount", "40", 0, 12, 1, 5, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1, true, "lavaselect");
		add_text(posx+175, posy+2, "%", 0, 13);

		add_text(posx, posy+=30, "- speed", 0, 15);
		add_box(posx+130, posy, "lavaspeed", "50", 0, 12, 1, 6, 3, 35, 25, text_color2.r, text_color2.g, text_color2.b, 1, true, "lavaselect");
		add_text(posx+175, posy+2, "%", 0, 13);

		posx = 120;
		posy = 420;

		add_text(posx, posy, "Create Map", 1, 20, 0, 11, text_color2.r, text_color2.g, text_color2.b, 30, 150);

		add_text(posx+=275, posy, "Cancel", 1, 20, 1, 7, text_color2.r, text_color2.g, text_color2.b, 30, 150);

	}
	check_custom_parameters();
	draw_window();
	return;
}

/*
	create and draw custom map 2 screen
*/

void menu_mgm::draw_custom2(ostring diamonds) {
	clear_area();

	if(name != "CUSTOM2") {
		del_elements();
		name = "CUSTOM2";

		add_text(200, 30, "Custom Map Created", 0, 30);

		add_text(50, 100, "Diamonds on Map:", 0, 15);
		add_box(180, 100, "diamonds", diamonds, 0, 12, 0, 0, 4, 40, 20, text_color2.r, text_color2.g, text_color2.b, 1);

		add_text(50, 150, "Diamonds needed:", 0, 15);
		selected = add_box(180, 150, "needed", diamonds, 1, 12, 0, 1, 4, 40, 20, text_color2.r, text_color2.g, text_color2.b, 1);

		add_text(50, 180, "Time:", 0, 15);
		add_box(180, 180, "time", 250, 1, 12, 0, 2, 4, 40, 20, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(235, 182, "seconds", 0, 13);

		add_text(220, 250, "Start Map", 1, 20, 0, 3, text_color2.r, text_color2.g, text_color2.b, 30, 140);
	}

	draw_window();

	return;
}

/*
	create and draw random map screen
*/

void menu_mgm::draw_random() {

	clear_area();

	if(name != "RANDOM") {
		del_elements();
		name = "RANDOM";

		add_text(200, 30, "Random Map", 0, 30);
		add_text(15, 80, "Do you really want to quit the current game", 0, 18);
		add_text(15, 100, "and start a random map?", 0, 18);

		add_text(175, 130, "Yes", 1, 20, 0, 0, text_color2.r, text_color2.g, text_color2.b, 25, 85);
		selected = add_text(280, 130, "No", 1, 20, 1, 0, text_color2.r, text_color2.g, text_color2.b, 25, 80);
	}

	draw_window();
	return;
}

/*
	create and draw options screen
*/

void menu_mgm::draw_options() {
	clear_area();

	if(name != "OPTIONS") {
		ostring fullscreen, windowed, resolution, framerate;
		if(map->fullscreen == true) {
			fullscreen = "1";
			windowed = "0";
		}
		else {
			fullscreen = "0";
			windowed = "1";
		}

		if(map->framerate == -1) framerate = "0";
		else framerate = "1";

		if(map->fullscreenX == 640) resolution = "640x480";
		else if(map->fullscreenX == 800) resolution = "800x600";
		else if(map->fullscreenX == 1024) resolution = "1024x768";
		else if(map->fullscreenX == 1280) resolution = "1280x1024";
		del_elements();

		name = "OPTIONS";
		add_text(260, 25, "Options", 0, 30);

		add_text(50, 75, "Window Mode", 0, 15);
		selected = add_select(180, 75, "windowselect", windowed, 1, 20, 0, 0, "fullscreenselect");

		add_text(50, 100, "Screen width", 0, 15);
		add_box(180, 100, "dimx", map->screenX, windowed, 12, 0, 1, 2, 40, 25, text_color2.r, text_color2.g, text_color2.b, 1, true, "windowselect");
		add_text(225, 102, "fields", 0, 13);

		add_text(50, 130, "Screen height", 0, 15);
		add_box(180, 130, "dimy", map->screenY, windowed, 12, 0, 2, 2, 40, 25, text_color2.r, text_color2.g, text_color2.b, 1, true, "windowselect");
		add_text(225, 132, "fields", 0, 13);

		add_text(50, 160, "Fullscreen Mode", 0, 15);
		add_select(180, 160, "fullscreenselect", fullscreen, 1, 20, 0, 3, "windowselect");

		add_text(50, 190, "Resolution", 0, 15);
		add_box(180, 190, "resolution", resolution, atoi(fullscreen), 11, 0, 4, 2, 72, 25, text_color2.r, text_color2.g, text_color2.b, 3, true, "fullscreenselect");
		add_text(255, 192, "pixel", 0, 13);

		add_text(50, 220, "Scroll Distance X", 0, 15);
		add_box(180, 220, "scrolldistx",  map->scrolldistX, 1, 12, 0, 5, 2, 40, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(225, 222, "fields", 0, 13);

		add_text(50, 250, "Scroll Distance Y", 0, 15);
		add_box(180, 250, "scrolldisty", map->scrolldistY, 1, 12, 0, 6, 2, 40, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(225, 252, "fields", 0, 13);

		add_text(50, 280, "Scroll Amount X", 0, 15);
		add_box(180, 280, "scrollamountx", map->scrollamountX, 1, 12, 0, 7, 2, 40, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(225, 282, "fields", 0, 13);

		add_text(50, 310, "Scroll Amount Y", 0, 15);
		add_box(180, 310, "scrollamounty", map->scrollamountY, 1, 12, 0, 8, 2, 40, 25, text_color2.r, text_color2.g, text_color2.b, 1);
		add_text(225, 312, "fields", 0, 13);

		add_text(50, 340, "Speed", 0, 15);
		add_box(180, 340, "speed", (speed-50)*-1, 1, 12, 0, 9, 2, 40, 25, text_color2.r, text_color2.g, text_color2.b, 1);

		add_text(50, 370, "Theme", 0, 15);
		add_box(180, 370, "themefolder", map->theme, 1, 14, 0, 10, 30, 120, 25, text_color2.r, text_color2.g, text_color2.b);

		add_text(310, 75, "Controls", 0, 15);

  		add_text(310, 115, "Left:", 0, 15);
		add_box(410, 115, "ctrl_left", keytoa(key[0]), 1, 14, 1, 0, 2, 100, 25, text_color2.r, text_color2.g, text_color2.b, 2);

		add_text(310, 145, "Right:", 0, 15);
		add_box(410, 145, "ctrl_right", keytoa(key[1]), 1, 14, 1, 1, 2, 100, 25, text_color2.r, text_color2.g, text_color2.b, 2);

		add_text(310, 175, "Up:", 0, 15);
		add_box(410, 175, "ctrl_up", keytoa(key[2]), 1, 14, 1, 2, 2, 100, 25, text_color2.r, text_color2.g, text_color2.b, 2);

		add_text(310, 205, "Down:", 0, 15);
		add_box(410, 205, "ctrl_down", keytoa(key[3]), 1, 14, 1, 3, 2, 100, 25, text_color2.r, text_color2.g, text_color2.b, 2);

		add_text(310, 235, "Use:", 0, 15);
		add_box(410, 235, "ctrl_use", keytoa(key[4]), 1, 14, 1, 4, 2, 100, 25, text_color2.r, text_color2.g, text_color2.b, 2);

		add_text(310, 265, "Scroll:", 0, 15);
		add_box(410, 265, "ctrl_scroll", keytoa(key[5]), 1, 14, 1, 5, 2, 100, 25, text_color2.r, text_color2.g, text_color2.b, 2);

		add_text(310, 295, "Pause:", 0, 15);
		add_box(410, 295, "ctrl_pause", keytoa(key[6]), 1, 14, 1, 6, 2, 100, 25, text_color2.r, text_color2.g, text_color2.b, 2);

		add_text(310, 335, "Show Framerate", 0, 15);
		add_select(440, 335, "framerate", framerate, 1, 20, 1, 7);

		add_text(85, 430, "Save Settings", 1, 20, 0, 11, text_color2.r, text_color2.g, text_color2.b, 25, 180);

		add_text(370, 430, "Cancel", 1, 20, 1, 8, text_color2.r, text_color2.g, text_color2.b, 25, 125);

	}

	draw_window();

	return;
}

/*
	validate custom map parameters
*/

void menu_mgm::check_custom_parameters() {
	int dimX = atoi(find("dimx").value), dimY = atoi(find("dimy").value), diamonds = atoi(find("diamonds").value), stones = atoi(find("stones").value), grass = atoi(find("grass").value);
	int nflies = atoi(find("nflies").value), bflies = atoi(find("bflies").value), sflies = atoi(find("sflies").value), walls = atoi(find("walls").value), metal_walls = atoi(find("mwalls").value);
	int empty=100, *temp;
	if(dimX < 0) dimX = 0;
	if(dimY < 0) dimY = 0;



	for(int i=0; i<8; i++) {
		if(i==0) temp = &diamonds;
		else if(i==1) temp = &stones;
		else if(i==2) temp = &grass;
		else if(i==3) temp = &nflies;
		else if(i==4) temp = &bflies;
		else if(i==5) temp = &sflies;
		else if(i==6) temp = &walls;
		else if(i==7) temp = &metal_walls;
		if(*temp < 0) *temp = 0;
		else if(*temp > empty) *temp = empty;
		empty-= *temp;
	}

	find("dimx").value = dimX;
	find("dimy").value = dimY;
	find("diamonds").value = diamonds;
	find("stones").value = stones;
	find("nflies").value = nflies;
	find("bflies").value = bflies;
	find("sflies").value = sflies;
	find("grass").value = grass;
	find("walls").value = walls;
	find("mwalls").value = metal_walls;
	find("empty").value = empty;

	if(find("magicwall").value == "1") {	// check magic wall
		int length = atoi(find("magiclength").value), duration = atoi(find("magicduration").value);

		if(length >= dimX - 4) length = dimX - 4;
		if(length < 0) length = 0;

		if(duration < 0) duration = 0;

		find("magiclength").value = length;
		find("magicduration").value = duration;
	}

	if(find("lavaselect").value == "1") {	// check lava
		int percent = atoi(find("lavaamount").value), speed = atoi(find("lavaspeed").value);

		if(percent < 0) percent = 0;
		else if(percent > 100) percent = 100;

		if(speed < 0) speed = 0;
		else if(speed > 100) speed = 100;

		find("lavaamount").value = percent;
		find("lavaspeed").value = speed;
	}
	return;
}

/*
	open menu, draw correct screens and exit menu
*/

int menu_mgm::open() {
	if((SDL_GetTicks() - map->global_action < 200 && map->global_action != 0) || map->scrolldiff%30 != 0) return 0;
	if(map->global_action != 0) map->global_action = SDL_GetTicks();
	int menue_type = 0;
	Uint8 *tastenstatus;
	bool running=true;
	SDL_Event event;
	draw_main();
	SDL_EnableKeyRepeat(500, 100);
	while(running==true) {
		tastenstatus = SDL_GetKeyState(NULL);
		while(SDL_PollEvent(&event)) {
			if(event.type == SDL_QUIT) return 1;
			else if(menue_type >= 0) {
					if(event.type == SDL_KEYDOWN) {
					int new_menue;
					new_menue = key_mgm(event.key.keysym.sym, tastenstatus);
					if(new_menue >= 0) menue_type = new_menue;
					else if(new_menue == -2) running = 0;
					else if(new_menue == -3) return 1;

					if(menue_type == 0) draw_main();
					else if(menue_type == 1) draw_save();
					else if(menue_type == 2) draw_load();
					else if(menue_type == 3) draw_custom();
					else if(menue_type == 4) draw_custom2(find("diamonds").value);
					else if(menue_type == 5) draw_random();
					else if(menue_type == 6) draw_options();
					else if(menue_type == 7) draw_gamesave();
					else if(menue_type == 8) draw_gameload();
				}
			}
		}
		surface::refresh();
	}
	SDL_EnableKeyRepeat(0, 0);
	name = "EMPTY";

	surface::screenfill(windowx, windowy, 16*30, 21*30, map->BGColor.r, map->BGColor.g, map->BGColor.b); 	// Die Stelle wieder sauber machen
 	if(map->global_action != 0) {
  		map->global_action = SDL_GetTicks();
		map->draw_screen(1);
 	}
	selected = 0;
	return 0;
}


/*
    TuxDash 0.8
    fields.h - a simple class for array objects

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

#ifndef _felder_h
#define _felder_h



class fields {
	friend class map;
	friend class field_mgm;
	private:
		short type;
		short action;
   /*
				what happens on a field
					*** gemeral rules: ***
					0 --> nothing special, normal behaviour
					< 0 --> in most cases a blocked field

					*** Type player ***
						1 = player movement
						2/3 = player animation 2/3
						4 = player pushing
					*** Type diamond ***
						4 = normal animation
						5 = falling diamond
						7 = diamond topples left
						8 = diamond topples right
						12 = falling diamond, decision making
						13 = falling into magic wall
						14 = falling out of magic wall
					*** type stone ***
						5 = falling stone
						7 = stone topples left
						8 = stone topples right
						11 = stone is pushed
						12 = falling stone, decision making
						13 = falling into magic wall
						14 = falling out of magic wall
					*** type entrance/exit ***
						0 = exit
						1 = entrance

					10 = explosion center
			*/
		short moved;
   /*
				pixel difference:
					action=0:	no meaning
					action=1:	how many pixels the player already moved

			*/
		short direction;
			/*
				movement directon
					action=0:	no meaning
					action=1:	directin the player is moving to
			*/
		short animcount;
			/*
				counter for different animations
			*/
	public:
		fields();	// default constructor sets default values
  		void clear();	// set a field to default values (empty)
		inline bool operator==(int);	// compare operators for direct compare of field types
		inline bool operator!=(int);
		inline bool operator<(int);
		inline bool operator<=(int);
		inline bool operator>=(int);
		inline bool operator>(int);
		inline void operator=(int);
		void settype(short, short);	// set type and action for a field

};

#include "fields.cpp"
#endif /* fields.h */

/*
    TuxDash 0.8
    menu.h - menu management

    Copyright (C) 2003 Matthias Gerstner <Matthias.Gerstner@student.fh-nuernberg.de> <http://www.tuxdash.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


*/

#ifndef _menu_mgm_h
#define _menu_mgm_h

/*
	this was supposed as a simple menu class but I fear it is a mad thing now
	at least it works somehow ...
*/

class menu_mgm {
	private:
		class element *anchor;	// anchor element for linked list of elements
		ostring name;	// name of the current menu
		int windowx, windowy;

		class element* selected;	// pointer to the current selected element
		class map* map;			// pointer to the map object

		class element& add();		// add an element to the current menu screen
		class element& find(const ostring&);	// find the element with the specified name in linked list
		void del_elements();	// delete all elements in linked list
		void clear_area();	// draw a blank menu screen
		void change_deps(bool, class element* =NULL);	// change dependency of multiple choice box
		void options_save();	// save options in options screen
		void draw_main();	// draw the named screen
		void draw_save();
		void draw_gamesave();
		void draw_load();
		void draw_gameload();
		void draw_custom();
		void draw_custom2(ostring);
		void draw_options();
		void draw_random();
		void check_custom_parameters();	// check if parameters are okay
		ostring keytoa(SDLKey);	// cast SDLKey to ASCII
		class element* add_text(int, int, const ostring&, bool, int, int =-1, int =-1, unsigned char =0, unsigned char =0, unsigned char = 0, int = -1, int = -1);	// add a text element
		class element* add_box(int, int, const ostring&, const ostring&, bool, int, int, int, int, int, int, unsigned char, unsigned char, unsigned char, int = 0, bool =false, char* =0);	// add a box element
		class element* add_select(int, int, const ostring&, const ostring&, bool, int, int, int, const char* oneoftwo=0);	// add a select element
		void draw_window();	// draw the current menu screen with all elements
		void selection_mgm(char);	// process user input for menu navigation
		int key_mgm(SDLKey&, Uint8*);	// management for control configuration
	public:
		SDL_Color text_color1, text_color2, text_select_color, boxbg_color, boxbg_color2;	// color values for different menu elements
		menu_mgm();
		menu_mgm(class map&);
		int open();	// open menu, draw main screen
		void init(class map&);	// initialise menu with needed values
		~menu_mgm();	// delete possibly remaining elements in linked list
};


#endif /* menu_mgm.h */
